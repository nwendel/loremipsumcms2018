﻿#region License
// Copyright (c) Niklas Wendel 2017-2018
// 
// Licensed under the Apache License, Version 2.0 (the "License"); 
// you may not use this file except in compliance with the License. 
// You may obtain a copy of the License at 
// 
// http://www.apache.org/licenses/LICENSE-2.0 
// 
// Unless required by applicable law or agreed to in writing, software 
// distributed under the License is distributed on an "AS IS" BASIS, 
// WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied. 
// See the License for the specific language governing permissions and 
// limitations under the License.
#endregion
using LoremIpsumCMS.PuppeteerSharp.PageModel;
using LoremIpsumCMS.PuppeteerSharp.PageModel.Elements;
using Microsoft.AspNetCore.Hosting;
using Microsoft.AspNetCore.Hosting.Server.Features;
using PuppeteerSharp;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace LoremIpsumCMS.PuppeteerSharp.Internal
{

    /// <summary>
    /// 
    /// </summary>
    public class AsyncBrowserWorker
    {

        #region Fields

        private readonly IWebHost _webHost;
        private readonly Browser _browser;
        private readonly List<Func<Browser, Page,Task>> _commands = new List<Func<Browser, Page, Task>>();

        #endregion

        #region Constructor

        /// <summary>
        /// 
        /// </summary>
        /// <param name="webHost"></param>
        /// <param name="browser"></param>
        public AsyncBrowserWorker(IWebHost webHost, Browser browser)
        {
            _webHost = webHost;
            _browser = browser;
        }

        #endregion

        #region Properties

        /// <summary>
        /// 
        /// </summary>
        public bool IsExecuted { get; private set; }

        /// <summary>
        /// 
        /// </summary>
        public Page Page { get; private set; }

        #endregion

        #region Add Command

        /// <summary>
        /// 
        /// </summary>
        /// <param name="command"></param>
        public void AddCommand(Func<Task> command)
        {
            _commands.Add((browser, page) => command());
        }

        /// <summary>
        /// 
        /// </summary>
        /// <param name="command"></param>
        public void AddCommand(Func<Page, Task> command)
        {
            _commands.Add((browser, page) => command(page));
        }

        /// <summary>
        /// 
        /// </summary>
        /// <param name="command"></param>
        public void AddCommand(Func<Browser, Page, Task> command)
        {
            IsExecuted = false;
            _commands.Add(command);
        }

        #endregion

        #region Navigate To

        /// <summary>
        /// 
        /// </summary>
        /// <typeparam name="TPageModel"></typeparam>
        /// <param name="pathAndQuery"></param>
        /// <returns></returns>
        public TPageModel NavigateTo<TPageModel>(string pathAndQuery)
            where TPageModel : AbstractPageModel, new()
        {
            var webHostAddress = GetWebHostAddress();
            var url = $"{webHostAddress}{pathAndQuery}";

            AddCommand(async page =>
            {
                var waitTask = page.WaitForNavigationAsync();
                var gotoTask = page.GoToAsync(url);
                await Task.WhenAll(gotoTask, waitTask);
            });

            return PageModelFactory.CreatePageModel<TPageModel>(this);
        }

        /// <summary>
        /// 
        /// </summary>
        /// <typeparam name="TPageModel"></typeparam>
        /// <param name="selectorAction"></param>
        /// <returns></returns>
        public TPageModel NavigateTo<TPageModel>(Action<Selector> selectorAction)
            where TPageModel : AbstractPageModel, new()
        {
            var element = FindElement<Element>(selectorAction);
            element.Click();
            return PageModelFactory.CreatePageModel<TPageModel>(this);
        }

        /// <summary>
        /// 
        /// </summary>
        /// <typeparam name="TPageModel"></typeparam>
        /// <param name="xpathAction"></param>
        /// <returns></returns>
        public TPageModel NavigateTo<TPageModel>(Action<XPath> xpathAction)
            where TPageModel : AbstractPageModel, new()
        {
            var element = FindElement<Element>(xpathAction);
            element.Click();
            return PageModelFactory.CreatePageModel<TPageModel>(this);
        }

        /// <summary>
        /// 
        /// </summary>
        /// <returns></returns>
        private string GetWebHostAddress()
        {
            var feature = _webHost.ServerFeatures.Get<IServerAddressesFeature>();
            var address = feature.Addresses.First();
            return address;
        }

        #endregion

        #region Find Element

        /// <summary>
        /// 
        /// </summary>
        /// <typeparam name="TElement"></typeparam>
        /// <param name="selectorAction"></param>
        /// <returns></returns>
        public TElement FindElement<TElement>(Action<Selector> selectorAction)
            where TElement : Element, new()
        {
            var selector = new Selector();
            selectorAction(selector);

            var element = PageModelFactory.CreateElement<TElement>(this);
            AddCommand(async page =>
            {
                var elementHandle = await page.QuerySelectorAsync(selector.Value);
                if (elementHandle == null)
                {
                    // TODO: Throw better exception
                    throw new Exception("Element not found");
                }
                element.Handle = elementHandle;
            });

            return element;
        }

        /// <summary>
        /// 
        /// </summary>
        /// <typeparam name="TElement"></typeparam>
        /// <param name="xpathAction"></param>
        /// <returns></returns>
        public TElement FindElement<TElement>(Action<XPath> xpathAction)
            where TElement : Element, new()
        {
            var xpath = new XPath();
            xpathAction(xpath);

            var element = PageModelFactory.CreateElement<TElement>(this);
            AddCommand(async page =>
            {
                var elementHandle = await page.XPathAsync(xpath.Value);
                element.Handle = elementHandle.FirstOrDefault();
                if (element.Handle == null)
                {
                    // TODO: Better exception here
                    throw new Exception("Element not found");
                }
            });

            return element;
        }

        #endregion

        #region Execute Async

        /// <summary>
        /// 
        /// </summary>
        /// <returns></returns>
        public async Task ExecuteAsync()
        {
            if (Page == null)
            {
                Page = await _browser.NewPageAsync();
            }

            foreach (var command in _commands)
            {
                await command(_browser, Page);
            }

            _commands.Clear();
            IsExecuted = true;
        }

        #endregion

    }

}
