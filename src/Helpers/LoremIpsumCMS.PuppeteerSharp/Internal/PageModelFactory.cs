﻿#region License
// Copyright (c) Niklas Wendel 2017-2018
// 
// Licensed under the Apache License, Version 2.0 (the "License"); 
// you may not use this file except in compliance with the License. 
// You may obtain a copy of the License at 
// 
// http://www.apache.org/licenses/LICENSE-2.0 
// 
// Unless required by applicable law or agreed to in writing, software 
// distributed under the License is distributed on an "AS IS" BASIS, 
// WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied. 
// See the License for the specific language governing permissions and 
// limitations under the License.
#endregion
using LoremIpsumCMS.PuppeteerSharp.PageModel;
using LoremIpsumCMS.PuppeteerSharp.PageModel.Elements;

namespace LoremIpsumCMS.PuppeteerSharp.Internal
{

    /// <summary>
    /// 
    /// </summary>
    public static class PageModelFactory
    {

        #region Create Page Model

        /// <summary>
        /// 
        /// </summary>
        /// <typeparam name="TPageModel"></typeparam>
        /// <param name="asyncBrowserWorker"></param>
        /// <returns></returns>
        public static TPageModel CreatePageModel<TPageModel>(AsyncBrowserWorker asyncBrowserWorker)
            where TPageModel : AbstractPageModel, IAsyncBrowserWorkerAware, new()
        {
            var pageModel = new TPageModel {AsyncBrowserWorker = asyncBrowserWorker};
            return pageModel;
        }

        #endregion

        #region Create Element

        /// <summary>
        /// 
        /// </summary>
        /// <typeparam name="TElement"></typeparam>
        /// <param name="asyncBrowserWorker"></param>
        /// <returns></returns>
        public static TElement CreateElement<TElement>(AsyncBrowserWorker asyncBrowserWorker)
            where TElement : Element, IAsyncBrowserWorkerAware, new()
        {
            var pageModel = new TElement
            {
                AsyncBrowserWorker = asyncBrowserWorker,
            };
            return pageModel;
        }

        #endregion

    }

}
