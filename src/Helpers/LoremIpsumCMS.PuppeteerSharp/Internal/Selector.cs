﻿#region License
// Copyright (c) Niklas Wendel 2017-2018
// 
// Licensed under the Apache License, Version 2.0 (the "License"); 
// you may not use this file except in compliance with the License. 
// You may obtain a copy of the License at 
// 
// http://www.apache.org/licenses/LICENSE-2.0 
// 
// Unless required by applicable law or agreed to in writing, software 
// distributed under the License is distributed on an "AS IS" BASIS, 
// WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied. 
// See the License for the specific language governing permissions and 
// limitations under the License.
#endregion
using System;
using System.Linq.Expressions;

namespace LoremIpsumCMS.PuppeteerSharp.Internal
{

    /// <summary>
    /// 
    /// </summary>
    public class Selector
    {

        #region Value

        /// <summary>
        /// 
        /// </summary>
        public string Value { get; private set; }

        #endregion

        #region By

        /// <summary>
        /// 
        /// </summary>
        /// <typeparam name="TViewModel"></typeparam>
        /// <param name="propertyExpression"></param>
        /// <returns></returns>
        public void By<TViewModel>(Expression<Func<TViewModel, object>> propertyExpression)
        {
            // TODO: This is wrong, I need to hook into AspNetCore's id generation for models
            Value = "#Name";
        }

        #endregion

        #region By Submit Button

        /// <summary>
        /// 
        /// </summary>
        /// <param name="buttonText"></param>
        public void BySubmitButton(string buttonText)
        {
            Value = "button[type = 'submit']";
        }

        #endregion

    }

}
