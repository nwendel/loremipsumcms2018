﻿#region License
// Copyright (c) Niklas Wendel 2017-2018
// 
// Licensed under the Apache License, Version 2.0 (the "License"); 
// you may not use this file except in compliance with the License. 
// You may obtain a copy of the License at 
// 
// http://www.apache.org/licenses/LICENSE-2.0 
// 
// Unless required by applicable law or agreed to in writing, software 
// distributed under the License is distributed on an "AS IS" BASIS, 
// WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied. 
// See the License for the specific language governing permissions and 
// limitations under the License.
#endregion
using LoremIpsumCMS.PuppeteerSharp.Internal;
using LoremIpsumCMS.PuppeteerSharp.PageModel.Elements;
using PuppeteerSharp;
using System;
using System.Threading.Tasks;

namespace LoremIpsumCMS.PuppeteerSharp.PageModel
{

    /// <summary>
    /// 
    /// </summary>
    public class AbstractPageModel : 
        IAsyncBrowserWorkerAware
    {

        #region Async Browser Worker Aware

        /// <inheritdoc />
        /// <summary>
        /// </summary>
        AsyncBrowserWorker IAsyncBrowserWorkerAware.AsyncBrowserWorker { get; set; }

        /// <summary>
        /// 
        /// </summary>
        /// <returns></returns>
        private AsyncBrowserWorker GetAsyncBrowserWorker()
        {
            var asyncBrowserWorkerAware = this as IAsyncBrowserWorkerAware;
            var asyncBrowserWorker = asyncBrowserWorkerAware.AsyncBrowserWorker;
            return asyncBrowserWorker;
        }

        /// <summary>
        /// 
        /// </summary>
        private Page Page
        {
            get
            {
                var asyncBrowserWorker = GetAsyncBrowserWorker();
                if (!asyncBrowserWorker.IsExecuted)
                {
                    throw new Exception("Not executed");
                }
                var page = asyncBrowserWorker.Page;
                return page;
            }
        }

        #endregion

        #region Properties

        /// <summary>
        /// 
        /// </summary>
        public Task<string> TitleAsync => Page.GetTitleAsync();

        #endregion

        #region Create Page Model

        /// <summary>
        /// 
        /// </summary>
        /// <typeparam name="TPageModel"></typeparam>
        /// <returns></returns>
        protected TPageModel CreatePageModel<TPageModel>()
            where TPageModel : AbstractPageModel, new()
        {
            var pageModel = PageModelFactory.CreatePageModel<TPageModel>(GetAsyncBrowserWorker());
            return pageModel;
        }

        #endregion

        #region Find Element

        /// <summary>
        /// 
        /// </summary>
        /// <typeparam name="TElement"></typeparam>
        /// <param name="selectorAction"></param>
        /// <returns></returns>
        public TElement FindElement<TElement>(Action<Selector> selectorAction)
            where TElement : Element, new()
        {
            return GetAsyncBrowserWorker().FindElement<TElement>(selectorAction);
        }

        /// <summary>
        /// 
        /// </summary>
        /// <typeparam name="TElement"></typeparam>
        /// <param name="xpathAction"></param>
        /// <returns></returns>
        public TElement FindElement<TElement>(Action<XPath> xpathAction)
            where TElement : Element, new()
        {
            return GetAsyncBrowserWorker().FindElement<TElement>(xpathAction);
        }

        #endregion

        #region Navigate To

        /// <summary>
        /// 
        /// </summary>
        /// <typeparam name="TPageModel"></typeparam>
        /// <param name="pathAndQuery"></param>
        /// <returns></returns>
        public TPageModel NavigateTo<TPageModel>(string pathAndQuery)
            where TPageModel : AbstractPageModel, new()
        {
            return GetAsyncBrowserWorker().NavigateTo<TPageModel>(pathAndQuery);
        }

        /// <summary>
        /// 
        /// </summary>
        /// <typeparam name="TPageModel"></typeparam>
        /// <param name="selectorAction"></param>
        /// <returns></returns>
        public TPageModel NavigateTo<TPageModel>(Action<Selector> selectorAction) 
            where TPageModel : AbstractPageModel, new()
        {
            return GetAsyncBrowserWorker().NavigateTo<TPageModel>(selectorAction);
        }

        /// <summary>
        /// 
        /// </summary>
        /// <typeparam name="TPageModel"></typeparam>
        /// <param name="xpathAction"></param>
        /// <returns></returns>
        public TPageModel NavigateTo<TPageModel>(Action<XPath> xpathAction)
            where TPageModel : AbstractPageModel, new()
        {
            return GetAsyncBrowserWorker().NavigateTo<TPageModel>(xpathAction);
        }

        #endregion

    }

}
