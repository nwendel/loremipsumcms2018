﻿#region License
// Copyright (c) Niklas Wendel 2017-2018
// 
// Licensed under the Apache License, Version 2.0 (the "License"); 
// you may not use this file except in compliance with the License. 
// You may obtain a copy of the License at 
// 
// http://www.apache.org/licenses/LICENSE-2.0 
// 
// Unless required by applicable law or agreed to in writing, software 
// distributed under the License is distributed on an "AS IS" BASIS, 
// WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied. 
// See the License for the specific language governing permissions and 
// limitations under the License.
#endregion
using LoremIpsumCMS.PuppeteerSharp.Internal;
using PuppeteerSharp;

namespace LoremIpsumCMS.PuppeteerSharp.PageModel.Elements
{

    /// <summary>
    /// 
    /// </summary>
    public class Element : IAsyncBrowserWorkerAware
    {

        #region Properties

        /// <summary>
        /// 
        /// </summary>
        public ElementHandle Handle { get; set; }

        #endregion

        #region Async Browser Worker Aware

        /// <inheritdoc />
        /// <summary>
        /// </summary>
        AsyncBrowserWorker IAsyncBrowserWorkerAware.AsyncBrowserWorker { get; set; }

        /// <summary>
        /// 
        /// </summary>
        /// <returns></returns>
        protected AsyncBrowserWorker GetAsyncBrowserWorker()
        {
            var asyncBrowserWorkerAware = this as IAsyncBrowserWorkerAware;
            var asyncBrowserWorker = asyncBrowserWorkerAware.AsyncBrowserWorker;
            return asyncBrowserWorker;
        }

        #endregion

        #region Click

        /// <summary>
        /// 
        /// </summary>
        public void Click()
        {
            GetAsyncBrowserWorker().AddCommand(async page =>
            {
                await Handle.ClickAsync();

                //var waitTask = page.WaitForNavigationAsync();
                //var clickTask = Handle.ClickAsync();
                //await Task.WhenAll(clickTask, waitTask);
            });
        }

        #endregion

    }

}
