﻿#region License
// Copyright (c) Niklas Wendel 2017-2018
// 
// Licensed under the Apache License, Version 2.0 (the "License"); 
// you may not use this file except in compliance with the License. 
// You may obtain a copy of the License at 
// 
// http://www.apache.org/licenses/LICENSE-2.0 
// 
// Unless required by applicable law or agreed to in writing, software 
// distributed under the License is distributed on an "AS IS" BASIS, 
// WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied. 
// See the License for the specific language governing permissions and 
// limitations under the License.
#endregion
using LoremIpsumCMS.PuppeteerSharp.Internal;
using LoremIpsumCMS.PuppeteerSharp.PageModel;
using Microsoft.AspNetCore.Hosting;
using Microsoft.Extensions.DependencyInjection;
using PuppeteerSharp;
using System;

namespace LoremIpsumCMS.PuppeteerSharp
{

    /// <summary>
    /// 
    /// </summary>
    public class PuppeteerHost : IDisposable
    {

        private readonly Browser _browser;
        private readonly IWebHost _webHost;
        private IServiceProvider _serviceProvider;

        /// <summary>
        /// 
        /// </summary>
        /// <param name="browser"></param>
        /// <param name="webHostBuilder"></param>
        public PuppeteerHost(Browser browser, IWebHostBuilder webHostBuilder)
        {
            _browser = browser;
            _webHost = webHostBuilder.Build();
            _webHost.Start();

            ConfigureServices();
        }

        /// <summary>
        /// 
        /// </summary>
        private void ConfigureServices()
        {
            var services = new ServiceCollection();

            services.AddSingleton(_webHost);
            services.AddSingleton(_browser);

            _serviceProvider = services.BuildServiceProvider();
        }

        /// <summary>
        /// 
        /// </summary>
        /// <typeparam name="TPageModel"></typeparam>
        /// <param name="pathAndQuery"></param>
        /// <returns></returns>
        public TPageModel NavigateTo<TPageModel>(string pathAndQuery)
            where TPageModel : AbstractPageModel, new()
        {
            var worker = new AsyncBrowserWorker(_webHost, _browser);
            var pageModel = worker.NavigateTo<TPageModel>(pathAndQuery);
            return pageModel;
        }

        /// <summary>
        /// 
        /// </summary>
        public void Dispose()
        {
            _browser?.Dispose();
            _webHost?.Dispose();
        }

    }

}
