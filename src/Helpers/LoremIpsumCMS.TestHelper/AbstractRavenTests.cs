﻿#region License
// Copyright (c) Niklas Wendel 2017-2018
// 
// Licensed under the Apache License, Version 2.0 (the "License"); 
// you may not use this file except in compliance with the License. 
// You may obtain a copy of the License at 
// 
// http://www.apache.org/licenses/LICENSE-2.0 
// 
// Unless required by applicable law or agreed to in writing, software 
// distributed under the License is distributed on an "AS IS" BASIS, 
// WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied. 
// See the License for the specific language governing permissions and 
// limitations under the License.
#endregion
using LoremIpsumCMS.Infrastructure.Raven.UniqueConstraints;
using Raven.Client.Documents;
using Raven.TestDriver;
using System;

namespace LoremIpsumCMS.TestHelper
{

    /// <summary>
    /// 
    /// </summary>
    public class AbstractRavenTests : RavenTestDriver
    {

        /// <summary>
        /// 
        /// </summary>
        protected AbstractRavenTests()
        {
            //ConfigureServer(new TestServerOptions
            //{
            //    CommandLineArgs = new List<string>(new[] { "--Features.Availability=Experimental" })
            //});

            DocumentStore = GetDocumentStore(null, Guid.NewGuid().ToString());
        }

        /// <summary>
        /// 
        /// </summary>
        /// <param name="documentStore"></param>
        protected override void PreInitialize(IDocumentStore documentStore)
        {
            base.PreInitialize(documentStore);

            UniqueConstraintListener.RegisterFor(documentStore);
        }

        /// <summary>
        /// 
        /// </summary>
        protected IDocumentStore DocumentStore { get; }

    }

}
