﻿#region License
// Copyright (c) Niklas Wendel 2017-2018
// 
// Licensed under the Apache License, Version 2.0 (the "License"); 
// you may not use this file except in compliance with the License. 
// You may obtain a copy of the License at 
// 
// http://www.apache.org/licenses/LICENSE-2.0 
// 
// Unless required by applicable law or agreed to in writing, software 
// distributed under the License is distributed on an "AS IS" BASIS, 
// WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied. 
// See the License for the specific language governing permissions and 
// limitations under the License.
#endregion
using LoremIpsumCMS.Infrastructure.Cqrs;
using LoremIpsumCMS.Infrastructure.Model;
using Newtonsoft.Json;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Reflection;
using System.Threading.Tasks;
using Xunit;

namespace LoremIpsumCMS.TestHelper.Cqrs
{

    /// <summary>
    /// 
    /// </summary>
    public abstract class AbstractCommandHandlerTests<TAggregate> : AbstractCqrsTests
        where TAggregate : AbstractAggregate, new()
    {


        /// <summary>
        /// 
        /// </summary>
        protected object CommandHandler { private get; set; }

        /// <summary>
        /// 
        /// </summary>
        /// <param name="given"></param>
        /// <param name="when"></param>
        /// <param name="then"></param>
        protected async Task Test(Func<Task<TAggregate>> given, Func<TAggregate, Task<object>> when, Action<object> then)
        {
            var aggregate = await given();
            var events = await when(aggregate);
            then(events);
        }

        /// <summary>
        /// 
        /// </summary>
        /// <param name="events"></param>
        /// <returns></returns>
        protected Func<Task<TAggregate>> Given(params AbstractEvent[] events)
        {
            return async () =>
            {
                var aggregate = new TAggregate();
                foreach (var @event in events)
                {
                    aggregate.ApplyEvent(@event);
                }

                // TODO: Is this correct to not create the aggregate if there are no events?
                if (events.Any())
                {
                    await AsyncDocumentSession.StoreAsync(aggregate);
                }

                return aggregate;
            };
        }

        /// <summary>
        /// 
        /// </summary>
        /// <typeparam name="TCommand"></typeparam>
        /// <param name="command"></param>
        /// <returns></returns>
        protected Func<TAggregate, Task<object>> When<TCommand>(TCommand command)
            where TCommand : AbstractCommand<TAggregate>
        {
            return async aggregate =>
            {
                try
                {
                    // TODO: Is this correct to not set id if there were no events?
                    if (aggregate.Id != null)
                    {
                        var propertyInfo = command.GetType().GetProperty("Id");
                        if (propertyInfo != null)
                        {
                            propertyInfo.SetValue(command, aggregate.Id);
                        }
                    }

                    // TODO: There is too much code here which is similar to code in the RavenCommandSender, can't I use the command sender instead?
                    if (CommandHandler is IValidateCommand<TCommand> validator)
                    {
                        await validator.ValidateAsync(command);
                    }

                    if (!(CommandHandler is IHandleCommand<TCommand> handler))
                    {
                        throw new CqrsException($"Command handler {CommandHandler.GetType().Name} does not handle command {command.GetType().Name}");
                    }

                    var events = await handler.HandleAsync(command);
                    var appliedEvents = new List<AbstractEvent>();
                    foreach (var @event in events)
                    {
                        if (@event.GetType().GetCustomAttribute<SkipAggregateApplyAttribute>() == null)
                        {
                            // TODO: Check if should be applied here...
                            aggregate.ApplyEvent(@event);
                        }
                        appliedEvents.Add(@event);
                    }

                    return appliedEvents.ToArray();
                }
                catch (Exception e)
                {
                    return e;
                }
            };
        }

        /// <summary>
        /// 
        /// </summary>
        /// <param name="expectedEvents"></param>
        /// <returns></returns>
        protected Action<object> Then(params AbstractEvent[] expectedEvents)
        {
            return got =>
            {
                switch (got)
                {
                    case AbstractEvent[] gotEvents:
                        var gotEventNames = gotEvents.Select(x => x.GetType().Name).ToList();
                        var expectedEventNames = expectedEvents.Select(x => x.GetType().Name).ToList();

                        if (gotEvents.Length == expectedEvents.Length)
                        {
                            for (var index = 0; index < gotEvents.Length; index++)
                            {
                                var gotEvent = gotEvents[index];
                                var expectedEvent = expectedEvents[index];

                                // I don't know which Id I will get...
                                expectedEvent.Id = gotEvent.Id;

                                if (gotEvent.GetType() == expectedEvent.GetType())
                                {
                                    Assert.Equal(Serialize(expectedEvent), Serialize(gotEvent));
                                }
                                else
                                {
                                    Assert.True(false, $"Unexpected event(s) emitted: {string.Join(", ", gotEventNames.Except(expectedEventNames))}");
                                }
                            }
                        }
                        else if (gotEvents.Length < expectedEvents.Length)
                        {
                            Assert.True(false, $"Expected event(s) missing: {string.Join(", ", expectedEventNames.Except(gotEventNames))}");
                        }
                        else
                        {
                            Assert.True(false, $"Unexpected event(s) emitted: {string.Join(", ", gotEventNames.Except(expectedEventNames))}");
                        }
                        break;
                    case CqrsException ex:
                        Assert.True(false, ex.Message);
                        break;
                    case Exception ex:
                        var exceptionName = got.GetType().Name;
                        Assert.True(false, $"Expected events, but got exception {exceptionName}: {ex.Message} ");
                        break;
                    default:
                        Assert.True(false, "Internal error executing test, expected events or exception");
                        break;
                }
            };
        }

        /// <summary>
        /// 
        /// </summary>
        /// <typeparam name="TException"></typeparam>
        /// <param name="message"></param>
        /// <returns></returns>
        protected Action<object> ThenFailWithException<TException>(string message = null)
            where TException : Exception
        {
            return got =>
            {
                switch (got)
                {
                    case TException ex:
                        if (message != null && message != ex.Message)
                        {
                            Assert.True(false, $"expected exception message '{message}' got '{ex.Message}'");
                        }
                        break;
                    case CqrsException ex:
                        Assert.True(false, ex.Message);
                        break;
                    case Exception _:
                        Assert.True(false, $"Expected exception {typeof(TException).Name}, but got exception {got.GetType().Name}");
                        break;
                    case IEnumerable<AbstractEvent> _:
                        Assert.True(false, $"Expected exception {typeof(TException).Name}, but got event result");
                        break;
                    default:
                        Assert.True(false, "Internal error executing test, expected events or exception");
                        break;
                }
            };
        }

        /// <summary>
        /// 
        /// </summary>
        /// <param name="obj"></param>
        /// <returns></returns>
        private static string Serialize(object obj)
        {
            return JsonConvert.SerializeObject(obj);
        }

    }

}
