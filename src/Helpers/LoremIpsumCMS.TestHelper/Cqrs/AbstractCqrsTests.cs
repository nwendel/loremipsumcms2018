﻿#region License
// Copyright (c) Niklas Wendel 2017-2018
// 
// Licensed under the Apache License, Version 2.0 (the "License"); 
// you may not use this file except in compliance with the License. 
// You may obtain a copy of the License at 
// 
// http://www.apache.org/licenses/LICENSE-2.0 
// 
// Unless required by applicable law or agreed to in writing, software 
// distributed under the License is distributed on an "AS IS" BASIS, 
// WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied. 
// See the License for the specific language governing permissions and 
// limitations under the License.
#endregion
using LoremIpsumCMS.Infrastructure.Cqrs;
using LoremIpsumCMS.Infrastructure.Model;
using LoremIpsumCMS.Infrastructure.Raven.UniqueConstraints;
using Raven.Client.Documents.Session;
using System.Threading.Tasks;

namespace LoremIpsumCMS.TestHelper.Cqrs
{

    /// <summary>
    /// 
    /// </summary>
    public abstract class AbstractCqrsTests : AbstractRavenTests
    {

        /// <summary>
        /// 
        /// </summary>
        protected AbstractCqrsTests()
        {
            AsyncDocumentSession = DocumentStore.OpenAsyncSession(new SessionOptions
            {
                TransactionMode = TransactionMode.ClusterWide
            });
        }

        /// <summary>
        /// 
        /// </summary>
        protected IAsyncDocumentSession AsyncDocumentSession { get; }

        /// <summary>
        /// 
        /// </summary>
        /// <typeparam name="TAggregate"></typeparam>
        /// <param name="events"></param>
        protected async Task<TAggregate> CreateAggregateAsync<TAggregate>(params AbstractEvent[] events)
            where TAggregate : AbstractAggregate, new()
        {
            var aggregate = new TAggregate();
            foreach (var @event in events)
            {
                aggregate.ApplyEvent(@event);
            }

            await AsyncDocumentSession.StoreAsync(aggregate);
            await AsyncDocumentSession.SaveChangesExAsync();

            return aggregate;
        }

    }

}
