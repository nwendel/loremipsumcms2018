﻿#region License
// Copyright (c) Niklas Wendel 2017-2018
// 
// Licensed under the Apache License, Version 2.0 (the "License"); 
// you may not use this file except in compliance with the License. 
// You may obtain a copy of the License at 
// 
// http://www.apache.org/licenses/LICENSE-2.0 
// 
// Unless required by applicable law or agreed to in writing, software 
// distributed under the License is distributed on an "AS IS" BASIS, 
// WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied. 
// See the License for the specific language governing permissions and 
// limitations under the License.
#endregion
using LoremIpsumCMS.Web.TestHelper.Http.Features;
using Microsoft.AspNetCore.Http;
using Microsoft.AspNetCore.Http.Features;
using Microsoft.AspNetCore.Mvc;
using Microsoft.AspNetCore.Mvc.Abstractions;
using Microsoft.AspNetCore.Mvc.Filters;
using Microsoft.AspNetCore.Routing;
using Moq;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Linq.Expressions;
using System.Reflection;

namespace LoremIpsumCMS.Web.TestHelper.Http
{

    /// <summary>
    /// 
    /// </summary>
    public abstract class AbstractHttpContextTests
    {

        #region Constructor

        /// <summary>
        /// 
        /// </summary>
        protected AbstractHttpContextTests()
        {
            var featureCollection = new FeatureCollection();
            featureCollection.Set<ISessionFeature>(new DefaultSessionFeature());
            featureCollection.Set<IHttpRequestFeature>(new HttpRequestFeature());
            HttpContext = new DefaultHttpContext
            {
                Session = new InMemorySession()
            };

            HttpContextAccessorMock = new Mock<IHttpContextAccessor>();
            HttpContextAccessorMock
                .Setup(x => x.HttpContext)
                .Returns(HttpContext);
        }

        #endregion

        #region Properties

        /// <summary>
        /// 
        /// </summary>
        protected HttpContext HttpContext { get; }

        /// <summary>
        /// 
        /// </summary>
        protected Mock<IHttpContextAccessor> HttpContextAccessorMock { get; }

        #endregion

        #region Create Action Executing Context For

        /// <summary>
        /// 
        /// </summary>
        /// <typeparam name="TController"></typeparam>
        /// <param name="controller"></param>
        /// <param name="actionCallExpression"></param>
        /// <returns></returns>
        protected ActionExecutingContext CreateActionExecutingContextFor<TController>(TController controller, Expression<Func<TController, IActionResult>> actionCallExpression)
            where TController : ControllerBase
        {
            var methodCallExpression = GetInstanceMethodCallExpression(actionCallExpression);
            var methodInfo = methodCallExpression.Method;

            var actionDescriptor = GetActionDescriptor(methodInfo);
            var actionContext = new ActionContext(HttpContext, new RouteData(), actionDescriptor);
            var filters = new List<IFilterMetadata>();
            var actionArguments = GetActionArguments(methodInfo, methodCallExpression.Arguments);

            var actionExecutingContext = new ActionExecutingContext(actionContext, filters, actionArguments, controller);

            return actionExecutingContext;
        }

        /// <summary>
        /// 
        /// </summary>
        /// <param name="actionCallExpression"></param>
        /// <returns></returns>
        private static MethodCallExpression GetInstanceMethodCallExpression(LambdaExpression actionCallExpression)
        {
            if (!(actionCallExpression.Body is MethodCallExpression methodCallExpression))
            {
                throw new ArgumentException("Not a method call expression", nameof(actionCallExpression));
            }
            var objectInstance = methodCallExpression.Object;
            if (objectInstance == null)
            {
                throw new ArgumentException("Not an instance method call expression", nameof(actionCallExpression));
            }

            return methodCallExpression;
        }

        /// <summary>
        /// 
        /// </summary>
        /// <param name="methodInfo"></param>
        /// <param name="argumentExpressions"></param>
        /// <returns></returns>
        private static IDictionary<string, object> GetActionArguments(MethodInfo methodInfo, IList<Expression> argumentExpressions)
        {
            var arguments = argumentExpressions
                .Select(x => Expression.Lambda(x).Compile().DynamicInvoke())
                .ToArray();

            var actionArguments = new Dictionary<string, object>();
            var parameterNames = methodInfo.GetParameters()
                .Select(x => x.Name)
                .ToArray();

            for (var ix = 0; ix < arguments.Length; ix++)
            {
                actionArguments.Add(parameterNames[ix], arguments[ix]);
            }

            return actionArguments;
        }

        /// <summary>
        /// 
        /// </summary>
        /// <param name="methodInfo"></param>
        /// <returns></returns>
        private static ActionDescriptor GetActionDescriptor(MethodInfo methodInfo)
        {
            var parameters = methodInfo.GetParameters();
            var parameterDescriptors = parameters
                .Select(x => new ParameterDescriptor {ParameterType = x.ParameterType})
                .ToList();

            var actionDescriptor = new ActionDescriptor
            {
                RouteValues = { { "action", methodInfo.Name } },
                Parameters = parameterDescriptors
            };

            return actionDescriptor;
        }

        #endregion

    }

}
