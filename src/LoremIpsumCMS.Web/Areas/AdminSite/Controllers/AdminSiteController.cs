﻿#region License
// Copyright (c) Niklas Wendel 2017-2018
// 
// Licensed under the Apache License, Version 2.0 (the "License"); 
// you may not use this file except in compliance with the License. 
// You may obtain a copy of the License at 
// 
// http://www.apache.org/licenses/LICENSE-2.0 
// 
// Unless required by applicable law or agreed to in writing, software 
// distributed under the License is distributed on an "AS IS" BASIS, 
// WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied. 
// See the License for the specific language governing permissions and 
// limitations under the License.
#endregion
using AutoMapper;
using LoremIpsumCMS.Commands;
using LoremIpsumCMS.Infrastructure.AutoMapper;
using LoremIpsumCMS.Infrastructure.Cqrs;
using LoremIpsumCMS.Infrastructure.Raven;
using LoremIpsumCMS.Model;
using LoremIpsumCMS.Queries;
using LoremIpsumCMS.Web.Areas.AdminSite.ViewModels.AdminSite;
using LoremIpsumCMS.Web.Infrastructure.Http;
using LoremIpsumCMS.Web.Infrastructure.Validation;
using LoremIpsumCMS.Web.Mvc;
using Microsoft.AspNetCore.Http;
using Microsoft.AspNetCore.Mvc;
using Microsoft.Extensions.Options;
using Raven.Client.Documents;
using Raven.Client.Documents.Session;
using System;
using System.Linq;
using System.Threading.Tasks;

namespace LoremIpsumCMS.Web.Areas.AdminSite.Controllers
{

    /// <summary>
    /// 
    /// </summary>
    [Area(AreaNames.AdminSite)]
    [LoremIpsumAdminRoute]
    public class AdminSiteController : Controller
    {

        #region Dependencies

        private readonly IHostAccessor _hostAccessor;
        private readonly ICommandSender _commandSender;
        private readonly IMapper _mapper;
        private readonly IAsyncDocumentSession _asyncDocumentSession;
        private readonly IHttpContextAccessor _httpContextAccessor;
        private readonly IOptions<LoremIpsumOptions> _options;

        #endregion

        #region Constructor

        /// <summary>
        /// 
        /// </summary>
        /// <param name="hostAccessor"></param>
        /// <param name="commandSender"></param>
        /// <param name="mapper"></param>
        /// <param name="asyncDocumentSession"></param>
        /// <param name="httpContextAccessor"></param>
        /// <param name="options"></param>
        public AdminSiteController(
            IHostAccessor hostAccessor,
            ICommandSender commandSender,
            IMapper mapper,
            IAsyncDocumentSession asyncDocumentSession,
            IHttpContextAccessor httpContextAccessor,
            IOptions<LoremIpsumOptions> options)
        {
            _hostAccessor = hostAccessor;
            _commandSender = commandSender;
            _mapper = mapper;
            _asyncDocumentSession = asyncDocumentSession;
            _httpContextAccessor = httpContextAccessor;
            _options = options;
        }

        #endregion

        #region Index

        /// <summary>
        /// 
        /// </summary>
        /// <returns></returns>
        //[Authorize(Policy = nameof(Permissions.ManageSites))]
        [HttpGet("sites")]
        public async Task<IActionResult> Index()
        {
            var sites = await _asyncDocumentSession.Query<Site>()
                .Customize(o => o.WaitForNonStaleResults())
                .ToListAsync();

            var viewModel = new IndexViewModel
            {
                Sites = sites
                    .MapTo<IndexSiteViewModel>(_mapper)
                    .ToArray()
            };

            return View(viewModel);
        }

        #endregion

        #region Create

        /// <summary>
        /// 
        /// </summary>
        /// <returns></returns>
        [HttpGet("site/create")]
        public IActionResult Create()
        {
            var host = _hostAccessor.CurrentHost.Host;

            var viewModel = new CreateViewModel
            {
                Hosts = new[]
                {
                    new SiteHostViewModel
                    {
                        Name = host,
                        IsAdmin = true
                    }
                }
            };

            return View(viewModel);
        }

        /// <summary>
        /// 
        /// </summary>
        /// <param name="viewModel"></param>
        /// <returns></returns>
        public IActionResult InvalidCreate(CreateViewModel viewModel)
        {
            return View(nameof(Create), viewModel);
        }

        /// <summary>
        /// 
        /// </summary>
        /// <returns></returns>
        [HttpPost("site/create")]
        public async Task<IActionResult> Create(CreateViewModel viewModel)
        {
            var hosts = viewModel.Hosts
                .Where(x => !x.IsDeleted)
                .MapTo<SiteHost>(_mapper)
                .ToList();

            var command = new CreateSiteCommand
            {
                Name = viewModel.Name,
                Hosts = hosts,
                CreatedAt = DateTime.Now
            };
            await _commandSender.SendAsync(command);

            return RedirectToAction(nameof(Index));
        }

        #endregion

        #region Edit

        /// <summary>
        /// 
        /// </summary>
        /// <param name="site"></param>
        /// <returns></returns>
        public ValidationResult ValidateEdit(Site site)
        {
            if (site == null)
            {
                return ValidationResult.NotFound;
            }
            return ValidationResult.Success;
        }

        /// <summary>
        /// 
        /// </summary>
        /// <param name="site"></param>
        /// <returns></returns>
        //[AuthorizeResource(Policy = nameof(Permissions.ManageSite))]
        [HttpGet("site/edit")]
        public IActionResult Edit(Site site)
        {
            var viewModel = new EditViewModel
            {
                Name = site.Name,
                OldNameSlug = site.NameSlug,
                Hosts = site.Hosts
                    .MapTo<SiteHostViewModel>(_mapper)
                    .ToArray()
            };
            return View(viewModel);
        }

        /// <summary>
        /// 
        /// </summary>
        /// <param name="viewModel"></param>
        /// <returns></returns>
        public IActionResult InvalidEdit(EditViewModel viewModel)
        {
            return View(nameof(Edit), viewModel);
        }

        /// <summary>
        /// 
        /// </summary>
        /// <returns></returns>
        //[AuthorizeResource(Policy = nameof(Permissions.ManageSite))]
        [HttpPost("site/edit")]
        public async Task<IActionResult> Edit(EditViewModel viewModel)
        {
            var site = await _asyncDocumentSession.LoadByAsync<Site>(x => x.NameSlug(viewModel.OldNameSlug));
            var hosts = viewModel.Hosts
                .Where(x => !x.IsDeleted)
                .MapTo<SiteHost>(_mapper)
                .ToList();

            var command = new UpdateSiteCommand
            {
                Id = site.Id,
                Name = viewModel.Name,
                Hosts = hosts,
                UpdatedAt = DateTime.Now
            };
            await _commandSender.SendAsync(command);

            return RedirectToAction(nameof(Index));
        }

        #endregion

        #region Delete

        /// <summary>
        /// 
        /// </summary>
        /// <param name="viewModel"></param>
        /// <returns></returns>
        //[Authorize(Policy = nameof(Permissions.ManageSites))]
        [HttpPost("site/delete")]
        public async Task<IActionResult> Delete(DeleteViewModel viewModel)
        {
            var site = await _asyncDocumentSession.LoadByAsync<Site>(x => x.NameSlug(viewModel.NameSlug));

            var command = new DeleteSiteCommand
            {
                Id = site.Id
            };
            await _commandSender.SendAsync(command);

            return RedirectToAction(nameof(Index));
        }

        #endregion

        #region Navigate To

        /// <summary>
        /// 
        /// </summary>
        /// <param name="nameSlug"></param>
        /// <returns></returns>
        [HttpGet("site/navigate-to/{nameSlug}")]
        public async Task<IActionResult> NavigateTo(string nameSlug)
        {
            var site = await _asyncDocumentSession.LoadByAsync<Site>(x => x.NameSlug(nameSlug));
            var adminHost = site.Hosts.First(x => x.IsAdmin);

            var request = _httpContextAccessor.HttpContext.Request;
            var path = Url.Action(nameof(Index));

            if (_hostAccessor.HasOverride)
            {
                var queryParameterName = _options.Value.HostnameOverrideQueryParameterName;
                return Redirect($"{request.Scheme}://{request.Host}{path}?{queryParameterName}={adminHost.Name}");
            }

            return Redirect($"{request.Scheme}://{adminHost.Name}{path}");
        }

        #endregion

    }

}
