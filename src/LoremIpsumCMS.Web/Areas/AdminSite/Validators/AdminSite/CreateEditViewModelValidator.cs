﻿#region License
// Copyright (c) Niklas Wendel 2017-2019
// 
// Licensed under the Apache License, Version 2.0 (the "License"); 
// you may not use this file except in compliance with the License. 
// You may obtain a copy of the License at 
// 
// http://www.apache.org/licenses/LICENSE-2.0 
// 
// Unless required by applicable law or agreed to in writing, software 
// distributed under the License is distributed on an "AS IS" BASIS, 
// WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied. 
// See the License for the specific language governing permissions and 
// limitations under the License.
#endregion
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using LoremIpsumCMS.Infrastructure.Linq;
using LoremIpsumCMS.Infrastructure.Validation;
using LoremIpsumCMS.Web.Areas.AdminSite.ViewModels.AdminSite;

namespace LoremIpsumCMS.Web.Areas.AdminSite.Validators.AdminSite
{

    /// <summary>
    /// 
    /// </summary>
    public class CreateEditViewModelValidator : AbstractValidator<CreateViewModel>
    {

        #region Constructor

        /// <summary>
        /// 
        /// </summary>
        public CreateEditViewModelValidator()
        {
            RuleFor(x => x.Name)
                .NotNull()
                .WithMessage("Name is required");
            RuleFor(x => x.Hosts)
                .NotNull();

            ValidateUsing(context => Task.FromResult(ValidateHosts(context)));
        }

        #endregion

        #region Validate Hosts

        /// <summary>
        /// 
        /// </summary>
        /// <param name="context"></param>
        /// <returns></returns>
        private IEnumerable<ValidationMessage> ValidateHosts(IValidationContext<CreateViewModel> context)
        {
            var hosts = context.Instance.Hosts;
            if (hosts == null)
            {
                yield break;
            }
            hosts = hosts
                .Where(x => !x.IsDeleted)
                .ToArray();

            if (hosts.None())
            {
                yield return ValidationMessage.Create<CreateViewModel>(x => x.Hosts, "Must have at least one host");
            }
            if (hosts.Any(x => x.Name == null))
            {
                yield return ValidationMessage.Create<CreateViewModel>(x => x.Hosts, "Hostname is required");
            }

            // TODO: This should only be checked in Edit?

            //// Check that current request host is defined as admin
            //var currentRequestHostIsAdmin = false;
            //var currentRequestHostname = _hostAccessor.CurrentHost.Host;
            //foreach (var host in hosts)
            //{
            //    if (host.Name == null)
            //    {
            //        continue;
            //    }
            //    if (host.Name == currentRequestHostname && host.IsAdmin)
            //    {
            //        currentRequestHostIsAdmin = true;
            //    }
            //}
            //if (!currentRequestHostIsAdmin)
            //{
            //    yield return new ValidationMessage<T>(x => x.Hosts, "Current host is not defined as admin");
            //}
        }

        #endregion

    }

}
