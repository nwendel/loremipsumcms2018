﻿#region License
// Copyright (c) Niklas Wendel 2017-2018
// 
// Licensed under the Apache License, Version 2.0 (the "License"); 
// you may not use this file except in compliance with the License. 
// You may obtain a copy of the License at 
// 
// http://www.apache.org/licenses/LICENSE-2.0 
// 
// Unless required by applicable law or agreed to in writing, software 
// distributed under the License is distributed on an "AS IS" BASIS, 
// WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied. 
// See the License for the specific language governing permissions and 
// limitations under the License.
#endregion
using LoremIpsumCMS.Infrastructure;
using LoremIpsumCMS.Infrastructure.Collections;
using LoremIpsumCMS.Infrastructure.Raven;
using LoremIpsumCMS.Infrastructure.Validation;
using LoremIpsumCMS.Model;
using LoremIpsumCMS.Queries;
using LoremIpsumCMS.Web.Areas.AdminSite.ViewModels.AdminSite;
using Raven.Client.Documents.Session;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace LoremIpsumCMS.Web.Areas.AdminSite.Validators.AdminSite
{

    /// <summary>
    /// 
    /// </summary>
    public class CreateViewModelValidator : AbstractValidator<CreateViewModel>
    {

        #region Dependencies

        private readonly IAsyncDocumentSession _asyncDocumentSession;

        #endregion

        #region Constructor

        /// <summary>
        /// 
        /// </summary>
        /// <param name="asyncDocumentSession"></param>
        public CreateViewModelValidator(IAsyncDocumentSession asyncDocumentSession)
        {
            _asyncDocumentSession = asyncDocumentSession;

            Include<CreateEditViewModelValidator>();

            ValidateUsing(ValidateNameUnique);
            ValidateUsing(ValidateHostsUnique);
        }

        #endregion

        #region Validate Name Unique

        /// <summary>
        /// 
        /// </summary>
        /// <param name="context"></param>
        /// <returns></returns>
        private Task<IEnumerable<ValidationMessage>> ValidateNameUnique(IValidationContext<CreateViewModel> context)
        {
            return AsyncEnumerable.Create<ValidationMessage>(async yield =>
            {
                var siteName = context.Instance.Name;
                if (string.IsNullOrWhiteSpace(siteName))
                {
                    return;
                }

                var siteNameSlug = siteName.Slugify();
                var site = await _asyncDocumentSession.LoadByAsync<Site>(x => x.NameSlug(siteNameSlug));
                if (site != null)
                {
                    yield.Return(ValidationMessage.Create<CreateViewModel>(x => x.Name, "Name must be unique"));
                }
            });
        }

        #endregion

        #region Validate Hosts Unique

        /// <summary>
        /// 
        /// </summary>
        /// <param name="context"></param>
        /// <returns></returns>
        private Task<IEnumerable<ValidationMessage>> ValidateHostsUnique(IValidationContext<CreateViewModel> context)
        {
            return AsyncEnumerable.Create<ValidationMessage>(async yield =>
            {
                if (context.Instance.Hosts == null)
                {
                    return;
                }

                var hosts = context.Instance.Hosts
                    .Where(x => !x.IsDeleted)
                    .ToList();
                foreach (var host in hosts)
                {
                    if (host.Name == null)
                    {
                        continue;
                    }
                    var existingHost = await _asyncDocumentSession.LoadByAsync<Host>(x => x.Name(host.Name));
                    if (existingHost != null)
                    {
                        yield.Return(ValidationMessage.Create<Site>(x => x.Hosts, "Hostnames must be unique"));
                    }
                }
            });
        }

        #endregion

    }

}
