﻿#region License
// Copyright (c) Niklas Wendel 2017-2018
// 
// Licensed under the Apache License, Version 2.0 (the "License"); 
// you may not use this file except in compliance with the License. 
// You may obtain a copy of the License at 
// 
// http://www.apache.org/licenses/LICENSE-2.0 
// 
// Unless required by applicable law or agreed to in writing, software 
// distributed under the License is distributed on an "AS IS" BASIS, 
// WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied. 
// See the License for the specific language governing permissions and 
// limitations under the License.
#endregion
using Microsoft.AspNetCore.Http;
using Microsoft.Extensions.Options;
using System.Threading.Tasks;

namespace LoremIpsumCMS.Web.Infrastructure.Http
{

    /// <summary>
    /// 
    /// </summary>
    public class HostnameOverrideMiddleware : IMiddleware
    {

        #region Dependencies

        private readonly IHostAccessor _hostAccessor;
        private readonly IOptions<LoremIpsumOptions> _options;

        #endregion

        #region Constructor

        /// <summary>
        /// 
        /// </summary>
        /// <param name="hostAccessor"></param>
        /// <param name="options"></param>
        public HostnameOverrideMiddleware(IHostAccessor hostAccessor, IOptions<LoremIpsumOptions> options)
        {
            _hostAccessor = hostAccessor;
            _options = options;
        }

        #endregion

        #region Invoke Async

        /// <summary>
        /// 
        /// </summary>
        /// <param name="context"></param>
        /// <param name="next"></param>
        /// <returns></returns>
        public Task InvokeAsync(HttpContext context, RequestDelegate next)
        {
            if (_options.Value.IsHostnameOverrideEnabled && _hostAccessor.IsLocalhost)
            {
                var requestQuery = context.Request.Query;
                var parameterName = _options.Value.HostnameOverrideQueryParameterName;
                if (requestQuery.TryGetValue(parameterName, out var value))
                {
                    context.Session.SetHostnameOverride(value.ToString());
                }
            }

            return next(context);
        }

        #endregion

    }

}
