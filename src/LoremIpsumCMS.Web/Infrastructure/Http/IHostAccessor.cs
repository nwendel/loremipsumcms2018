﻿#region License
// Copyright (c) Niklas Wendel 2017-2018
// 
// Licensed under the Apache License, Version 2.0 (the "License"); 
// you may not use this file except in compliance with the License. 
// You may obtain a copy of the License at 
// 
// http://www.apache.org/licenses/LICENSE-2.0 
// 
// Unless required by applicable law or agreed to in writing, software 
// distributed under the License is distributed on an "AS IS" BASIS, 
// WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied. 
// See the License for the specific language governing permissions and 
// limitations under the License.
#endregion
using Microsoft.AspNetCore.Http;

namespace LoremIpsumCMS.Web.Infrastructure.Http
{

    /// <summary>
    /// 
    /// </summary>
    public interface IHostAccessor
    {

        #region Current Host

        /// <summary>
        /// 
        /// </summary>
        HostString CurrentHost { get; }

        #endregion

        #region Is Localhost

        /// <summary>
        /// 
        /// </summary>
        bool IsLocalhost { get; }

        #endregion

        #region Has Override

        /// <summary>
        /// 
        /// </summary>
        bool HasOverride { get; }

        #endregion

        #region Override Host

        /// <summary>
        /// 
        /// </summary>
        string OverrideHost { get; }

        #endregion

    }

}
