﻿#region License
// Copyright (c) Niklas Wendel 2017-2018
// 
// Licensed under the Apache License, Version 2.0 (the "License"); 
// you may not use this file except in compliance with the License. 
// You may obtain a copy of the License at 
// 
// http://www.apache.org/licenses/LICENSE-2.0 
// 
// Unless required by applicable law or agreed to in writing, software 
// distributed under the License is distributed on an "AS IS" BASIS, 
// WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied. 
// See the License for the specific language governing permissions and 
// limitations under the License.
#endregion
using LoremIpsumCMS.Infrastructure;
using Microsoft.AspNetCore.Mvc;

namespace LoremIpsumCMS.Web.Infrastructure.Mvc
{

    /// <summary>
    /// 
    /// </summary>
    public static class ControllerName
    {

        #region For

        /// <summary>
        /// 
        /// </summary>
        /// <typeparam name="TController"></typeparam>
        /// <returns></returns>
        public static string For<TController>()
            where TController : ControllerBase
        {
            var controllerTypeName = typeof(TController).Name;
            var controllerName = controllerTypeName.TrimEndsWith(nameof(Controller));
            return controllerName;
        }

        #endregion

    }

}
