﻿#region License
// Copyright (c) Niklas Wendel 2017-2018
// 
// Licensed under the Apache License, Version 2.0 (the "License"); 
// you may not use this file except in compliance with the License. 
// You may obtain a copy of the License at 
// 
// http://www.apache.org/licenses/LICENSE-2.0 
// 
// Unless required by applicable law or agreed to in writing, software 
// distributed under the License is distributed on an "AS IS" BASIS, 
// WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied. 
// See the License for the specific language governing permissions and 
// limitations under the License.
#endregion

using System;
using LoremIpsumCMS.Web.Infrastructure.Mvc.Rendering.Internal;
using Microsoft.AspNetCore.Html;
using Microsoft.AspNetCore.Mvc;
using System.Threading.Tasks;

namespace LoremIpsumCMS.Web.Infrastructure.Mvc.Rendering
{

    /// <summary>
    /// 
    /// </summary>
    public static class ViewComponentActionHelperExtensions
    {

        #region Action

        /// <summary>
        /// 
        /// </summary>
        /// <param name="self"></param>
        /// <param name="actionName"></param>
        /// <param name="controllerName"></param>
        /// <returns></returns>
        public static Task<IHtmlContent> ActionAsync(this IViewComponentHelper self, string actionName, string controllerName)
        {
            return ActionCoreAsync(self, actionName, controllerName, null);
        }

        /// <summary>
        /// 
        /// </summary>
        /// <param name="self"></param>
        /// <param name="controllerName"></param>
        /// <param name="actionName"></param>
        /// <param name="routeValues"></param>
        /// <returns></returns>
        public static Task<IHtmlContent> ActionAsync(this IViewComponentHelper self, string actionName, string controllerName, object routeValues)
        {
            if (routeValues == null)
            {
                throw new ArgumentNullException(nameof(routeValues));
            }

            return ActionCoreAsync(self, actionName, controllerName, routeValues);
        }

        #endregion

        #region Action Core

        /// <summary>
        /// 
        /// </summary>
        /// <param name="self"></param>
        /// <param name="actionName"></param>
        /// <param name="controllerName"></param>
        /// <param name="routeValues"></param>
        /// <returns></returns>
        private static Task<IHtmlContent> ActionCoreAsync(this IViewComponentHelper self, string actionName, string controllerName, object routeValues)
        {
            if (string.IsNullOrEmpty(actionName))
            {
                throw new ArgumentNullException(nameof(actionName));
            }
            if (string.IsNullOrEmpty(controllerName))
            {
                throw new ArgumentNullException(nameof(controllerName));
            }

            return self.InvokeAsync(typeof(ActionViewComponent), new {ControllerName = controllerName, ActionName = actionName, RouteValues = routeValues});
        }

        #endregion

    }

}
