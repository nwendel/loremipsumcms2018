﻿#region License
// Copyright (c) Niklas Wendel 2017-2018
// 
// Licensed under the Apache License, Version 2.0 (the "License"); 
// you may not use this file except in compliance with the License. 
// You may obtain a copy of the License at 
// 
// http://www.apache.org/licenses/LICENSE-2.0 
// 
// Unless required by applicable law or agreed to in writing, software 
// distributed under the License is distributed on an "AS IS" BASIS, 
// WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied. 
// See the License for the specific language governing permissions and 
// limitations under the License.
#endregion
using LoremIpsumCMS.Infrastructure.Proxy;
using LoremIpsumCMS.Infrastructure.Raven;
using Microsoft.AspNetCore.Http;
using Microsoft.Extensions.Options;
using Raven.Client.Documents;
using Raven.Client.Documents.Session;

namespace LoremIpsumCMS.Web.Infrastructure.Raven
{

    /// <summary>
    /// 
    /// </summary>
    public class AsyncDocumentSessionFactory
    {

        #region Dependencies

        private readonly IDocumentStore _documentStore;
        private readonly IHttpContextAccessor _httpContextAccessor;
        private readonly IAsyncDocumentSessionHolder _asyncDocumentSessionHolder;
        private readonly IOptions<LoremIpsumOptions> _options;

        #endregion

        #region Constructor

        /// <summary>
        /// 
        /// </summary>
        /// <param name="documentStore"></param>
        /// <param name="httpContextAccessor"></param>
        /// <param name="asyncDocumentSessionHolder"></param>
        /// <param name="options"></param>
        public AsyncDocumentSessionFactory(
            IDocumentStore documentStore,
            IHttpContextAccessor httpContextAccessor,
            IAsyncDocumentSessionHolder asyncDocumentSessionHolder,
            IOptions<LoremIpsumOptions> options)
        {
            _documentStore = documentStore;
            _httpContextAccessor = httpContextAccessor;
            _asyncDocumentSessionHolder = asyncDocumentSessionHolder;
            _options = options;
        }

        #endregion

        #region Create Async Document Session

        /// <summary>
        /// 
        /// </summary>
        public IAsyncDocumentSession CreateAsyncDocumentSession()
        {
            return ProxyBuilder
                .For<IAsyncDocumentSession>()
                .WithTarget(AsyncDocumentSessionAccessor)
                //.UseInterceptor<AsyncDocumentSessionUniqueConstraintInterceptor>()
                .Build();
        }

        private IAsyncDocumentSession AsyncDocumentSessionAccessor()
        {
            var httpContext = _httpContextAccessor.HttpContext;
            if (httpContext == null)
            {
                // TODO: Is it correct not to create a session here?
                return _asyncDocumentSessionHolder.AsyncDocumentSession;
            }

            var asyncDocumentSession = httpContext.GetAsyncDocumentSession();
            if (asyncDocumentSession == null)
            {
                asyncDocumentSession = _documentStore.OpenAsyncSession(_options.Value.RavenDatabase);
                httpContext.SetAsyncDocumentSession(asyncDocumentSession);
            }
            return asyncDocumentSession;
        }

        #endregion

    }

}
