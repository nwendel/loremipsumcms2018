﻿#region License
// Copyright (c) Niklas Wendel 2017-2018
// 
// Licensed under the Apache License, Version 2.0 (the "License"); 
// you may not use this file except in compliance with the License. 
// You may obtain a copy of the License at 
// 
// http://www.apache.org/licenses/LICENSE-2.0 
// 
// Unless required by applicable law or agreed to in writing, software 
// distributed under the License is distributed on an "AS IS" BASIS, 
// WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied. 
// See the License for the specific language governing permissions and 
// limitations under the License.
#endregion
using LoremIpsumCMS.Infrastructure.Raven.UniqueConstraints;
using Microsoft.AspNetCore.Hosting;
using Microsoft.Extensions.Options;
using Raven.Client.Documents;
using Raven.Client.Documents.Operations;
using Raven.Client.Exceptions.Database;
using Raven.Client.ServerWide;
using Raven.Client.ServerWide.Operations;
using System;

namespace LoremIpsumCMS.Web.Infrastructure.Raven
{

    /// <summary>
    /// 
    /// </summary>
    public class DocumentStoreFactory
    {

        #region Dependencies

        private readonly IOptions<LoremIpsumOptions> _options;
        private readonly IApplicationLifetime _applicationLifetime;

        #endregion

        #region Fields

        private readonly Lazy<IDocumentStore> _documentStore;

        #endregion

        #region Constructor

        /// <summary>
        /// 
        /// </summary>
        /// <param name="options"></param>
        /// <param name="applicationLifetime"></param>
        public DocumentStoreFactory(
            IOptions<LoremIpsumOptions> options,
            IApplicationLifetime applicationLifetime)
        {
            _options = options;
            _applicationLifetime = applicationLifetime;

            _documentStore = new Lazy<IDocumentStore>(CreateInternal);
        }

        #endregion

        #region Create

        /// <summary>
        /// 
        /// </summary>
        /// <returns></returns>
        public IDocumentStore CreateDocumentStore()
        {
            return _documentStore.Value;
        }

        /// <summary>
        /// 
        /// </summary>
        /// <returns></returns>
        private IDocumentStore CreateInternal()
        {
            var documentStore = new DocumentStore
            {
                Urls = new [] { _options.Value.RavenUrl },
                Database = _options.Value.RavenDatabase
            };
            UniqueConstraintListener.RegisterFor(documentStore);
            documentStore.Initialize();

            // TODO: Raven4
            //documentStore.DatabaseCommands.GlobalAdmin.EnsureDatabaseExists(_options.Value.RavenDatabase);

            var database = _options.Value.RavenDatabase;
            try
            {
                documentStore.Maintenance.ForDatabase(database).Send(new GetStatisticsOperation());
            }
            catch (DatabaseDoesNotExistException)
            {
                documentStore.Maintenance.Server.Send(new CreateDatabaseOperation(new DatabaseRecord(database)));
            }

            _applicationLifetime.ApplicationStopped.Register(DisposeDocumentStore);

            return documentStore;
        }

        #endregion

        #region Dispose Document Store

        /// <summary>
        /// 
        /// </summary>
        private void DisposeDocumentStore()
        {
            _documentStore.Value.Dispose();
        }

        #endregion

    }

}
