﻿#region License
// Copyright (c) Niklas Wendel 2017-2018
// 
// Licensed under the Apache License, Version 2.0 (the "License"); 
// you may not use this file except in compliance with the License. 
// You may obtain a copy of the License at 
// 
// http://www.apache.org/licenses/LICENSE-2.0 
// 
// Unless required by applicable law or agreed to in writing, software 
// distributed under the License is distributed on an "AS IS" BASIS, 
// WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied. 
// See the License for the specific language governing permissions and 
// limitations under the License.
#endregion
using FluentRegistration;
using Microsoft.Extensions.DependencyInjection;
using Raven.Client.Documents;
using Raven.Client.Documents.Session;

namespace LoremIpsumCMS.Web.Infrastructure.Raven
{

    /// <summary>
    /// 
    /// </summary>
    public class RavenWebServiceInstaller : IServiceInstaller
    {

        #region Install

        /// <summary>
        /// 
        /// </summary>
        /// <param name="services"></param>
        public void Install(IServiceCollection services)
        {
            services.Register(c => c
                .For<IDocumentStore>()
                .UsingFactory<DocumentStoreFactory>(f => f.CreateDocumentStore()));

            services.Register(c => c
                .For<IAsyncDocumentSession>()
                .UsingFactory<AsyncDocumentSessionFactory>(f => f.CreateAsyncDocumentSession()));
            
            services.Register(r => r
                .ImplementedBy<RavenWebInitializer>()
                .WithServices.AllInterfaces());

            services.Register(r => r
                .For<RavenSessionMiddleware>()
                .ImplementedBy<RavenSessionMiddleware>()
                .Lifetime.Transient());
        }

        #endregion

    }

}
