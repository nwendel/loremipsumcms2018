﻿#region License
// Copyright (c) Niklas Wendel 2017-2019
// 
// Licensed under the Apache License, Version 2.0 (the "License"); 
// you may not use this file except in compliance with the License. 
// You may obtain a copy of the License at 
// 
// http://www.apache.org/licenses/LICENSE-2.0 
// 
// Unless required by applicable law or agreed to in writing, software 
// distributed under the License is distributed on an "AS IS" BASIS, 
// WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied. 
// See the License for the specific language governing permissions and 
// limitations under the License.
#endregion
using LoremIpsumCMS.Infrastructure;
using LoremIpsumCMS.Infrastructure.Linq;
using LoremIpsumCMS.Infrastructure.Reflection;
using LoremIpsumCMS.Infrastructure.Validation;
using LoremIpsumCMS.Web.Infrastructure.Mvc;
using LoremIpsumCMS.Web.Infrastructure.Mvc.ModelBinding;
using Microsoft.AspNetCore.Mvc;
using Microsoft.AspNetCore.Mvc.Filters;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Reflection;
using System.Threading.Tasks;

namespace LoremIpsumCMS.Web.Infrastructure.Validation
{

    /// <summary>
    /// 
    /// </summary>
    public class ValidateActionFilterAttribute : ActionFilterAttribute
    {

        #region Dependencies

        private readonly IValidationService _validationService;

        #endregion

        #region Constructor

        /// <summary>
        /// 
        /// </summary>
        public ValidateActionFilterAttribute(IValidationService validationService)
        {
            _validationService = validationService;
            Order = FilterOrder.ValidateActionFilterAttributeOrder;
        }

        #endregion

        #region On Action Execution Async

        /// <summary>
        /// 
        /// </summary>
        /// <param name="context"></param>
        /// <param name="next"></param>
        /// <returns></returns>
        public override async Task OnActionExecutionAsync(ActionExecutingContext context, ActionExecutionDelegate next)
        {
            await ValidateAsync(context);
            await base.OnActionExecutionAsync(context, next);
        }

        #endregion

        #region Validate Async

        /// <summary>
        /// 
        /// </summary>
        /// <param name="context"></param>
        private async Task ValidateAsync(ActionExecutingContext context)
        {
            var actionDescriptor = context.ActionDescriptor;
            var actionName = actionDescriptor.RouteValues["action"].TrimEndsWith("Async");
            var actionParameters = actionDescriptor.Parameters;
            var actionParameterTypes = actionParameters.Select(x => x.ParameterType).ToArray();
            var actionArgumentValues = context.ActionArguments.Values.ToArray();
            var controller = (Controller)context.Controller;
            var controllerType = controller.GetType();

            await ValidateArgumentsAsync(controller, actionArgumentValues);
            var result = await InvokeControllerValidateAsync(controller, controllerType, actionName, actionParameterTypes, actionArgumentValues);
            if (result.IsRedirect)
            {
                context.Result = result.RedirectActionResult;
            }
            else if (!controller.ModelState.IsValid)
            {
                await InvokeControllerInvalidAsync(context, controller, controllerType, actionName, actionParameterTypes, actionArgumentValues);
            }
        }

        #endregion

        #region Validate Arguments Async

        /// <summary>
        /// 
        /// </summary>
        /// <param name="controller"></param>
        /// <param name="actionArgumentValues"></param>
        private async Task ValidateArgumentsAsync(Controller controller, IEnumerable<object> actionArgumentValues)
        {
            foreach (var argument in actionArgumentValues.Where(x => x != null))
            {
                var messages = await _validationService.ValidateAsync(argument).ToListAsync();
                controller.ModelState.AddModelErrors(messages);
            }
        }

        #endregion

        #region Invoke Controller Validate Async

        /// <summary>
        /// 
        /// </summary>
        /// <param name="controller"></param>
        /// <param name="controllerType"></param>
        /// <param name="actionName"></param>
        /// <param name="actionParameterTypes"></param>
        /// <param name="actionArgumentValues"></param>
        /// <returns></returns>
        private async Task<ValidationResult> InvokeControllerValidateAsync(
            Controller controller, Type controllerType,
            string actionName, Type[] actionParameterTypes, object[] actionArgumentValues)
        {
            var methodInfo = GetControllerMethod<ValidationResult>($"Validate{actionName}", controllerType, actionParameterTypes);
            if (methodInfo == null)
            {
                return ValidationResult.Success;
            }

            var result = await InvokeControllerMethodAsync<ValidationResult>(methodInfo, controller, actionArgumentValues);
            if (result.HasMessages)
            {
                controller.ModelState.AddModelErrors(result.Messages);
            }
            return result;
        }

        #endregion

        #region Invoke Controller Invalid Async

        /// <summary>
        /// 
        /// </summary>
        /// <param name="context"></param>
        /// <param name="controller"></param>
        /// <param name="controllerType"></param>
        /// <param name="actionName"></param>
        /// <param name="actionParameterTypes"></param>
        /// <param name="actionArgumentValues"></param>
        private async Task InvokeControllerInvalidAsync(
            ActionExecutingContext context, Controller controller, Type controllerType,
            string actionName, Type[] actionParameterTypes, object[] actionArgumentValues)
        {
            var methodInfo = GetControllerMethod<IActionResult>($"Invalid{actionName}", controllerType, actionParameterTypes);
            if (methodInfo == null)
            {
                throw new ValidationMethodException($"Method Invalid{actionName} is missing from controller {controllerType.FullName}");
            }

            var actionResult = await InvokeControllerMethodAsync<IActionResult>(methodInfo, controller, actionArgumentValues);
            context.Result = actionResult;
        }

        #endregion

        #region Get Controller Method

        /// <summary>
        /// 
        /// </summary>
        /// <param name="methodName"></param>
        /// <param name="controllerType"></param>
        /// <param name="parameterTypes"></param>
        /// <returns></returns>
        private static MethodInfo GetControllerMethod<TResult>(string methodName, Type controllerType, Type[] parameterTypes)
        {
            var methodInfo = controllerType.GetMethod(methodName, parameterTypes);
            var asyncMethodInfo = controllerType.GetMethod($"{methodName}Async", parameterTypes);

            if (methodInfo != null && asyncMethodInfo != null)
            {
                throw new ValidationMethodException($"Ambiguous methods {methodName} and {methodName}Async");
            }

            methodInfo = methodInfo ?? asyncMethodInfo;
            if (methodInfo == null)
            {
                return null;
            }

            if (methodInfo.ReturnType != typeof(TResult) && methodInfo.ReturnType != typeof(Task<TResult>))
            {
                throw new ValidationMethodException($"Invalid return type for method {methodName}");
            }
            return methodInfo;
        }

        #endregion

        #region Invoke Controller Method Async

        /// <summary>
        /// 
        /// </summary>
        /// <typeparam name="TResult"></typeparam>
        /// <param name="methodInfo"></param>
        /// <param name="controller"></param>
        /// <param name="argumentValues"></param>
        /// <returns></returns>
        private async Task<TResult> InvokeControllerMethodAsync<TResult>(MethodInfo methodInfo, Controller controller, object[] argumentValues)
            where TResult : class
        {
            var result = methodInfo.ReturnType == typeof(Task<TResult>)
                ? await methodInfo.InvokeAsync<TResult>(controller, argumentValues)
                : (TResult) methodInfo.InvokeAndUnwrapException(controller, argumentValues);

            if (result == null)
            {
                throw new ValidationMethodException($"Method {methodInfo.Name} cannot return null");
            }

            return result;
        }

        #endregion

    }

}
