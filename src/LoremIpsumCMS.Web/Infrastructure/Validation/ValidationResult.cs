﻿#region License
// Copyright (c) Niklas Wendel 2017-2019
// 
// Licensed under the Apache License, Version 2.0 (the "License"); 
// you may not use this file except in compliance with the License. 
// You may obtain a copy of the License at 
// 
// http://www.apache.org/licenses/LICENSE-2.0 
// 
// Unless required by applicable law or agreed to in writing, software 
// distributed under the License is distributed on an "AS IS" BASIS, 
// WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied. 
// See the License for the specific language governing permissions and 
// limitations under the License.
#endregion
using LoremIpsumCMS.Infrastructure.Linq;
using LoremIpsumCMS.Infrastructure.Validation;
using Microsoft.AspNetCore.Mvc;
using System.Collections.Generic;
using System.Linq;

namespace LoremIpsumCMS.Web.Infrastructure.Validation
{

    /// <summary>
    /// 
    /// </summary>
    public class ValidationResult
    {

        #region Fields

        private readonly List<ValidationMessage> _messages = new List<ValidationMessage>();

        #endregion

        #region Static Results

        /// <summary>
        /// 
        /// </summary>
        public static readonly ValidationResult Success = new ValidationResult();

        /// <summary>
        /// 
        /// </summary>
        public static readonly ValidationResult NotFound = new ValidationResult { RedirectActionResult = new NotFoundResult() };

        /// <summary>
        /// 
        /// </summary>
        public static readonly ValidationResult Forbid = new ValidationResult { RedirectActionResult = new ForbidResult() };

        /// <summary>
        /// 
        /// </summary>
        public static readonly ValidationResult BadRequest = new ValidationResult { RedirectActionResult = new BadRequestResult() };

        /// <summary>
        /// 
        /// </summary>
        public static ValidationResult Message(string message)
        {
            var validationResult = new ValidationResult();
            validationResult._messages.Add(ValidationMessage.Create(message));
            return validationResult;
        }

        #endregion

        #region Properties

        /// <summary>
        /// 
        /// </summary>
        public bool IsSuccess => _messages.None() && !IsRedirect;

        /// <summary>
        /// 
        /// </summary>
        public bool HasMessages => _messages.Any();

        /// <summary>
        /// 
        /// </summary>
        public IEnumerable<ValidationMessage> Messages => _messages.AsReadOnly();

        /// <summary>
        /// 
        /// </summary>
        public bool IsRedirect => RedirectActionResult != null;

        /// <summary>
        /// 
        /// </summary>
        public IActionResult RedirectActionResult { get; private set; }

        #endregion

    }

}
