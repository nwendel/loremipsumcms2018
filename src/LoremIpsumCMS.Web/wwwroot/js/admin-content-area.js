﻿'use strict';

$('.add-content-area-item').click(function (e) {
    var tableBody = $(e.target.parentElement.firstElementChild)
        .find('tbody');
    var nextIndex = tableBody
        .find('tr')
        .length;
    var row = tableBody
        .children('tr:first')
        .clone(true)
        .removeAttr('hidden');
    row.find('input')
        .each(function (index, item) {
            var element = $(item);
            var name = element.attr('name');
            name = name.replace('0', nextIndex);
            element.attr('name', name);
            var id = element.attr('id');
            if (id) {
                id = id.replace('0', nextIndex);
                element.attr('id', id);
            }
        });

    var contentAreaItemName = $(e.target.parentElement).find('.content-area-item-name');
    var partialName = contentAreaItemName.val();
    contentAreaItemName.val('');

    row.find('span').text(partialName)
    var inputs = row.find('input');
    $(inputs[0]).val(partialName);
    $(inputs[1]).prop('checked', false);
    $(inputs[2]).prop('checked', false);

    tableBody.append(row);
});
