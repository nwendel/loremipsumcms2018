﻿'use strict';

$(document).on("click", ".page-data-type", function () {
    var url = $("#create-page").data("url") + "?pagetypenameslug=" + $(this).data("pagetypenameslug");
    $("#create-page")
        .removeAttr("disabled")
        .attr("href", url);
});

$(document).on("click", ".delete-page", function () {
    var pagePath = $(this).data('pagepath');

    $(".modal-body #Path").text(pagePath);
    $("#deletePageForm-Path").val(pagePath);
});