﻿'use strict';

$(document).on("click", ".partial-data-type", function () {
    var url = $("#create-partial").data("url") + "?partialtypenameslug=" + $(this).data("partialtypenameslug");
    $("#create-partial")
        .removeAttr("disabled")
        .attr("href", url);
});

$(document).on("click", ".delete-partial", function () {
    var name = $(this).data('partialname');
    var nameSlug = $(this).data('partialnameslug');

    $(".modal-body #Name").text(name);
    $("#deletePartialForm-NameSlug").val(nameSlug);
});