﻿'use strict';

$('.delete-host').click(function () {
    $(this)
        .closest('td')
        .find('input')
        .prop('checked', true);
    $(this)
        .closest('tr')
        .attr('hidden', 'hidden');
});

$('.add-host').click(function () {
    var nextIndex = $('#hosts-table > tbody > tr')
        .length;
    var row = $('#hosts-table > tbody')
        .children('tr:first')
        .clone(true)
        .removeAttr('hidden');
    row.find('input')
        .each(function () {
            var name = $(this).attr('name');
            name = name.replace('0', nextIndex);
            $(this).attr('name', name);
            var id = $(this).attr('id');
            id = id.replace('0', nextIndex);
            $(this).attr('id', id);
        });
    var inputs = row.find('input');
    $(inputs[0]).val('');
    $(inputs[1]).prop('checked', false);
    $(inputs[2]).prop('checked', false);
    $('#hosts-table > tbody')
        .append(row);
});

$(document).on("click", ".delete-site", function () {
    var siteName = $(this).data('sitename');
    var siteNameSlug = $(this).data('sitenameslug');

    $(".modal-body #Name").text(siteName);
    $("#deleteSiteForm-NameSlug").val(siteNameSlug);
});