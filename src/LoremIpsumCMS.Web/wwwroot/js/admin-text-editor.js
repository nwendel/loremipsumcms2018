﻿'use strict';

var TextEditor = function () {
    var ckEditorHandler = function () {
		CKEDITOR.disableAutoInline = true;
		$('textarea.ckeditor').ckeditor();
	};

	return {
		init: function() {
			ckEditorHandler();
		}
	};
}();

$(document).ready(function () {
    TextEditor.init();
});
