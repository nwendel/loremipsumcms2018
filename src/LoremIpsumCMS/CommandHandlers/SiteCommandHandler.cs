﻿#region License
// Copyright (c) Niklas Wendel 2017-2018
// 
// Licensed under the Apache License, Version 2.0 (the "License"); 
// you may not use this file except in compliance with the License. 
// You may obtain a copy of the License at 
// 
// http://www.apache.org/licenses/LICENSE-2.0 
// 
// Unless required by applicable law or agreed to in writing, software 
// distributed under the License is distributed on an "AS IS" BASIS, 
// WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied. 
// See the License for the specific language governing permissions and 
// limitations under the License.
#endregion
using LoremIpsumCMS.Commands;
using LoremIpsumCMS.Events;
using LoremIpsumCMS.Exceptions;
using LoremIpsumCMS.Infrastructure;
using LoremIpsumCMS.Infrastructure.Collections;
using LoremIpsumCMS.Infrastructure.Cqrs;
using LoremIpsumCMS.Infrastructure.Raven;
using LoremIpsumCMS.Model;
using LoremIpsumCMS.Queries;
using Raven.Client.Documents;
using Raven.Client.Documents.Session;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace LoremIpsumCMS.CommandHandlers
{

    /// <summary>
    /// 
    /// </summary>
    public class SiteCommandHandler :
        IValidateCommand<CreateSiteCommand>,
        IHandleCommand<CreateSiteCommand>,
        IValidateCommand<UpdateSiteCommand>,
        IHandleCommand<UpdateSiteCommand>,
        IValidateCommand<DeleteSiteCommand>,
        IHandleCommand<DeleteSiteCommand>
    {

        #region Dependencies

        private readonly IAsyncDocumentSession _asyncDocumentSession;

        #endregion

        #region Constructor

        /// <summary>
        /// 
        /// </summary>
        /// <param name="asyncDocumentSession"></param>
        public SiteCommandHandler(IAsyncDocumentSession asyncDocumentSession)
        {
            _asyncDocumentSession = asyncDocumentSession;
        }

        #endregion

        #region Validate Create

        /// <summary>
        /// 
        /// </summary>
        /// <param name="command"></param>
        public async Task ValidateAsync(CreateSiteCommand command)
        {
            var siteNameSlug = command.Name.Slugify();
            var existingSite = await _asyncDocumentSession.Query<Site>()
                .Where(x => x.NameSlug == siteNameSlug)
                .SingleOrDefaultAsync();
            if (existingSite != null)
            {
                throw new SiteExistsException(siteNameSlug);
            }

            var hostnames = command.Hosts
                .Select(x => x.Name)
                .ToArray();

            var otherHosts = await _asyncDocumentSession.LoadByAsync<Host>(x => x.Names(hostnames));
            foreach (var host in command.Hosts)
            {
                var found = otherHosts[host.Name] != null;
                if(found)
                {
                    throw new HostExistsException(host.Name);
                }
            }
        }

        #endregion

        #region Handle Create

        /// <summary>
        /// 
        /// </summary>
        /// <param name="command"></param>
        /// <returns></returns>
        public Task<IEnumerable<AbstractEvent>> HandleAsync(CreateSiteCommand command)
        {
            return AsyncEnumerable.Create<AbstractEvent>(yield =>
            {
                yield.Return(new SiteCreatedEvent
                {
                    Name = command.Name,
                    Hosts = command.Hosts,
                    CreatedAt = command.CreatedAt,
                    CreatedByUserId = command.CreatedByUserId
                });
                return Task.CompletedTask;
            });
        }

        #endregion

        #region Validate Update

        /// <summary>
        /// 
        /// </summary>
        /// <param name="command"></param>
        public async Task ValidateAsync(UpdateSiteCommand command)
        {
            var siteId = command.Id;
            var site = await _asyncDocumentSession.LoadAsync<Site>(siteId);
            if (site == null)
            {
                throw new SiteNotFoundException(siteId);
            }

            var siteNameSlug = command.Name.Slugify();
            var otherSite = await _asyncDocumentSession.LoadByAsync<Site>(x => x.NameSlug(siteNameSlug));
            if (otherSite != null && otherSite.Id != siteId)
            {
                throw new SiteExistsException(siteNameSlug);
            }

            foreach (var host in command.Hosts)
            {
                var hostname = host.Name;
                var otherHost = await _asyncDocumentSession.LoadByAsync<Host>(x => x.Name(hostname));
                if (otherHost != null && otherHost.SiteId != siteId)
                {
                    throw new HostExistsException(hostname);
                }
            }
        }

        #endregion

        #region Handle Update

        /// <summary>
        /// 
        /// </summary>
        /// <param name="command"></param>
        /// <returns></returns>
        public Task<IEnumerable<AbstractEvent>> HandleAsync(UpdateSiteCommand command)
        {
            return AsyncEnumerable.Create<AbstractEvent>(yield =>
            {
                yield.Return(new SiteUpdatedEvent
                {
                    Id = command.Id,
                    Name = command.Name,
                    Hosts = command.Hosts,
                    UpdatedAt = command.UpdatedAt,
                });
                return Task.CompletedTask;
            });
        }

        #endregion

        #region Validate Delete

        /// <summary>
        /// 
        /// </summary>
        /// <param name="command"></param>
        public async Task ValidateAsync(DeleteSiteCommand command)
        {
            var siteId = command.Id;
            var site = await _asyncDocumentSession.LoadAsync<Site>(siteId);
            if (site == null)
            {
                throw new SiteNotFoundException(siteId);
            }
        }

        #endregion

        #region Handle Delete

        /// <summary>
        /// 
        /// </summary>
        /// <param name="command"></param>
        /// <returns></returns>
        public Task<IEnumerable<AbstractEvent>> HandleAsync(DeleteSiteCommand command)
        {
            return AsyncEnumerable.Create<AbstractEvent>(yield =>
            {
                yield.Return(new SiteDeletedEvent
                {
                    Id = command.Id
                });
                return Task.CompletedTask;
            });
        }

        #endregion

    }

}
