﻿#region License
// Copyright (c) Niklas Wendel 2017-2018
// 
// Licensed under the Apache License, Version 2.0 (the "License"); 
// you may not use this file except in compliance with the License. 
// You may obtain a copy of the License at 
// 
// http://www.apache.org/licenses/LICENSE-2.0 
// 
// Unless required by applicable law or agreed to in writing, software 
// distributed under the License is distributed on an "AS IS" BASIS, 
// WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied. 
// See the License for the specific language governing permissions and 
// limitations under the License.
#endregion
using LoremIpsumCMS.Events;
using LoremIpsumCMS.Infrastructure.Cqrs;
using LoremIpsumCMS.Model;
using Raven.Client.Documents;
using Raven.Client.Documents.Session;
using System.Linq;
using System.Threading.Tasks;

namespace LoremIpsumCMS.EventHandlers
{

    /// <summary>
    /// 
    /// </summary>
    public class HostEventHandler :
        ISubscribeTo<SiteCreatedEvent>,
        ISubscribeTo<SiteDeletedEvent>
    {

        #region Dependencies

        private readonly IAsyncDocumentSession _asyncDocumentSession;

        #endregion

        #region Constructor

        /// <summary>
        /// 
        /// </summary>
        /// <param name="asyncDocumentSession"></param>
        public HostEventHandler(IAsyncDocumentSession asyncDocumentSession)
        {
            _asyncDocumentSession = asyncDocumentSession;
        }

        #endregion

        #region Handle Site Created

        /// <summary>
        /// 
        /// </summary>
        /// <param name="event"></param>
        public async Task HandleAsync(SiteCreatedEvent @event)
        {
            foreach (var siteHost in @event.Hosts)
            {
                var host = new Host(siteHost.Name, @event.Id, siteHost.IsAdmin);
                await _asyncDocumentSession.StoreAsync(host);
            }
        }

        #endregion

        #region Handle Site Deleted

        /// <summary>
        /// 
        /// </summary>
        /// <param name="event"></param>
        public async Task HandleAsync(SiteDeletedEvent @event)
        {
            var hosts = await _asyncDocumentSession.Query<Host>()
                .Where(x => x.SiteId == @event.Id)
                .ToListAsync();

            foreach (var host in hosts)
            {
                _asyncDocumentSession.Delete(host);
            }
        }

        #endregion

    }

}
