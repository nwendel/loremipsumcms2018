﻿#region License
// Copyright (c) Niklas Wendel 2017-2018
// 
// Licensed under the Apache License, Version 2.0 (the "License"); 
// you may not use this file except in compliance with the License. 
// You may obtain a copy of the License at 
// 
// http://www.apache.org/licenses/LICENSE-2.0 
// 
// Unless required by applicable law or agreed to in writing, software 
// distributed under the License is distributed on an "AS IS" BASIS, 
// WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied. 
// See the License for the specific language governing permissions and 
// limitations under the License.
#endregion
using AutoMapper;
using FluentRegistration;
using Microsoft.Extensions.DependencyInjection;

namespace LoremIpsumCMS.Infrastructure.AutoMapper
{

    /// <summary>
    /// 
    /// </summary>
    public class MapperServiceInstaller : IServiceInstaller
    {

        #region Install

        /// <summary>
        /// 
        /// </summary>
        /// <param name="services"></param>
        public void Install(IServiceCollection services)
        {
            services.Register(c => c
                .ImplementedBy<MapperServiceFactory>()
                .WithServices
                    .Self()
                    .AllInterfaces());

            services.Register(c => c
                .For<IMapper>()
                .UsingFactory<MapperServiceFactory>(f => f.CreateMapper()));
        }

        #endregion

    }

}
