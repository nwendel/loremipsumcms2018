﻿#region License
// Copyright (c) Niklas Wendel 2017-2018
// 
// Licensed under the Apache License, Version 2.0 (the "License"); 
// you may not use this file except in compliance with the License. 
// You may obtain a copy of the License at 
// 
// http://www.apache.org/licenses/LICENSE-2.0 
// 
// Unless required by applicable law or agreed to in writing, software 
// distributed under the License is distributed on an "AS IS" BASIS, 
// WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied. 
// See the License for the specific language governing permissions and 
// limitations under the License.
#endregion
using LoremIpsumCMS.Infrastructure.EventStore;
using LoremIpsumCMS.Infrastructure.Logging;
using LoremIpsumCMS.Infrastructure.Model;
using LoremIpsumCMS.Infrastructure.Raven.UniqueConstraints;
using LoremIpsumCMS.Infrastructure.Reflection;
using LoremIpsumCMS.Infrastructure.Validation;
using Microsoft.Extensions.DependencyInjection;
using Microsoft.Extensions.Logging;
using Raven.Client.Documents.Session;
using System;
using System.Collections.Concurrent;
using System.Collections.Generic;
using System.Linq;
using System.Reflection;
using System.Threading.Tasks;

namespace LoremIpsumCMS.Infrastructure.Cqrs
{

    /// <summary>
    /// 
    /// </summary>
    public class RavenCommandSender : ICommandSender
    {

        #region Dependencies

        private readonly IServiceProvider _serviceProvider;
        private readonly IValidationService _validationService;
        private readonly IEventStore _eventStore;
        private readonly IAsyncDocumentSession _asyncDocumentSession;

        #endregion

        #region Fields

        private readonly ILogger _logger = LogManager.CreateCurrentClassLogger();
        private readonly ConcurrentDictionary<Type, Func<IEnumerable<AbstractEvent>, Task<IEnumerable<AbstractEvent>>>> _eventAppliers = new ConcurrentDictionary<Type, Func<IEnumerable<AbstractEvent>, Task<IEnumerable<AbstractEvent>>>>();
        private readonly ConcurrentDictionary<Type, bool> _shouldApplyEvent = new ConcurrentDictionary<Type, bool>();

        #endregion

        #region Constructor

        /// <summary>
        /// 
        /// </summary>
        /// <param name="serviceProvider"></param>
        /// <param name="validationService"></param>
        /// <param name="eventStore"></param>
        /// <param name="asyncDocumentSession"></param>
        public RavenCommandSender(
            IServiceProvider serviceProvider,
            IValidationService validationService,
            IEventStore eventStore,
            IAsyncDocumentSession asyncDocumentSession)
        {
            _serviceProvider = serviceProvider;
            _validationService = validationService;
            _eventStore = eventStore;
            _asyncDocumentSession = asyncDocumentSession;
        }

        #endregion

        #region Validate Async

        /// <inheritdoc />
        /// <summary>
        /// </summary>
        /// <typeparam name="TCommand"></typeparam>
        /// <param name="command"></param>
        public async Task ValidateAsync<TCommand>(TCommand command)
            where TCommand : class, ICommand
        {
            if (command == null)
            {
                throw new ArgumentNullException(nameof(command));
            }

            var messages = await _validationService.ValidateAsync(command);
            var message = messages.FirstOrDefault();
            if (message != null)
            {
                throw new ValidationException(message.Text);
            }

            var commandValidator = _serviceProvider.GetService<IValidateCommand<TCommand>>();
            if (commandValidator != null)
            {
                await commandValidator.ValidateAsync(command);
            }
        }

        #endregion

        #region Send Async

        /// <summary>
        /// 
        /// </summary>
        /// <typeparam name="TCommand"></typeparam>
        /// <param name="command"></param>
        /// <returns></returns>
        public async Task<string> SendAsync<TCommand>(TCommand command)
            where TCommand : class, ICommand
        {
            _logger.LogDebug($"Sending command: {typeof(TCommand).FullName}");

            await ValidateAsync(command);

            var events = await HandleCommand(command);
            var applier = FindApplierForType(command.AggregateType);
            var appliedEvents = (await applier.Invoke(events)).ToList();
            await PublishEvents(appliedEvents);
            await _eventStore.SaveEventsAsync(command.AggregateType, appliedEvents);

            //if (!_documentSession.Advanced.GetIsInTransactionScope())
            //{
                await _asyncDocumentSession.SaveChangesExAsync();
            //}

            // TODO: Do I need to return the aggregate id here? I would prefer not to...
            var aggregateId = appliedEvents.FirstOrDefault()?.Id;
            return aggregateId;
        }

        /// <summary>
        /// 
        /// </summary>
        /// <param name="aggregateType"></param>
        /// <returns></returns>
        private Func<IEnumerable<AbstractEvent>, Task<IEnumerable<AbstractEvent>>> FindApplierForType(Type aggregateType)
        {
            var found = _eventAppliers.TryGetValue(aggregateType, out var applier);
            if (found)
            {
                return applier;
            }

            var methodInfo = typeof(RavenCommandSender)
                .GetTypeInfo()
                .GetMethod(nameof(ApplyEventsAsync));
            var genericMethodInfo = methodInfo.MakeGenericMethod(aggregateType);
            applier = events =>
            {
                var result = genericMethodInfo.InvokeAsync<IEnumerable<AbstractEvent>>(this, new object[] {events});
                return result;
            };
            _eventAppliers[aggregateType] = applier;
            return applier;
        }

        #endregion
        
        #region Apply Events

        /// <summary>
        /// 
        /// </summary>
        /// <param name="events"></param>
        public async Task<IEnumerable<AbstractEvent>> ApplyEventsAsync<TAggregate>(IEnumerable<AbstractEvent> events)
            where TAggregate : AbstractAggregate, new()
        {
            var appliedEvents = new List<AbstractEvent>();
            AbstractAggregate aggregate = null;

            foreach (var @event in events)
            {
                if (aggregate == null)
                {
                    // TODO: Is this correct?
                    //       Only create aggregate if command doesn't have Id property?  
                    //       If it does the aggregate should already exist!
                    //       Perhaps IIdAware? On non-create commands?
                    aggregate = @event.Id == null
                        ? await CreateAggregateAsync<TAggregate>()
                        : await LoadAggregateAsync<TAggregate>(@event.Id);
                }

                if (@event.Id == null)
                {
                    @event.Id = aggregate.Id;
                }

                // TODO: By having the if statement here, it will load the aggregate even tho it is not used,
                // TODO: is that correct? Perhaps move this check higher?
                if (ShouldApplyEventOnAggregate(@event))
                {
                    aggregate.ApplyEvent(@event);
                }
                appliedEvents.Add(@event);
            }
            return appliedEvents;
        }

        /// <summary>
        /// 
        /// </summary>
        /// <returns></returns>
        private async Task<TAggregate> CreateAggregateAsync<TAggregate>()
            where TAggregate : AbstractAggregate, new()
        {
            var aggregate = new TAggregate();
            await _asyncDocumentSession.StoreAsync(aggregate);
            return aggregate;
        }

        /// <summary>
        /// 
        /// </summary>
        /// <typeparam name="TAggregate"></typeparam>
        /// <returns></returns>
        private async Task<TAggregate> LoadAggregateAsync<TAggregate>(string id)
            where TAggregate : AbstractAggregate
        {
            var aggregate = await _asyncDocumentSession.LoadAsync<TAggregate>(id);
            if (aggregate == null)
            {
                throw new CqrsException($"Failed to load aggregate with id {id}");
            }

            return aggregate;
        }

        /// <summary>
        /// 
        /// </summary>
        /// <param name="event"></param>
        /// <returns></returns>
        private bool ShouldApplyEventOnAggregate(AbstractEvent @event)
        {
            var eventType = @event.GetType();

            var found = _shouldApplyEvent.TryGetValue(eventType, out var shouldApply);
            if (found)
            {
                return shouldApply;
            }

            shouldApply = eventType.GetTypeInfo().GetCustomAttribute<SkipAggregateApplyAttribute>() == null;
            _shouldApplyEvent[eventType] = shouldApply;

            return shouldApply;
        }

        #endregion

        #region Handle Command

        /// <summary>
        /// 
        /// </summary>
        /// <typeparam name="TCommand"></typeparam>
        /// <param name="command"></param>
        /// <returns></returns>
        private async Task<IEnumerable<AbstractEvent>> HandleCommand<TCommand>(TCommand command)
            where TCommand : ICommand
        {
            var commandHandler = _serviceProvider.GetRequiredService<IHandleCommand<TCommand>>();
            var events = await commandHandler.HandleAsync(command);
            return events;
        }

        #endregion

        #region Publish Events

        /// <summary>
        /// 
        /// </summary>
        /// <param name="appliedEvents"></param>
        private async Task PublishEvents(IEnumerable<AbstractEvent> appliedEvents)
        {
            foreach (var appliedEvent in appliedEvents)
            {
                await PublishEventAsync(appliedEvent);
            }
        }

        /// <summary>
        /// 
        /// </summary>
        /// <param name="appliedEvent"></param>
        private async Task PublishEventAsync(AbstractEvent appliedEvent)
        {
            var eventSubscriberType = typeof(ISubscribeTo<>).MakeGenericType(appliedEvent.GetType());
            var eventHandlerMethod = eventSubscriberType.GetMethod(nameof(ISubscribeTo<AbstractEvent>.HandleAsync));

            var eventSubscribers = _serviceProvider.GetServices(eventSubscriberType) ?? new object[0];

            foreach (var eventSubscriber in eventSubscribers)
            {
                _logger.LogDebug($"Publishing event {appliedEvent.GetType().FullName} to {eventSubscriber.GetType().FullName}");

                await eventHandlerMethod.InvokeAsync(eventSubscriber, new object[] { appliedEvent });
            }
        }

        #endregion

    }

}
