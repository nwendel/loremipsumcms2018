﻿#region License
// Copyright (c) Niklas Wendel 2017-2018
// 
// Licensed under the Apache License, Version 2.0 (the "License"); 
// you may not use this file except in compliance with the License. 
// You may obtain a copy of the License at 
// 
// http://www.apache.org/licenses/LICENSE-2.0 
// 
// Unless required by applicable law or agreed to in writing, software 
// distributed under the License is distributed on an "AS IS" BASIS, 
// WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied. 
// See the License for the specific language governing permissions and 
// limitations under the License.
#endregion
using LoremIpsumCMS.Infrastructure.Cqrs;
using Raven.Client.Documents.Session;
using System;
using System.Collections.Generic;
using System.Threading.Tasks;

namespace LoremIpsumCMS.Infrastructure.EventStore
{

    /// <summary>
    /// 
    /// </summary>
    public class RavenEventStore : IEventStore
    {

        #region Depnendencies

        private readonly IAsyncDocumentSession _asyncDocumentSession;

        #endregion

        #region Constructor

        /// <summary>
        /// 
        /// </summary>
        /// <param name="asyncDocumentSession"></param>
        public RavenEventStore(IAsyncDocumentSession asyncDocumentSession)
        {
            _asyncDocumentSession = asyncDocumentSession;
        }

        #endregion

        #region Save Events Async

        /// <summary>
        /// 
        /// </summary>
        /// <param name="aggregateType"></param>
        /// <param name="events"></param>
        public async Task SaveEventsAsync(Type aggregateType, IEnumerable<AbstractEvent> events)
        {
            if (aggregateType == null)
            {
                throw new ArgumentNullException(nameof(aggregateType));
            }
            if (events == null)
            {
                throw new ArgumentNullException(nameof(events));
            }

            string aggregateId = null;
            var aggregateTypeName = aggregateType.Name;

            foreach (var @event in events)
            {
                if (@event.Id == null)
                {
                    throw new InvalidOperationException("Cannot save event without AggregateId");
                }
                if (aggregateId == null)
                {
                    aggregateId = @event.Id;
                }
                if (@event.Id != aggregateId)
                {
                    throw new InvalidOperationException("Cannot save events with different AggregateIds");
                }

                var persistedEvent = new PersistedEvent(aggregateTypeName, @event);
                await _asyncDocumentSession.StoreAsync(persistedEvent);
            }
        }

        #endregion

    }

}
