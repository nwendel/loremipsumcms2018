﻿using LoremIpsumCMS.Infrastructure.Collections;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace LoremIpsumCMS.Infrastructure.Linq
{

    // TODO: Temporary solution while waiting for C# 8

    /// <summary>
    /// 
    /// </summary>
    public static class TaskEnumerableExtensions
    {

        /// <summary>
        /// 
        /// </summary>
        /// <typeparam name="T"></typeparam>
        /// <typeparam name="TItem"></typeparam>
        /// <param name="self"></param>
        /// <param name="selector"></param>
        /// <returns></returns>
        public static Task<IEnumerable<TItem>> SelectManyAsync<T, TItem>(this IEnumerable<T> self, Func<T, Task<IEnumerable<TItem>>> selector)
        {
            return AsyncEnumerable.Create<TItem>(async yield =>
            {
                foreach (var item in self)
                {
                    foreach (var result in await selector(item))
                    {
                        yield.Return(result);
                    }
                }
            });
        }

        /// <summary>
        /// 
        /// </summary>
        /// <typeparam name="T"></typeparam>
        /// <typeparam name="TItem"></typeparam>
        /// <param name="self"></param>
        /// <param name="selector"></param>
        /// <returns></returns>
        public static Task<IEnumerable<TItem>> SelectManyAsync<T, TItem>(this IEnumerable<T> self, Func<T, int, Task<IEnumerable<TItem>>> selector)
        {
            return AsyncEnumerable.Create<TItem>(async yield =>
            {
                var ix = 0;
                foreach (var item in self)
                {
                    foreach (var result in await selector(item, ix))
                    {
                        yield.Return(result);
                        ix++;
                    }
                }
            });
        }

        /// <summary>
        /// 
        /// </summary>
        /// <typeparam name="TItem"></typeparam>
        /// <param name="self"></param>
        /// <returns></returns>
        public static async Task<List<TItem>> ToListAsync<TItem>(this Task<IEnumerable<TItem>> self)
        {
            return (await self).ToList();
        }

    }

}
