﻿#region License
// Copyright (c) Niklas Wendel 2017-2018
// 
// Licensed under the Apache License, Version 2.0 (the "License"); 
// you may not use this file except in compliance with the License. 
// You may obtain a copy of the License at 
// 
// http://www.apache.org/licenses/LICENSE-2.0 
// 
// Unless required by applicable law or agreed to in writing, software 
// distributed under the License is distributed on an "AS IS" BASIS, 
// WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied. 
// See the License for the specific language governing permissions and 
// limitations under the License.
#endregion
using AutoMapper;
using FluentRegistration;
using Microsoft.Extensions.DependencyInjection;
using System.Reflection;
using LoremIpsumCMS.Infrastructure.Validation;

namespace LoremIpsumCMS.Infrastructure.Modules
{

    /// <summary>
    /// 
    /// </summary>
    public abstract class AbstractModule : IModule
    {

        #region Properties

        /// <summary>
        /// 
        /// </summary>
        public abstract string Name { get; }

        /// <summary>
        /// 
        /// </summary>
        public string Version => Assembly.GetName().Version.ToString();

        /// <summary>
        /// 
        /// </summary>
        public Assembly Assembly => GetType().GetTypeInfo().Assembly;

        /// <summary>
        /// 
        /// </summary>
        public string BaseNamespace => Assembly.GetName().Name;

        #endregion

        #region Configure Services

        /// <summary>
        /// 
        /// </summary>
        /// <param name="services"></param>
        public virtual void ConfigureServices(IServiceCollection services)
        {
            ConfigureServicesInstallers(services);
            ConfigureServicesMapperProfiles(services);
            ConfigureServicesValueResolvers(services);
            ConfigureServicesValidators(services);
        }

        /// <summary>
        /// 
        /// </summary>
        /// <param name="services"></param>
        protected virtual void ConfigureServicesInstallers(IServiceCollection services)
        {
            services.Install(i => i.FromAssemblyContaining(GetType()));
        }

        /// <summary>
        /// 
        /// </summary>
        /// <param name="services"></param>
        protected virtual void ConfigureServicesMapperProfiles(IServiceCollection services)
        {
            services.Register(r => r
                .FromAssemblyContaining(GetType())
                .Where(c => c.AssignableTo<Profile>())
                .WithServices.Service<Profile>());
        }

        /// <summary>
        /// 
        /// </summary>
        /// <param name="services"></param>
        protected virtual void ConfigureServicesValueResolvers(IServiceCollection services)
        {
            services.Register(r => r
                .FromAssemblyContaining(GetType())
                .Where(c => c.AssignableTo(typeof(IValueResolver<,,>)))
                .WithServices.Self());
        }

        /// <summary>
        /// 
        /// </summary>
        /// <param name="services"></param>
        protected virtual void ConfigureServicesValidators(IServiceCollection services)
        {
            services.Register(r => r
                .FromAssemblyContaining(GetType())
                .Where(c => c.AssignableTo(typeof(IValidator<>)))
                .WithServices
                    .AllInterfaces()
                    .Self());
        }

        #endregion

    }

}
