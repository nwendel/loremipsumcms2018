﻿#region License
// Copyright (c) Niklas Wendel 2017-2018
// 
// Licensed under the Apache License, Version 2.0 (the "License"); 
// you may not use this file except in compliance with the License. 
// You may obtain a copy of the License at 
// 
// http://www.apache.org/licenses/LICENSE-2.0 
// 
// Unless required by applicable law or agreed to in writing, software 
// distributed under the License is distributed on an "AS IS" BASIS, 
// WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied. 
// See the License for the specific language governing permissions and 
// limitations under the License.
#endregion
using LoremIpsumCMS.Infrastructure.Dependency;
using LoremIpsumCMS.Infrastructure.Logging;
using Microsoft.Extensions.DependencyModel;
using Microsoft.Extensions.Logging;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Reflection;

namespace LoremIpsumCMS.Infrastructure.Modules
{

    /// <summary>
    /// 
    /// </summary>
    public class ModuleLoader
    {

        #region Depencies

        private readonly ILogger _logger = LogManager.CreateCurrentClassLogger();
        private readonly DependencyContext _dependencyContext = DependencyContext.Default;

        #endregion

        #region Load All

        /// <summary>
        /// 
        /// </summary>
        /// <returns></returns>
        public IEnumerable<IModule> LoadAll()
        {
            var moduleAssemblies = LoadAssemblies();
            var moduleTypes = FindModuleTypes(moduleAssemblies);

            var modules = moduleTypes
                .Select(Activator.CreateInstance)
                .Cast<IModule>()
                .OrderByDependencies()
                .ToList();

            return modules;
        }

        #endregion

        #region Load Assemblies

        /// <summary>
        /// 
        /// </summary>
        /// <returns></returns>
        private IEnumerable<Assembly> LoadAssemblies()
        {
            // TODO: Something smarter here how I detect module assemblies?

            var compileLibraries = _dependencyContext.CompileLibraries;
            var candidateLibraries = compileLibraries
                .Where(x =>
                    x.Name == "LoremIpsumCMS" ||
                    x.Dependencies.Any(d => d.Name == "LoremIpsumCMS" ||
                                            d.Name == "LoremIpsumCMS.Web"))
                .ToList();
            var candidateAssemblies = candidateLibraries
                .Select(x => Assembly.Load(new AssemblyName(x.Name)))
                .ToList();

            foreach (var candidateAssembly in candidateAssemblies)
            {
                _logger.LogDebug("Found candidate module assembly: {0}", candidateAssembly.FullName);
            }

            // TODO: Check here that same assembly does not contain multiple modules?

            return candidateAssemblies;
        }

        #endregion

        #region Find Module Types

        /// <summary>
        /// 
        /// </summary>
        /// <param name="moduleAssemblies"></param>
        /// <returns></returns>
        private IEnumerable<Type> FindModuleTypes(IEnumerable<Assembly> moduleAssemblies)
        {
            var moduleTypes = moduleAssemblies
                .SelectMany(x => x.GetTypes())
                .Where(x => !x.GetTypeInfo().IsAbstract && typeof(IModule).IsAssignableFrom(x))
                .ToList();
            foreach (var moduleType in moduleTypes)
            {
                _logger.LogDebug("Found module: {0}", moduleType.GetTypeInfo().FullName);
            }

            return moduleTypes;
        }

        #endregion

    }

}
