﻿namespace LoremIpsumCMS.Infrastructure.Proxy
{

    /// <summary>
    /// 
    /// </summary>
    public abstract class AbstractInterceptor<T> : 
        IProxyTarget<T>

    {

        #region Properties

        /// <summary>
        /// 
        /// </summary>
        public T Target { get; set; }

        #endregion

    }

}
