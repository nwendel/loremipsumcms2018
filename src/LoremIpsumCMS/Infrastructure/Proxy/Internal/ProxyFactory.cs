﻿#region License
// Copyright (c) Niklas Wendel 2017-2018
// 
// Licensed under the Apache License, Version 2.0 (the "License"); 
// you may not use this file except in compliance with the License. 
// You may obtain a copy of the License at 
// 
// http://www.apache.org/licenses/LICENSE-2.0 
// 
// Unless required by applicable law or agreed to in writing, software 
// distributed under the License is distributed on an "AS IS" BASIS, 
// WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied. 
// See the License for the specific language governing permissions and 
// limitations under the License.
#endregion
using System;
using System.Collections.Generic;
using System.Linq;
using System.Reflection;
using System.Reflection.Emit;

namespace LoremIpsumCMS.Infrastructure.Proxy.Internal
{

    /// <summary>
    /// 
    /// </summary>
    public class ProxyFactory<TTarget> :
        IProxyBuilderFor<TTarget>,
        IProxyBuilderWithTarget<TTarget>
    {

        #region Fields

        private readonly IDictionary<Type, Type> _proxyTypeCache;
        private Func<TTarget> _targetAccessor;
        private readonly List<Type> _interceptorTypes = new List<Type>();

        #endregion

        #region Constructor

        /// <summary>
        /// 
        /// </summary>
        /// <param name="proxyTypeCache"></param>
        public ProxyFactory(IDictionary<Type, Type> proxyTypeCache)
        {
            _proxyTypeCache = proxyTypeCache;
        }

        #endregion

        #region With Target

        /// <summary>
        /// 
        /// </summary>
        /// <param name="targetAccessor"></param>
        /// <returns></returns>
        public IProxyBuilderWithTarget<TTarget> WithTarget(Func<TTarget> targetAccessor)
        {
            _targetAccessor = targetAccessor;
            return this;
        }

        #endregion

        #region Use Interceptor

        /// <summary>
        /// 
        /// </summary>
        /// <typeparam name="TInterceptor"></typeparam>
        /// <returns></returns>
        public IProxyBuilderWithTarget<TTarget> UseInterceptor<TInterceptor>()
            where TInterceptor : AbstractInterceptor<TTarget>, IProxyTarget<TTarget>, new()
        {
            _interceptorTypes.Add(typeof(TInterceptor));
            return this;
        }

        #endregion

        #region Build

        /// <summary>
        /// 
        /// </summary>
        /// <returns></returns>
        public TTarget Build()
        {
            var proxyType = CreateOrGetProxyType();
            return (TTarget)Activator.CreateInstance(proxyType, _targetAccessor);
        }

        #endregion

        #region Create Or Get Proxy Type

        /// <summary>
        /// 
        /// </summary>
        /// <typeparam name="T"></typeparam>
        /// <returns></returns>
        private Type CreateOrGetProxyType()
        {
            if (!typeof(TTarget).GetTypeInfo().IsInterface)
            {
                // TODO: Include interface name in exception!
                throw new ArgumentException("T must be an interface");
            }

            // TODO: Change this to not lock when found in cache?

            // TODO: Key in cache cannot be name of TTarget (interceptors must be considered too)

            lock (_proxyTypeCache)
            {
                var found = _proxyTypeCache.TryGetValue(typeof(TTarget), out var proxyType);
                if (found)
                {
                    return proxyType;
                }

                var typeBuilder = CreateTypeBuilder();
                var targetAccessorFieldBuilder = typeBuilder.DefineField("_targetAccessor", typeof(Func<TTarget>), FieldAttributes.Private);
                DefineConstructor(typeBuilder, targetAccessorFieldBuilder);
                ImplementProxyTargetInterface(typeBuilder, targetAccessorFieldBuilder);

                ImplementInterface(typeBuilder, targetAccessorFieldBuilder);
                foreach (var interfaceType in typeof(TTarget).GetInterfaces())
                {
                    ImplementInterface(typeBuilder, targetAccessorFieldBuilder, interfaceType);
                }

                proxyType = typeBuilder.CreateTypeInfo().AsType();
                _proxyTypeCache.Add(typeof(TTarget), proxyType);
                return proxyType;
            }
        }

        #endregion

        #region Create Type Builder

        /// <summary>
        /// 
        /// </summary>
        /// <returns></returns>
        private static TypeBuilder CreateTypeBuilder()
        {
            var typeName = typeof(TTarget).Name;

            // TODO: Name of proxy type must be different due to support for interceptors

            var assemblyBuilder = AssemblyBuilder.DefineDynamicAssembly(new AssemblyName(typeName + "$Proxy"), AssemblyBuilderAccess.Run);
            var moduleBuilder = assemblyBuilder.DefineDynamicModule(typeName + "$Proxy");
            var typeBuilder = moduleBuilder.DefineType(typeName + "$Proxy", TypeAttributes.Public | TypeAttributes.Class);

            // TODO: This does not work for generic types?

            return typeBuilder;
        }

        #endregion

        #region Define Constructor

        /// <summary>
        /// 
        /// </summary>
        /// <typeparam name="T"></typeparam>
        /// <param name="typeBuilder"></param>
        /// <param name="targetAccessorFieldBuilder"></param>
        private static void DefineConstructor(TypeBuilder typeBuilder, FieldBuilder targetAccessorFieldBuilder)
        {
            var targetAccessorType = typeof(Func<TTarget>);

            var constructorBuilder = typeBuilder.DefineConstructor(MethodAttributes.Public, CallingConventions.HasThis, new[] { targetAccessorType });
            var _ = constructorBuilder.DefineParameter(1, ParameterAttributes.None, "targetAccessorType");
            var constructorEmitter = constructorBuilder.GetILGenerator();

            constructorEmitter.Emit(OpCodes.Ldarg_0);
            constructorEmitter.Emit(OpCodes.Ldarg_1);
            constructorEmitter.Emit(OpCodes.Stfld, targetAccessorFieldBuilder);
            constructorEmitter.Emit(OpCodes.Ret);
        }

        #endregion

        #region Implement Proxy Target Interface

        /// <summary>
        /// 
        /// </summary>
        public static void ImplementProxyTargetInterface(TypeBuilder typeBuilder, FieldBuilder creatorFieldBuilder)
        {
            var proxyTargetType = typeof(IProxyTarget<TTarget>);
            var getMethod = proxyTargetType.GetProperty(nameof(IProxyTarget<TTarget>.Target)).GetGetMethod();

            typeBuilder.AddInterfaceImplementation(proxyTargetType);
            var methodBuilder = typeBuilder.DefineMethod(getMethod.Name, MethodAttributes.Public | MethodAttributes.Virtual, typeof(TTarget), Type.EmptyTypes);
            typeBuilder.DefineMethodOverride(methodBuilder, getMethod);

            var ilGenerator = methodBuilder.GetILGenerator();
            ilGenerator.Emit(OpCodes.Ldarg_0);
            ilGenerator.Emit(OpCodes.Ldfld, creatorFieldBuilder);
            ilGenerator.EmitCall(OpCodes.Callvirt, typeof(Func<TTarget>).GetTypeInfo().GetMethod(nameof(Func<TTarget>.Invoke)), null);
            ilGenerator.Emit(OpCodes.Ret);
        }

        #endregion

        #region Implement Interface

        /// <summary>
        /// 
        /// </summary>
        /// <typeparam name="T"></typeparam>
        /// <param name="typeBuilder"></param>
        /// <param name="creatorFieldBuilder"></param>
        private static void ImplementInterface(TypeBuilder typeBuilder, FieldBuilder creatorFieldBuilder)
        {
            ImplementInterface(typeBuilder, creatorFieldBuilder, typeof(TTarget));
        }

        /// <summary>
        /// 
        /// </summary>
        /// <param name="typeBuilder"></param>
        /// <param name="creatorFieldBuilder"></param>
        /// <param name="type"></param>
        private static void ImplementInterface(TypeBuilder typeBuilder, FieldBuilder creatorFieldBuilder, Type type)
        {
            var invokeMethodInfo = typeof(Func<>).MakeGenericType(type).GetTypeInfo().GetMethod(nameof(Func<object>.Invoke));

            typeBuilder.AddInterfaceImplementation(type);
            foreach (var methodInfo in type.GetMethods())
            {
                var parameterTypes = methodInfo.GetParameters().Select(x => x.ParameterType).ToArray();
                var methodBuilder = typeBuilder.DefineMethod(methodInfo.Name, MethodAttributes.Public | MethodAttributes.Virtual, methodInfo.ReturnType, parameterTypes);
                CopyGenericArguments(methodBuilder, methodInfo.GetGenericArguments());
                typeBuilder.DefineMethodOverride(methodBuilder, methodInfo);

                var ilGenerator = methodBuilder.GetILGenerator();
                // TODO: Is this correct?  Works for now, but dispose should probably be called if target is not null?
                if (typeof(IDisposable) != type)
                {
                    ilGenerator.Emit(OpCodes.Ldarg_0);
                    ilGenerator.Emit(OpCodes.Ldfld, creatorFieldBuilder);
                    ilGenerator.EmitCall(OpCodes.Callvirt, invokeMethodInfo, null);
                    for (var ix = 0; ix < parameterTypes.Length; ix++)
                    {
                        ilGenerator.Emit(OpCodes.Ldarg_S, ix + 1);
                    }
                    ilGenerator.EmitCall(OpCodes.Callvirt, methodInfo, null);
                }
                ilGenerator.Emit(OpCodes.Ret);
            }
        }

        /// <summary>
        /// 
        /// </summary>
        /// <param name="methodBuilder"></param>
        /// <param name="genericArguments"></param>
        private static void CopyGenericArguments(MethodBuilder methodBuilder, IReadOnlyList<Type> genericArguments)
        {
            if (genericArguments.Count == 0)
            {
                return;
            }

            var genericTypeParameters = methodBuilder.DefineGenericParameters(genericArguments
                .Select(x => x.Name)
                .ToArray());
            for (var ix = 0; ix < genericTypeParameters.Length; ix++)
            {
                var genericArgumentTypeInfo = genericArguments[ix].GetTypeInfo();
                var constraints = genericArgumentTypeInfo.GetGenericParameterConstraints();
                var baseType = constraints.SingleOrDefault(x => x.GetTypeInfo().IsClass);
                var interfaces = constraints.Where(x => x.GetTypeInfo().IsInterface).ToArray();
                genericTypeParameters[ix].SetBaseTypeConstraint(baseType);
                genericTypeParameters[ix].SetInterfaceConstraints(interfaces);

                var attributes = genericArgumentTypeInfo.GenericParameterAttributes;
                genericTypeParameters[ix].SetGenericParameterAttributes(attributes);
            }
        }

        #endregion

    }

}
