﻿#region License
// Copyright (c) Niklas Wendel 2017-2018
// 
// Licensed under the Apache License, Version 2.0 (the "License"); 
// you may not use this file except in compliance with the License. 
// You may obtain a copy of the License at 
// 
// http://www.apache.org/licenses/LICENSE-2.0 
// 
// Unless required by applicable law or agreed to in writing, software 
// distributed under the License is distributed on an "AS IS" BASIS, 
// WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied. 
// See the License for the specific language governing permissions and 
// limitations under the License.
#endregion
using Raven.Client.Documents.Operations;
using Raven.Client.Documents.Session;
using System.Threading.Tasks;

namespace LoremIpsumCMS.Infrastructure.Raven
{

    /// <summary>
    /// 
    /// </summary>
    public static class AdvancedSessionOperationsExtensions
    {
        /// <summary>
        /// 
        /// </summary>
        /// <param name="self"></param>
        /// <param name="operation"></param>
        /// <returns></returns>
        public static Task<TResult> SendAsync<TResult>(this IAdvancedDocumentSessionOperations self, IOperation<TResult> operation)
        {
            var documentStore = self.DocumentStore;
            var database = self.GetDatabase();
            var operationExecutor = documentStore.Operations.ForDatabase(database);

            var result = operationExecutor.SendAsync(operation);
            return result;
        }

        #region Get Database

        /// <summary>
        /// 
        /// </summary>
        /// <param name="self"></param>
        /// <returns></returns>
        public static string GetDatabase(this IAdvancedDocumentSessionOperations self)
        {
            // TODO: Is this correct? Is there a simpler way?
            return self.GetCurrentSessionNode().Result.Database;
        }

        #endregion

    }

}
