﻿#region License
// Copyright (c) Niklas Wendel 2017-2018
// 
// Licensed under the Apache License, Version 2.0 (the "License"); 
// you may not use this file except in compliance with the License. 
// You may obtain a copy of the License at 
// 
// http://www.apache.org/licenses/LICENSE-2.0 
// 
// Unless required by applicable law or agreed to in writing, software 
// distributed under the License is distributed on an "AS IS" BASIS, 
// WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied. 
// See the License for the specific language governing permissions and 
// limitations under the License.
#endregion
using LoremIpsumCMS.Infrastructure.Raven.UniqueConstraints.Internal;
using Raven.Client.Documents.Operations.CompareExchange;
using Raven.Client.Documents.Session;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Linq.Expressions;
using System.Threading.Tasks;

namespace LoremIpsumCMS.Infrastructure.Raven.UniqueConstraints
{

    /// <summary>
    /// 
    /// </summary>
    public static class AsyncDocumentSessionExtensions
    {

        #region Load By Unique Constraint Async

        /// <summary>
        /// 
        /// </summary>
        /// <typeparam name="TDocument"></typeparam>
        /// <param name="self"></param>
        /// <param name="keySelector"></param>
        /// <param name="value"></param>
        /// <returns></returns>
        public static async Task<TDocument> LoadByUniqueConstraintAsync<TDocument>(this IAsyncDocumentSession self, Expression<Func<TDocument, string>> keySelector, string value)
        {
            // TODO: Can this be done with just one roundtrip to server?

            var documentStore = self.Advanced.DocumentStore;
            var key = UniqueConstraintHelper.CreateKey(documentStore, keySelector, value);

            var result = await self.Advanced.SendAsync(new GetCompareExchangeValueOperation<string>(key));
            if (result == null)
            {
                return default(TDocument);
            }

            var entity = await self.LoadAsync<TDocument>(result.Value);
            return entity;
        }

        /// <summary>
        /// 
        /// </summary>
        /// <typeparam name="TDocument"></typeparam>
        /// <param name="self"></param>
        /// <param name="keySelector"></param>
        /// <param name="values"></param>
        /// <returns></returns>
        public static async Task<Dictionary<string, TDocument>> LoadByUniqueConstraintAsync<TDocument>(this IAsyncDocumentSession self, Expression<Func<TDocument, string>> keySelector, string[] values)
        {
            // TODO: Can this be done with just one roundtrip to server?

            // TODO: Workaround to add a dummy id since Raven crashes with just one entry if not found...

            var documentStore = self.Advanced.DocumentStore;
            var keys = values
                .Select(x => UniqueConstraintHelper.CreateKey(documentStore, keySelector, x))
                .Concat(new[] {Guid.NewGuid().ToString()})
                .ToArray();
            var getResult = await self.Advanced.SendAsync(new GetCompareExchangeValuesOperation<string>(keys));

            var ids = getResult
                .Select(x => x.Value.Value)
                .Where(x => x != null)
                .ToArray();

            var entities = await self.LoadAsync<TDocument>(ids);

            var result = new Dictionary<string, TDocument>();
            for (var ix = 0; ix < values.Length; ix++)
            {
                var key = keys[ix];
                var id = getResult[key].Value;
                var value = id != null
                    ? entities[id]
                    : default(TDocument);
                result.Add(values[ix], value);
            }
            return result;
        }

        #endregion

        ///// <summary>
        ///// 
        ///// </summary>
        ///// <typeparam name="TDocument"></typeparam>
        ///// <param name="self"></param>
        ///// <param name="entity"></param>
        ///// <returns></returns>
        //public static async Task<UniqueConstraintCheckResult<TDocument>> CheckForUniqueConstraintsAsync<TDocument>(this IAsyncDocumentSession self, TDocument entity)
        //{
        //    await Task.Run(() => { });
        //    return null;
        //}

        #region Save Changes Ex Async

        /// <summary>
        /// 
        /// </summary>
        /// <param name="self"></param>
        /// <returns></returns>
        public static async Task SaveChangesExAsync(this IAsyncDocumentSession self)
        {
            await self.SaveChangesAsync();

            // TODO: This is a hack...

            var documentSession = (InMemoryDocumentSessionOperations)self.Advanced;

            var deleteOperations = documentSession.GetUniqueConstraintDeleteOperations();
            foreach (var deleteOperation in deleteOperations)
            {
                documentSession.DeleteCompareExchangeValue(deleteOperation.Key, deleteOperation.Index);
            }

            documentSession.ClearUniqueConstraintOperations();
        }

        #endregion


    }

}
