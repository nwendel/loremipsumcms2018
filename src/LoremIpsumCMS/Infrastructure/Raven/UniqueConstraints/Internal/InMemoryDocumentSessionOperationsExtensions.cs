﻿#region License
// Copyright (c) Niklas Wendel 2017-2018
// 
// Licensed under the Apache License, Version 2.0 (the "License"); 
// you may not use this file except in compliance with the License. 
// You may obtain a copy of the License at 
// 
// http://www.apache.org/licenses/LICENSE-2.0 
// 
// Unless required by applicable law or agreed to in writing, software 
// distributed under the License is distributed on an "AS IS" BASIS, 
// WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied. 
// See the License for the specific language governing permissions and 
// limitations under the License.
#endregion
using LoremIpsumCMS.Infrastructure.Collections.Generic;
using Raven.Client.Documents.Operations;
using Raven.Client.Documents.Operations.CompareExchange;
using Raven.Client.Documents.Session;
using System.Collections.Generic;

namespace LoremIpsumCMS.Infrastructure.Raven.UniqueConstraints.Internal
{

    /// <summary>
    /// 
    /// </summary>
    public static class InMemoryDocumentSessionOperationsExtensions
    {

        #region Constants

        private const string UniqueConstraintsUndoOperationsKey = nameof(UniqueConstraintsUndoOperationsKey);
        private const string UniqueConstraintsDeleteOperationsKey = nameof(UniqueConstraintsDeleteOperationsKey);

        #endregion

        #region Get Compare Exchange Values

        /// <summary>
        /// 
        /// </summary>
        /// <param name="self"></param>
        /// <param name="keys"></param>
        public static Dictionary<string, CompareExchangeValue<string>> GetCompareExchangeValues(this InMemoryDocumentSessionOperations self, string[] keys)
        {
            if (keys.Length == 0)
            {
                return new Dictionary<string, CompareExchangeValue<string>>();
            }

            var getCommand = new GetCompareExchangeValuesOperation<string>(keys);
            var result = self.Send(getCommand);
            return result;
        }

        #endregion

        #region Create Compare Exchange Value

        /// <summary>
        /// 
        /// </summary>
        /// <param name="self"></param>
        /// <param name="key"></param>
        /// <param name="value"></param>
        public static bool CreateCompareExchangeValue(this InMemoryDocumentSessionOperations self, string key, string value)
        {
            var putCommand = new PutCompareExchangeValueOperation<string>(key, value, 0);
            var result = self.Send(putCommand);

            if (result.Successful)
            {
                self.AddUniqueConstraintUndoOperation(UniqueConstraintOperationType.Delete, key, result.Index);
            }

            return result.Successful;
        }

        #endregion

        #region Create Compare Exchange Value

        /// <summary>
        /// 
        /// </summary>
        /// <param name="self"></param>
        /// <param name="key"></param>
        /// <param name="index"></param>
        /// <param name="value"></param>
        /// <param name="oldValue"></param>
        public static bool UpdateCompareExchangeValue(this InMemoryDocumentSessionOperations self, string key, long index, string value, string oldValue)
        {
            var putCommand = new PutCompareExchangeValueOperation<string>(key, value, index);
            var result = self.Send(putCommand);

            if (result.Successful)
            {
                self.AddUniqueConstraintUndoOperation(UniqueConstraintOperationType.Modify, key, oldValue, index);
                self.AddUniqueConstraintUndoOperation(UniqueConstraintOperationType.Delete, key, result.Index);
            }

            return result.Successful;
        }

        #endregion

        #region Delete Compare Exchange Value

        /// <summary>
        /// 
        /// </summary>
        /// <param name="self"></param>
        /// <param name="key"></param>
        /// <param name="index"></param>
        public static void DeleteCompareExchangeValue(this InMemoryDocumentSessionOperations self, string key, long index)
        {
            var deleteCommand = new DeleteCompareExchangeValueOperation<string>(key, index);
            var deleteResult = self.Send(deleteCommand);
            if (!deleteResult.Successful)
            {
                throw new FatalUniqueConstraintException($"Unable to apply undo: Delete of {key}");
            }
        }

        #endregion

        #region Send

        /// <summary>
        /// 
        /// </summary>
        /// <typeparam name="TResult"></typeparam>
        /// <param name="self"></param>
        /// <param name="operation"></param>
        /// <returns></returns>
        private static TResult Send<TResult>(this InMemoryDocumentSessionOperations self, IOperation<TResult> operation)
        {
            return self.DocumentStore.Operations.ForDatabase(self.DatabaseName).Send(operation);
        }

        #endregion

        #region Add Unique Constraint Undo Operation

        /// <summary>
        /// 
        /// </summary>
        /// <param name="self"></param>
        /// <param name="operationType"></param>
        /// <param name="key"></param>
        /// <param name="value"></param>
        /// <param name="index"></param>
        private static void AddUniqueConstraintUndoOperation(this InMemoryDocumentSessionOperations self, UniqueConstraintOperationType operationType, string key, string value, long index)
        {
            var undoOperations = self.GetUniqueConstraintUndoOperations();
            undoOperations.Add(new UniqueConstraintOperation
            {
                OperationType = operationType,
                Key = key,
                Value = value,
                Index = index
            });
        }

        /// <summary>
        /// 
        /// </summary>
        /// <param name="self"></param>
        /// <param name="operationType"></param>
        /// <param name="key"></param>
        /// <param name="index"></param>
        private static void AddUniqueConstraintUndoOperation(this InMemoryDocumentSessionOperations self, UniqueConstraintOperationType operationType, string key, long index)
        {
            var undoOperations = self.GetUniqueConstraintUndoOperations();
            undoOperations.Add(new UniqueConstraintOperation
            {
                OperationType = operationType,
                Key = key,
                Index = index
            });
        }

        #endregion

        #region Get Unique Constraint Undo Operations

        /// <summary>
        /// 
        /// </summary>
        /// <returns></returns>
        public static List<UniqueConstraintOperation> GetUniqueConstraintUndoOperations(this InMemoryDocumentSessionOperations self)
        {
            var found = self.ExternalState.TryGetValue(UniqueConstraintsUndoOperationsKey, out var value);
            if (!found)
            {
                value = new List<UniqueConstraintOperation>();
                self.ExternalState.Add(UniqueConstraintsUndoOperationsKey, value);
            }
            var undoOperations = (List<UniqueConstraintOperation>)value;
            return undoOperations;
        }

        #endregion

        #region Add Unique Constraint Delete Operation

        /// <summary>
        /// 
        /// </summary>
        /// <param name="self"></param>
        /// <param name="key"></param>
        /// <param name="value"></param>
        /// <param name="index"></param>
        public static void AddUniqueConstraintDeleteOperation(this InMemoryDocumentSessionOperations self, string key, string value, long index)
        {
            var deleteOperations = self.GetUniqueConstraintDeleteOperations();

            deleteOperations.Add(new UniqueConstraintOperation
            {
                OperationType = UniqueConstraintOperationType.Delete,
                Key = key,
                Value = value,
                Index = index
            });
        }

        #endregion

        #region Remove Unique Constraint Delete Operation

        /// <summary>
        /// 
        /// </summary>
        /// <param name="self"></param>
        /// <param name="key"></param>
        public static void RemoveUniqueConstraintDeleteOperation(this InMemoryDocumentSessionOperations self, string key)
        {
            var deleteOperations = self.GetUniqueConstraintDeleteOperations();
            deleteOperations.RemoveWhere(x => x.Key == key);
        }

        #endregion

        #region Get Unique Constraint Delete Operations

        /// <summary>
        /// 
        /// </summary>
        /// <returns></returns>
        public static List<UniqueConstraintOperation> GetUniqueConstraintDeleteOperations(this InMemoryDocumentSessionOperations self)
        {
            var found = self.ExternalState.TryGetValue(UniqueConstraintsDeleteOperationsKey, out var value);
            if (!found)
            {
                value = new List<UniqueConstraintOperation>();
                self.ExternalState.Add(UniqueConstraintsDeleteOperationsKey, value);
            }
            var undoOperations = (List<UniqueConstraintOperation>)value;
            return undoOperations;
        }

        #endregion

        #region Clear Unique Constraint Operations

        /// <summary>
        /// 
        /// </summary>
        /// <returns></returns>
        public static void ClearUniqueConstraintOperations(this InMemoryDocumentSessionOperations self)
        {
            self.ExternalState.Remove(UniqueConstraintsUndoOperationsKey);
            self.ExternalState.Remove(UniqueConstraintsDeleteOperationsKey);
        }

        #endregion

    }

}
