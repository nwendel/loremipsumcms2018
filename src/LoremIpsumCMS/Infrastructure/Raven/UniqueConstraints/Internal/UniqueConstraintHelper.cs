﻿#region License
// Copyright (c) Niklas Wendel 2017-2018
// 
// Licensed under the Apache License, Version 2.0 (the "License"); 
// you may not use this file except in compliance with the License. 
// You may obtain a copy of the License at 
// 
// http://www.apache.org/licenses/LICENSE-2.0 
// 
// Unless required by applicable law or agreed to in writing, software 
// distributed under the License is distributed on an "AS IS" BASIS, 
// WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied. 
// See the License for the specific language governing permissions and 
// limitations under the License.
#endregion
using Raven.Client.Documents;
using System;
using System.Linq.Expressions;
using System.Reflection;

namespace LoremIpsumCMS.Infrastructure.Raven.UniqueConstraints.Internal
{

    /// <summary>
    /// 
    /// </summary>
    public static class UniqueConstraintHelper
    {

        #region Create Key

        /// <summary>
        /// 
        /// </summary>
        /// <param name="documentStore"></param>
        /// <param name="propertyInfo"></param>
        /// <param name="entity"></param>
        /// <returns></returns>
        public static string CreateKey(IDocumentStore documentStore, PropertyInfo propertyInfo, object entity)
        {
            var entityType = entity.GetType();
            var propertyName = propertyInfo.Name;
            var value = (string)propertyInfo.GetValue(entity);
            return CreateKey(documentStore, entityType, propertyName, value);
        }

        /// <summary>
        /// 
        /// </summary>
        /// <typeparam name="TDocument"></typeparam>
        /// <param name="documentStore"></param>
        /// <param name="keySelector"></param>
        /// <param name="value"></param>
        /// <returns></returns>
        public static string CreateKey<TDocument>(IDocumentStore documentStore, Expression<Func<TDocument, string>> keySelector, string value)
        {
            var entityType = typeof(TDocument);
            var propertyName = GetPropertyNameForKeySelector(keySelector);

            return CreateKey(documentStore, entityType, propertyName, value);
        }

        /// <summary>
        /// 
        /// </summary>
        /// <param name="documentStore"></param>
        /// <param name="entityType"></param>
        /// <param name="propertyName"></param>
        /// <param name="value"></param>
        /// <returns></returns>
        private static string CreateKey(IDocumentStore documentStore, Type entityType, string propertyName, string value)
        {
            var conventions = documentStore.Conventions;
            var collectionName = conventions.GetCollectionName(entityType);
            var separator = conventions.IdentityPartsSeparator;
            return $"UniqueConstraint{separator}{collectionName}{separator}{propertyName}{separator}{value}";
        }

        #endregion

        #region Get Property Name For Key Selector

        /// <summary>
        /// 
        /// </summary>
        /// <typeparam name="TDocument"></typeparam>
        /// <param name="keySelector"></param>
        /// <returns></returns>
        private static string GetPropertyNameForKeySelector<TDocument>(Expression<Func<TDocument, string>> keySelector)
        {
            var body = GetMemberExpression(keySelector);
            var propertyName = body.Member.Name;
            return propertyName;
        }

        /// <summary>
        /// 
        /// </summary>
        /// <typeparam name="T"></typeparam>
        /// <param name="keySelector"></param>
        /// <returns></returns>
        private static MemberExpression GetMemberExpression<T>(Expression<Func<T, string>> keySelector)
        {
            MemberExpression body;
            if (keySelector.Body is MemberExpression expression)
            {
                body = expression;
            }
            else
            {
                var operand = ((UnaryExpression)keySelector.Body).Operand;
                body = (MemberExpression)operand;
            }

            var isDefined = body.Member.IsDefined(typeof(UniqueConstraintAttribute), true);

            if (isDefined == false)
            {
                throw new InvalidOperationException($"Call to LoadByUniqueConstraint on {body.Member.DeclaringType.Name}.{body.Member.Name}, but the property isn't marked with [UniqueConstraint]");
            }
            return body;
        }

        #endregion

    }

}
