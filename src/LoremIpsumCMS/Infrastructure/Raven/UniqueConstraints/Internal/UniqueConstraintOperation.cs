﻿namespace LoremIpsumCMS.Infrastructure.Raven.UniqueConstraints.Internal
{

    /// <summary>
    /// 
    /// </summary>
    public class UniqueConstraintOperation
    {

        #region Properties

        /// <summary>
        /// 
        /// </summary>
        public UniqueConstraintOperationType OperationType { get; set; }

        /// <summary>
        /// 
        /// </summary>
        public string Key { get; set; }

        /// <summary>
        /// 
        /// </summary>
        public string Value { get; set; }

        /// <summary>
        /// 
        /// </summary>
        public long Index { get; set; }

        #endregion

    }

}
