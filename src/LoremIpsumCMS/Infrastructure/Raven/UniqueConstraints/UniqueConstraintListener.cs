﻿#region License
// Copyright (c) Niklas Wendel 2017-2018
// 
// Licensed under the Apache License, Version 2.0 (the "License"); 
// you may not use this file except in compliance with the License. 
// You may obtain a copy of the License at 
// 
// http://www.apache.org/licenses/LICENSE-2.0 
// 
// Unless required by applicable law or agreed to in writing, software 
// distributed under the License is distributed on an "AS IS" BASIS, 
// WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied. 
// See the License for the specific language governing permissions and 
// limitations under the License.
#endregion
using LoremIpsumCMS.Infrastructure.Linq;
using LoremIpsumCMS.Infrastructure.Raven.UniqueConstraints.Internal;
using LoremIpsumCMS.Infrastructure.Reflection;
using Raven.Client.Documents;
using Raven.Client.Documents.Operations.CompareExchange;
using Raven.Client.Documents.Session;
using Raven.Client.Json;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Reflection;

namespace LoremIpsumCMS.Infrastructure.Raven.UniqueConstraints
{

    /// <summary>
    /// 
    /// </summary>
    public static class UniqueConstraintListener
    {

        #region Register For

        /// <summary>
        /// 
        /// </summary>
        /// <param name="documentStore"></param>
        public static void RegisterFor(IDocumentStore documentStore)
        {
            documentStore.OnBeforeStore += OnBeforeStore;
            documentStore.OnBeforeDelete += OnBeforeDelete;
        }

        #endregion

        #region On Before Store

        /// <summary>
        /// 
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        private static void OnBeforeStore(object sender, BeforeStoreEventArgs e)
        {
            var documentSession = e.Session;
            var documentStore = documentSession.DocumentStore;

            var entity = e.Entity;
            var metadata = e.DocumentMetadata;
            var entityType = e.Entity.GetType();
            var propertyInfos = FindUniqueConstraintProperties(entityType);

            var oldUniqueConstraints = e.DocumentMetadata.GetUniqueConstraints();
            var oldKeys = oldUniqueConstraints
                .Select(x => x.Value)
                .Cast<string>()
                .ToArray();
            var oldCompareExchangeValues = documentSession.GetCompareExchangeValues(oldKeys);

            MarkForDelete(documentSession, oldCompareExchangeValues, e.DocumentId);

            if (propertyInfos.None())
            {
                metadata.RemoveUniqueConstraints();
                return;
            }
            var uniqueConstraints = new MetadataAsDictionary();
            metadata.SetUniqueConstraints(uniqueConstraints);

            foreach (var propertyInfo in propertyInfos)
            {
                var propertyValue = propertyInfo.GetValue(entity);
                if (propertyValue == null)
                {
                    continue;
                }

                var key = UniqueConstraintHelper.CreateKey(documentStore, propertyInfo, entity);
                var propertyName = propertyInfo.Name;
                uniqueConstraints.Add(propertyName, key);

                if (oldCompareExchangeValues.ContainsKey(key) && oldCompareExchangeValues[key].Value == e.DocumentId)
                {
                    documentSession.RemoveUniqueConstraintDeleteOperation(key);
                }
                else
                {
                    var isCreated = documentSession.CreateCompareExchangeValue(key, e.DocumentId);
                    if (isCreated)
                    {
                        continue;
                    }

                    var deleteOperations = documentSession.GetUniqueConstraintDeleteOperations();
                    var deleteOperation = deleteOperations.SingleOrDefault(x => x.Key == key);
                    if (deleteOperation != null)
                    {
                        var isUpdated = documentSession.UpdateCompareExchangeValue(key, deleteOperation.Index, e.DocumentId, deleteOperation.Value);
                        if (!isUpdated)
                        {
                            ThrowUniqueConstraintException(documentSession, $"Key {key} was updated");
                        }

                        deleteOperations.Remove(deleteOperation);
                    }
                    else
                    {
                        ThrowUniqueConstraintException(documentSession, $"Key already exists {key}");
                    }
                }
            }
        }

        /// <summary>
        /// 
        /// </summary>
        /// <param name="entityType"></param>
        /// <returns></returns>
        private static List<PropertyInfo> FindUniqueConstraintProperties(Type entityType)
        {
            var properties = entityType
                .GetProperties(BindingFlags.Instance | BindingFlags.Public | BindingFlags.NonPublic)
                .Where(x => x.HasCustomAttribute<UniqueConstraintAttribute>())
                .ToList();
            return properties;
        }

        #endregion

        #region On Before Delete

        /// <summary>
        /// 
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        private static void OnBeforeDelete(object sender, BeforeDeleteEventArgs e)
        {
            // TODO: Metadata is not available for an entity here, workaround for now

            using (var documentSession = e.Session.DocumentStore.OpenSession())
            {
                var oldEntity = documentSession.Load<object>(e.DocumentId);
                var uniqueConstraints = documentSession.Advanced.GetMetadataFor(oldEntity).GetUniqueConstraints();
                var keys = uniqueConstraints
                    .Select(x => x.Value)
                    .Cast<string>()
                    .ToArray();
                var compareExchangeValues = e.Session.GetCompareExchangeValues(keys);

                MarkForDelete(e.Session, compareExchangeValues, e.DocumentId);
            }
        }

        #endregion

        #region Mark for Delete

        /// <summary>
        /// 
        /// </summary>
        /// <param name="documentSession"></param>
        /// <param name="oldCompareExchangeValues"></param>
        /// <param name="documentId"></param>
        private static void MarkForDelete(InMemoryDocumentSessionOperations documentSession, Dictionary<string, CompareExchangeValue<string>> oldCompareExchangeValues, string documentId)
        {
            foreach (var keyValue in oldCompareExchangeValues)
            {
                var key = keyValue.Key;
                var currentDocumentId = keyValue.Value.Value;
                var index = keyValue.Value.Index;
                if (currentDocumentId != documentId)
                {
                    ThrowUniqueConstraintException(documentSession, $"Key {key} references different document");
                }
                documentSession.AddUniqueConstraintDeleteOperation(key, documentId, index);
            }
        }

        #endregion

        #region Throw Unique Constraint Exception

        /// <summary>
        /// 
        /// </summary>
        /// <param name="documentSession"></param>
        /// <param name="message"></param>
        private static void ThrowUniqueConstraintException(InMemoryDocumentSessionOperations documentSession, string message)
        {
            var undoOperations = documentSession.GetUniqueConstraintUndoOperations();
            foreach (var undoOperation in undoOperations)
            {
                switch (undoOperation.OperationType)
                {
                    case UniqueConstraintOperationType.Delete:
                        documentSession.DeleteCompareExchangeValue(undoOperation.Key, undoOperation.Index);
                        break;
                }
            }

            documentSession.ClearUniqueConstraintOperations();

            throw new UniqueConstraintException(message);
        }

        #endregion
    }

}
