﻿#region License
// Copyright (c) Niklas Wendel 2017-2018
// 
// Licensed under the Apache License, Version 2.0 (the "License"); 
// you may not use this file except in compliance with the License. 
// You may obtain a copy of the License at 
// 
// http://www.apache.org/licenses/LICENSE-2.0 
// 
// Unless required by applicable law or agreed to in writing, software 
// distributed under the License is distributed on an "AS IS" BASIS, 
// WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied. 
// See the License for the specific language governing permissions and 
// limitations under the License.
#endregion
using LoremIpsumCMS.Infrastructure.Raven.UniqueConstraints2.Internal;
using LoremIpsumCMS.Infrastructure.Reflection;
using Raven.Client.Documents.Operations.CompareExchange;
using Raven.Client.Documents.Session;
using Raven.Client.Json;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Linq.Expressions;
using System.Reflection;
using System.Threading.Tasks;
using Sparrow.Json;

namespace LoremIpsumCMS.Infrastructure.Raven.UniqueConstraints2
{

    /// <summary>
    /// 
    /// </summary>
    public static class AsyncDocumentSessionExtensions
    {

        #region Update Unique Constraints For

        /// <summary>
        /// 
        /// </summary>
        /// <typeparam name="TDocument"></typeparam>
        /// <param name="self"></param>
        /// <param name="document"></param>
        public static async Task UpdateUniqueConstraintsFor<TDocument>(this IAsyncDocumentSession self, TDocument document)
        {
            var documentId = self.Advanced.GetDocumentId(document);
            var metadata = self.Advanced.GetMetadataFor(document);

            var oldUniqueConstraints = metadata.GetUniqueConstraints();
            var oldKeys = oldUniqueConstraints
                .Select(x => x.Value)
                .Cast<string[]>()
                .SelectMany(x => x)
                .ToArray();
            var oldCompareExchangeValues = oldKeys.Length != 0
                ? await self.Advanced.ClusterTransaction.GetCompareExchangeValuesAsync<string>(oldKeys)
                : new Dictionary<string, CompareExchangeValue<string>>();

            var uniqueConstraints = new MetadataAsDictionary();
            metadata.SetUniqueConstraints(uniqueConstraints);

            var entityType = document.GetType();
            var propertyInfos = FindUniqueConstraintProperties(entityType);

            foreach (var propertyInfo in propertyInfos)
            {
                var oldPropertyKeys = (string[])oldUniqueConstraints[propertyInfo.Name];
                var propertyKeys = UniqueConstraintHelper.CreateKeys(self.Advanced.DocumentStore, propertyInfo, document);

                if (oldPropertyKeys != null)
                {
                    var deleteKeys = oldKeys.Except(propertyKeys);
                    foreach (var key in deleteKeys)
                    {
                        self.Advanced.ClusterTransaction.DeleteCompareExchangeValue(oldCompareExchangeValues[key]);
                    }
                }

                var propertyName = propertyInfo.Name;
                uniqueConstraints.Add(propertyName, propertyKeys);
                foreach (var key in propertyKeys)
                {
                    if (oldPropertyKeys == null || !oldPropertyKeys.Contains(key))
                    {
                        self.Advanced.ClusterTransaction.CreateCompareExchangeValue(key, documentId);
                    }
                }
            }
        }

        /// <summary>
        /// 
        /// </summary>
        /// <param name="entityType"></param>
        /// <returns></returns>
        private static List<PropertyInfo> FindUniqueConstraintProperties(Type entityType)
        {
            // TODO: NonPublic?

            var properties = entityType
                .GetProperties(BindingFlags.Instance | BindingFlags.Public | BindingFlags.NonPublic)
                .Where(x => x.HasCustomAttribute<UniqueConstraintAttribute>())
                .ToList();

            // TODO: Check that properties are valid types

            return properties;
        }

        #endregion

        #region Delete Unique Constraints For

        /// <summary>
        /// 
        /// </summary>
        /// <typeparam name="TDocument"></typeparam>
        /// <param name="self"></param>
        /// <param name="document"></param>
        public static async Task DeleteUniqueConstraintsFor<TDocument>(this IAsyncDocumentSession self, TDocument document)
        {
            var metadata = self.Advanced.GetMetadataFor(document);
            var uniqueConstraints = metadata.GetUniqueConstraints();

            var keys = uniqueConstraints
                .Select(x => x.Value)
                .Cast<string[]>()
                .SelectMany(x => x)
                .ToArray();

            var compareExchangeValues = await self.Advanced.ClusterTransaction.GetCompareExchangeValuesAsync<string>(keys);
            foreach (var compareExchangeValue in compareExchangeValues)
            {
                // TODO: Throw if compareExchange not found?

                self.Advanced.ClusterTransaction.DeleteCompareExchangeValue(compareExchangeValue.Value);
            }
        }

        #endregion

        #region Load By Unique Constraint Async

        /// <summary>
        /// 
        /// </summary>
        /// <typeparam name="TDocument"></typeparam>
        /// <param name="self"></param>
        /// <param name="keySelector"></param>
        /// <param name="value"></param>
        /// <returns></returns>
        public static async Task<TDocument> LoadByUniqueConstraintAsync<TDocument>(this IAsyncDocumentSession self, Expression<Func<TDocument, string>> keySelector, string value)
        {
            // TODO: Can this be done with just one roundtrip to server?

            var documentStore = self.Advanced.DocumentStore;
            var key = UniqueConstraintHelper.CreateKey(documentStore, keySelector, value);

            var result = await self.Advanced.SendAsync(new GetCompareExchangeValueOperation<string>(key));
            if (result == null)
            {
                return default(TDocument);
            }

            var entity = await self.LoadAsync<TDocument>(result.Value);
            return entity;
        }

        /// <summary>
        /// 
        /// </summary>
        /// <typeparam name="TDocument"></typeparam>
        /// <param name="self"></param>
        /// <param name="keySelector"></param>
        /// <param name="values"></param>
        /// <returns></returns>
        public static async Task<Dictionary<string, TDocument>> LoadByUniqueConstraintAsync<TDocument>(this IAsyncDocumentSession self, Expression<Func<TDocument, string>> keySelector, string[] values)
        {
            // TODO: Can this be done with just one roundtrip to server?

            // TODO: Workaround to add a dummy id since Raven crashes with just one entry if not found...

            var documentStore = self.Advanced.DocumentStore;
            var keys = values
                .Select(x => UniqueConstraintHelper.CreateKey(documentStore, keySelector, x))
                .Concat(new[] {Guid.NewGuid().ToString()})
                .ToArray();
            var getResult = await self.Advanced.SendAsync(new GetCompareExchangeValuesOperation<string>(keys));

            var ids = getResult
                .Select(x => x.Value.Value)
                .Where(x => x != null)
                .ToArray();

            var entities = await self.LoadAsync<TDocument>(ids);

            var result = new Dictionary<string, TDocument>();
            for (var ix = 0; ix < values.Length; ix++)
            {
                var key = keys[ix];
                var id = getResult[key].Value;
                var value = id != null
                    ? entities[id]
                    : default(TDocument);
                result.Add(values[ix], value);
            }
            return result;
        }

        #endregion


    }

}
