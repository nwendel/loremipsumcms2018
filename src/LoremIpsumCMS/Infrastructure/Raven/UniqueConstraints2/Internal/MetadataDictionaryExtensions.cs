﻿#region License
// Copyright (c) Niklas Wendel 2017-2018
// 
// Licensed under the Apache License, Version 2.0 (the "License"); 
// you may not use this file except in compliance with the License. 
// You may obtain a copy of the License at 
// 
// http://www.apache.org/licenses/LICENSE-2.0 
// 
// Unless required by applicable law or agreed to in writing, software 
// distributed under the License is distributed on an "AS IS" BASIS, 
// WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied. 
// See the License for the specific language governing permissions and 
// limitations under the License.
#endregion
using Raven.Client.Documents.Session;
using Raven.Client.Json;

namespace LoremIpsumCMS.Infrastructure.Raven.UniqueConstraints2.Internal
{

    /// <summary>
    /// 
    /// </summary>
    public static class MetadataDictionaryExtensions
    {

        private const string _metadataKey = "UniqueConstraints";

        /// <summary>
        /// 
        /// </summary>
        public static void RemoveUniqueConstraints(this IMetadataDictionary self)
        {
            self.Remove(_metadataKey);
        }

        /// <summary>
        /// 
        /// </summary>
        /// <returns></returns>
        public static MetadataAsDictionary GetUniqueConstraints(this IMetadataDictionary self)
        {
            if (!self.ContainsKey(_metadataKey))
            {
                self[_metadataKey] = new MetadataAsDictionary();
            }

            var uniqueConstraints = self[_metadataKey];
            return (MetadataAsDictionary)uniqueConstraints;
        }

        /// <summary>
        /// 
        /// </summary>
        /// <returns></returns>
        public static void SetUniqueConstraints(this IMetadataDictionary self, MetadataAsDictionary uniqueConstraints)
        {
            self[_metadataKey] = uniqueConstraints;
        }

    }

}
