﻿#region License
// Copyright (c) Niklas Wendel 2017-2018
// 
// Licensed under the Apache License, Version 2.0 (the "License"); 
// you may not use this file except in compliance with the License. 
// You may obtain a copy of the License at 
// 
// http://www.apache.org/licenses/LICENSE-2.0 
// 
// Unless required by applicable law or agreed to in writing, software 
// distributed under the License is distributed on an "AS IS" BASIS, 
// WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied. 
// See the License for the specific language governing permissions and 
// limitations under the License.
#endregion
using Raven.Client.Documents;
using System;
using System.Linq;
using System.Linq.Expressions;
using System.Reflection;

namespace LoremIpsumCMS.Infrastructure.Raven.UniqueConstraints2.Internal
{

    /// <summary>
    /// 
    /// </summary>
    public static class UniqueConstraintHelper
    {

        #region Create Keys

        /// <summary>
        /// 
        /// </summary>
        /// <param name="documentStore"></param>
        /// <param name="propertyInfo"></param>
        /// <param name="document"></param>
        /// <returns></returns>
        public static string[] CreateKeys(IDocumentStore documentStore, PropertyInfo propertyInfo, object document)
        {
            var propertyName = propertyInfo.Name;
            var documentType = document.GetType();

            var conventions = documentStore.Conventions;
            var collectionName = conventions.GetCollectionName(documentType);
            var separator = conventions.IdentityPartsSeparator;

            string[] values;
            var propertyValue = propertyInfo.GetValue(document);
            if (propertyValue == null)
            {
                return new string[0];
            }

            switch (propertyValue)
            {
                case string propertyValueSingle:
                    values = new[] { propertyValueSingle };
                    break;
                case string[] propertyValueMultiple:
                    if (propertyValueMultiple.Length == 0)
                    {
                        return new string[0];
                    }
                    values = propertyValueMultiple;
                    break;
                default:
                    throw new UniqueConstraintException("TODO");
            }

            var keys = values
                .Select(x => CreateKey(collectionName, separator, propertyName, x))
                .ToArray();
            return keys;
        }

        #endregion

        #region Create Key

        /// <summary>
        /// 
        /// </summary>
        /// <typeparam name="TDocument"></typeparam>
        /// <param name="documentStore"></param>
        /// <param name="keySelector"></param>
        /// <param name="value"></param>
        /// <returns></returns>
        public static string CreateKey<TDocument>(IDocumentStore documentStore, Expression<Func<TDocument, string>> keySelector, string value)
        {
            var documentType = typeof(TDocument);
            var propertyName = GetPropertyNameForKeySelector(keySelector);

            var conventions = documentStore.Conventions;
            var collectionName = conventions.GetCollectionName(documentType);
            var separator = conventions.IdentityPartsSeparator;

            return CreateKey(collectionName, separator, propertyName, value);
        }

        /// <summary>
        /// 
        /// </summary>
        /// <param name="collectionName"></param>
        /// <param name="separator"></param>
        /// <param name="propertyName"></param>
        /// <param name="value"></param>
        /// <returns></returns>
        private static string CreateKey(string collectionName, string separator, string propertyName, string value)
        {
            return $"UniqueConstraint{separator}{collectionName}{separator}{propertyName}{separator}{value}";
        }

        #endregion

        #region Get Property Name For Key Selector

        /// <summary>
        /// 
        /// </summary>
        /// <typeparam name="TDocument"></typeparam>
        /// <param name="keySelector"></param>
        /// <returns></returns>
        private static string GetPropertyNameForKeySelector<TDocument>(Expression<Func<TDocument, string>> keySelector)
        {
            var body = GetMemberExpression(keySelector);
            var propertyName = body.Member.Name;
            return propertyName;
        }

        /// <summary>
        /// 
        /// </summary>
        /// <typeparam name="T"></typeparam>
        /// <param name="keySelector"></param>
        /// <returns></returns>
        private static MemberExpression GetMemberExpression<T>(Expression<Func<T, string>> keySelector)
        {
            MemberExpression body;
            if (keySelector.Body is MemberExpression expression)
            {
                body = expression;
            }
            else
            {
                var operand = ((UnaryExpression)keySelector.Body).Operand;
                body = (MemberExpression)operand;
            }

            var isDefined = body.Member.IsDefined(typeof(UniqueConstraintAttribute), true);

            if (isDefined == false)
            {
                throw new InvalidOperationException($"Call to LoadByUniqueConstraint on {body.Member.DeclaringType.Name}.{body.Member.Name}, but the property isn't marked with [UniqueConstraint]");
            }
            return body;
        }

        #endregion

    }

}
