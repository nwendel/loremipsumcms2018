﻿#region License
// Copyright (c) Niklas Wendel 2017-2018
// 
// Licensed under the Apache License, Version 2.0 (the "License"); 
// you may not use this file except in compliance with the License. 
// You may obtain a copy of the License at 
// 
// http://www.apache.org/licenses/LICENSE-2.0 
// 
// Unless required by applicable law or agreed to in writing, software 
// distributed under the License is distributed on an "AS IS" BASIS, 
// WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied. 
// See the License for the specific language governing permissions and 
// limitations under the License.
#endregion
using LoremIpsumCMS.Infrastructure.Linq;
using LoremIpsumCMS.Infrastructure.Validation.Internal;
using System;
using System.Collections.Generic;
using System.Linq.Expressions;
using System.Threading.Tasks;

namespace LoremIpsumCMS.Infrastructure.Validation
{

    /// <inheritdoc />
    /// <summary>
    /// </summary>
    public abstract class AbstractValidator<T> : IValidator<T>
    {

        #region Fields

        private readonly IList<IValidationRule<T>> _validationRules = new List<IValidationRule<T>>();

        #endregion

        #region Validate Async

        /// <summary>
        /// 
        /// </summary>
        /// <param name="context"></param>
        /// <returns></returns>
        public Task<IEnumerable<ValidationMessage>> ValidateAsync(IValidationContext<T> context)
        {
            var messages = _validationRules.SelectManyAsync(x => x.ValidateAsync(context));
            return messages;
        }

        #endregion

        #region Rule For

        /// <summary>
        /// 
        /// </summary>
        /// <typeparam name="TResult"></typeparam>
        /// <param name="propertyExpression"></param>
        /// <returns></returns>
        protected IValidationRuleBuilder<TResult> RuleFor<TResult>(Expression<Func<T, TResult>> propertyExpression)
        {
            var rule = new PropertyValidationRule<T, TResult>(propertyExpression);
            _validationRules.Add(rule);

            var builder = new PropertyValidationRuleBuilder<T, TResult>(rule);
            return builder;
        }

        #endregion

        #region Validate Using

        /// <summary>
        /// 
        /// </summary>
        /// <param name="validator"></param>
        protected IValidationRuleBuilderOptions ValidateUsing(Func<IValidationContext<T>, Task<IEnumerable<ValidationMessage>>> validator)
        {
            var rule = new InstanceValidationRule<T>();
            _validationRules.Add(rule);

            var builder = new InstanceValidationRuleBuilder<T>(rule);
            builder.ValidateUsing(validator);
            return builder;
        }

        #endregion

        #region Include

        /// <summary>
        /// 
        /// </summary>
        /// <typeparam name="TValidator"></typeparam>
        /// <returns></returns>
        protected IValidationRuleBuilderOptions Include<TValidator>()
            where TValidator : IValidator<T>
        {
            var rule = new InstanceValidationRule<T>();
            _validationRules.Add(rule);

            var builder = new InstanceValidationRuleBuilder<T>(rule);
            builder.Include<TValidator>();
            return builder;
        }

        #endregion

    }

}
