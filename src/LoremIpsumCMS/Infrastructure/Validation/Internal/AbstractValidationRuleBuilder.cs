﻿#region License
// Copyright (c) Niklas Wendel 2017-2018
// 
// Licensed under the Apache License, Version 2.0 (the "License"); 
// you may not use this file except in compliance with the License. 
// You may obtain a copy of the License at 
// 
// http://www.apache.org/licenses/LICENSE-2.0 
// 
// Unless required by applicable law or agreed to in writing, software 
// distributed under the License is distributed on an "AS IS" BASIS, 
// WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied. 
// See the License for the specific language governing permissions and 
// limitations under the License.
#endregion
using LoremIpsumCMS.Infrastructure.Validation.ValueValidators;
using System;
using System.Collections.Generic;
using System.Threading.Tasks;

namespace LoremIpsumCMS.Infrastructure.Validation.Internal
{

    /// <summary>
    /// 
    /// </summary>
    public abstract class AbstractValidationRuleBuilder<T> : 
        IValidationRuleBuilder<T>,
        IValueValidatorContainer<T>
    {

        #region Fields

        private readonly IValidationRuleOptions _validationRuleOptions;

        #endregion

        #region Constructor

        /// <summary>
        /// 
        /// </summary>
        /// <param name="validationRuleOptions"></param>
        protected AbstractValidationRuleBuilder(IValidationRuleOptions validationRuleOptions)
        {
            _validationRuleOptions = validationRuleOptions;
        }

        #endregion

        #region Validate

        // TODO: Can't this be an extension too?

        /// <summary>
        /// 
        /// </summary>
        /// <returns></returns>
        public IValidationRuleBuilder<T> Validate()
        {
            AddValueValidator(new IncludeValidator<T, IValidator<T>>());
            return this;
        }

        #endregion

        #region Include

        // TODO: Can't this be an extension too?

        /// <summary>
        /// 
        /// </summary>
        /// <typeparam name="TValidator"></typeparam>
        /// <returns></returns>
        public IValidationRuleBuilder<T> Include<TValidator>()
            where TValidator : IValidator<T>
        {
            AddValueValidator(new IncludeValidator<T, TValidator>());
            return this;
        }

        #endregion

        #region Validate Using

        // TODO: Can't this be an extension too?

        /// <summary>
        /// 
        /// </summary>
        /// <typeparam name="T2"></typeparam>
        /// <param name="validator"></param>
        /// <returns></returns>
        public IValidationRuleBuilder<T> ValidateUsing(Func<IValidationContext<T>, Task<IEnumerable<ValidationMessage>>> validator)
        {
            AddValueValidator(new ValidateUsingValueValidator<T>(validator));
            return this;
        }

        #endregion

        #region With Message

        /// <summary>
        /// 
        /// </summary>
        /// <param name="message"></param>
        /// <returns></returns>
        public IValidationRuleBuilderOptions WithMessage(string message)
        {
            _validationRuleOptions.WithMessage = message;
            return this;
        }

        #endregion

        #region Add Value Validator

        /// <summary>
        /// 
        /// </summary>
        /// <param name="valueValidator"></param>
        public abstract void AddValueValidator(IValueValidator<T> valueValidator);

        #endregion

    }

}
