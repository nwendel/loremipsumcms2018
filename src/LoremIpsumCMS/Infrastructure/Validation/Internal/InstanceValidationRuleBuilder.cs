﻿#region License
// Copyright (c) Niklas Wendel 2017-2018
// 
// Licensed under the Apache License, Version 2.0 (the "License"); 
// you may not use this file except in compliance with the License. 
// You may obtain a copy of the License at 
// 
// http://www.apache.org/licenses/LICENSE-2.0 
// 
// Unless required by applicable law or agreed to in writing, software 
// distributed under the License is distributed on an "AS IS" BASIS, 
// WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied. 
// See the License for the specific language governing permissions and 
// limitations under the License.
#endregion
using LoremIpsumCMS.Infrastructure.Validation.ValueValidators;

namespace LoremIpsumCMS.Infrastructure.Validation.Internal
{

    /// <summary>
    /// 
    /// </summary>
    public class InstanceValidationRuleBuilder<T> : AbstractValidationRuleBuilder<T>
    {

        #region Fields

        private readonly InstanceValidationRule<T> _instanceValidationRule;

        #endregion


        /// <inheritdoc />
        /// <summary>
        /// </summary>
        /// <param name="instanceValidationRule"></param>
        public InstanceValidationRuleBuilder(InstanceValidationRule<T> instanceValidationRule) 
            : base(instanceValidationRule)
        {
            _instanceValidationRule = instanceValidationRule;
        }

        /// <inheritdoc />
        /// <summary>
        /// </summary>
        /// <param name="valueValidator"></param>
        public override void AddValueValidator(IValueValidator<T> valueValidator)
        {
            _instanceValidationRule.AddValueValidator(valueValidator);
        }

    }

}
