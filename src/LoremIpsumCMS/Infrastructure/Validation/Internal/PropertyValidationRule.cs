﻿#region License
// Copyright (c) Niklas Wendel 2017-2018
// 
// Licensed under the Apache License, Version 2.0 (the "License"); 
// you may not use this file except in compliance with the License. 
// You may obtain a copy of the License at 
// 
// http://www.apache.org/licenses/LICENSE-2.0 
// 
// Unless required by applicable law or agreed to in writing, software 
// distributed under the License is distributed on an "AS IS" BASIS, 
// WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied. 
// See the License for the specific language governing permissions and 
// limitations under the License.
#endregion
using LoremIpsumCMS.Infrastructure.Linq;
using LoremIpsumCMS.Infrastructure.Linq.Expressions;
using LoremIpsumCMS.Infrastructure.Validation.ValueValidators;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Linq.Expressions;
using System.Threading.Tasks;

namespace LoremIpsumCMS.Infrastructure.Validation.Internal
{

    /// <summary>
    /// 
    /// </summary>
    public class PropertyValidationRule<T, TResult> : AbstractValidationRule<T>
    {

        #region Fields

        private readonly string _propertyName;
        private readonly Func<T, TResult> _property;
        private readonly List<IValueValidator<TResult>> _valueValidators = new List<IValueValidator<TResult>>();

        #endregion

        #region Constructor

        /// <summary>
        /// 
        /// </summary>
        /// <param name="propertyExpression"></param>
        public PropertyValidationRule(Expression<Func<T, TResult>> propertyExpression)
        {
            _propertyName = propertyExpression.GetPropertyName();
            _property = propertyExpression.Compile();
        }

        #endregion

        #region Validate Async

        /// <summary>
        /// 
        /// </summary>
        /// <param name="context"></param>
        /// <returns></returns>
        public override async Task<IEnumerable<ValidationMessage>> ValidateAsync(IValidationContext<T> context)
        {
            var value = _property(context.Instance);
            var propertyChain = new PropertyChain(context.PropertyChain, _propertyName);

            var messages = await _valueValidators
                .SelectManyAsync(x => x.ValidateAsync(propertyChain, value, context.ServiceProvider));

            // TODO: This code is duplicated between property and instance validator
            if (WithMessage != null && messages.Any())
            {
                return new[] { ValidationMessage.Create(propertyChain, WithMessage) };
            }

            return messages;
        }

        #endregion

        #region Add Value Validator

        /// <summary>
        /// 
        /// </summary>
        /// <param name="valueValidator"></param>
        public void AddValueValidator(IValueValidator<TResult> valueValidator)
        {
            _valueValidators.Add(valueValidator);
        }

        #endregion

    }

}
