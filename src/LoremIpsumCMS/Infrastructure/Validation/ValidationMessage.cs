﻿#region License
// Copyright (c) Niklas Wendel 2017-2018
// 
// Licensed under the Apache License, Version 2.0 (the "License"); 
// you may not use this file except in compliance with the License. 
// You may obtain a copy of the License at 
// 
// http://www.apache.org/licenses/LICENSE-2.0 
// 
// Unless required by applicable law or agreed to in writing, software 
// distributed under the License is distributed on an "AS IS" BASIS, 
// WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied. 
// See the License for the specific language governing permissions and 
// limitations under the License.
#endregion
using LoremIpsumCMS.Infrastructure.Linq.Expressions;
using LoremIpsumCMS.Infrastructure.Validation.Internal;
using System;
using System.Linq.Expressions;

namespace LoremIpsumCMS.Infrastructure.Validation
{

    /// <summary>
    /// 
    /// </summary>
    public class ValidationMessage
    {

        #region Constructor

        /// <summary>
        /// 
        /// </summary>
        /// <param name="text"></param>
        private ValidationMessage(string text)
        {
            PropertyName = string.Empty;
            Text = text;
        }

        /// <summary>
        /// 
        /// </summary>
        /// <param name="propertyName"></param>
        /// <param name="text"></param>
        private ValidationMessage(string propertyName, string text)
        {
            PropertyName = propertyName;
            Text = text;
        }

        #endregion

        #region Properties

        /// <summary>
        /// 
        /// </summary>
        public string PropertyName { get; }

        /// <summary>
        /// 
        /// </summary>
        public string Text { get; }

        #endregion

        #region Create

        /// <summary>
        /// 
        /// </summary>
        /// <param name="text"></param>
        /// <returns></returns>
        public static ValidationMessage Create(string text)
        {
            if (string.IsNullOrWhiteSpace(text))
            {
                throw new ArgumentNullException(nameof(text));
            }

            return new ValidationMessage(text);
        }

        /// <summary>
        /// 
        /// </summary>
        /// <param name="propertyName"></param>
        /// <param name="text"></param>
        /// <returns></returns>
        public static ValidationMessage Create(string propertyName, string text)
        {
            if (propertyName == null)
            {
                throw new ArgumentNullException(nameof(propertyName));
            }
            if (string.IsNullOrWhiteSpace(text))
            {
                throw new ArgumentNullException(nameof(text));
            }

            return new ValidationMessage(propertyName, text);
        }

        /// <summary>
        /// 
        /// </summary>
        /// <param name="propertyChain"></param>
        /// <param name="text"></param>
        /// <returns></returns>
        public static ValidationMessage Create(PropertyChain propertyChain, string text)
        {
            if (propertyChain == null)
            {
                throw new ArgumentNullException(nameof(propertyChain));
            }
            if (string.IsNullOrWhiteSpace(text))
            {
                throw new ArgumentNullException(nameof(text));
            }

            var propertyName = propertyChain.FullName;
            return new ValidationMessage(propertyName, text);
        }

        /// <summary>
        /// 
        /// </summary>
        /// <typeparam name="T"></typeparam>
        /// <param name="propertyExpression"></param>
        /// <param name="text"></param>
        /// <returns></returns>
        public static ValidationMessage Create<T>(Expression<Func<T, object>> propertyExpression, string text)
        {
            if (propertyExpression == null)
            {
                throw new ArgumentNullException(nameof(propertyExpression));
            }

            var propertyName = propertyExpression.GetPropertyName();
            return Create(propertyName, text);
        }


        #endregion

    }

}
