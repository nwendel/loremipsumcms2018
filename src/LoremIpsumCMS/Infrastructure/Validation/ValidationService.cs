﻿#region License
// Copyright (c) Niklas Wendel 2017-2018
// 
// Licensed under the Apache License, Version 2.0 (the "License"); 
// you may not use this file except in compliance with the License. 
// You may obtain a copy of the License at 
// 
// http://www.apache.org/licenses/LICENSE-2.0 
// 
// Unless required by applicable law or agreed to in writing, software 
// distributed under the License is distributed on an "AS IS" BASIS, 
// WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied. 
// See the License for the specific language governing permissions and 
// limitations under the License.
#endregion
using LoremIpsumCMS.Infrastructure.Linq;
using LoremIpsumCMS.Infrastructure.Reflection;
using Microsoft.Extensions.DependencyInjection;
using System;
using System.Collections.Generic;
using System.Threading.Tasks;

namespace LoremIpsumCMS.Infrastructure.Validation
{

    /// <summary>
    /// 
    /// </summary>
    public class ValidationService : IValidationService
    {

        #region Dependencies

        private readonly IServiceProvider _serviceProvider;

        #endregion

        #region Constructor

        /// <summary>
        /// 
        /// </summary>
        /// <param name="serviceProvider"></param>
        public ValidationService(IServiceProvider serviceProvider)
        {
            _serviceProvider = serviceProvider;
        }

        #endregion

        #region Validate Async

        /// <summary>
        /// 
        /// </summary>
        /// <param name="instance"></param>
        /// <returns></returns>
        public Task<IEnumerable<ValidationMessage>> ValidateAsync(object instance)
        {
            if (instance == null)
            {
                throw new ArgumentNullException(nameof(instance));
            }

            var instanceType = instance.GetType();
            var validateType = typeof(IValidator<>).MakeGenericType(instanceType);
            var methodInfo = validateType.GetMethod(nameof(IValidator<object>.ValidateAsync));
            var contextType = typeof(ValidationContext<>).MakeGenericType(instanceType);
            var context = Activator.CreateInstance(contextType, instance, _serviceProvider);

            var validators = _serviceProvider.GetServices(validateType);
            var messages = validators.SelectManyAsync(x => methodInfo.InvokeAsync<IEnumerable<ValidationMessage>>(x, new[] {context}));
            return messages;
        }

        #endregion

    }

}
