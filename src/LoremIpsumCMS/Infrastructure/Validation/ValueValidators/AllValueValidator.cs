﻿#region License
// Copyright (c) Niklas Wendel 2017-2018
// 
// Licensed under the Apache License, Version 2.0 (the "License"); 
// you may not use this file except in compliance with the License. 
// You may obtain a copy of the License at 
// 
// http://www.apache.org/licenses/LICENSE-2.0 
// 
// Unless required by applicable law or agreed to in writing, software 
// distributed under the License is distributed on an "AS IS" BASIS, 
// WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied. 
// See the License for the specific language governing permissions and 
// limitations under the License.
#endregion
using LoremIpsumCMS.Infrastructure.Linq;
using LoremIpsumCMS.Infrastructure.Validation.Internal;
using System;
using System.Collections.Generic;
using System.Threading.Tasks;

namespace LoremIpsumCMS.Infrastructure.Validation.ValueValidators
{
    /// <summary>
    /// 
    /// </summary>
    /// <typeparam name="T"></typeparam>
    /// <typeparam name="TItem"></typeparam>
    public class AllValueValidator<T, TItem> : IValueValidator<T>
        where T : IEnumerable<TItem>
    {

        #region Fields

        private readonly InstanceValidationRule<TItem> _instanceRule = new InstanceValidationRule<TItem>();

        #endregion

        #region Constructor

        /// <summary>
        /// 
        /// </summary>
        /// <param name="action"></param>
        public AllValueValidator(Action<IValidationRuleBuilder<TItem>> action)
        {
            var ruleBuilder = new InstanceValidationRuleBuilder<TItem>(_instanceRule);
            action(ruleBuilder);
        }

        #endregion

        #region Validate Async

        /// <summary>
        /// 
        /// </summary>
        /// <param name="propertyChain"></param>
        /// <param name="value"></param>
        /// <param name="serviceProvider"></param>
        /// <returns></returns>
        public Task<IEnumerable<ValidationMessage>> ValidateAsync(PropertyChain propertyChain, T value, IServiceProvider serviceProvider)
        {
            var safeValue = (IEnumerable<TItem>) value ?? new TItem[0];
            var messages = safeValue.SelectManyAsync((item, ix) =>
            {
                var itemPropertyChain = new PropertyChain(propertyChain, ix);
                var context = new ValidationContext<TItem>(itemPropertyChain, item, serviceProvider);
                var itemMessages = _instanceRule.ValidateAsync(context);
                return itemMessages;
            });
            return messages;
        }

        #endregion

    }

}
