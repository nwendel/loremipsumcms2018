﻿#region License
// Copyright (c) Niklas Wendel 2017-2018
// 
// Licensed under the Apache License, Version 2.0 (the "License"); 
// you may not use this file except in compliance with the License. 
// You may obtain a copy of the License at 
// 
// http://www.apache.org/licenses/LICENSE-2.0 
// 
// Unless required by applicable law or agreed to in writing, software 
// distributed under the License is distributed on an "AS IS" BASIS, 
// WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied. 
// See the License for the specific language governing permissions and 
// limitations under the License.
#endregion
using LoremIpsumCMS.Events;
using LoremIpsumCMS.Infrastructure;
using LoremIpsumCMS.Infrastructure.Cqrs;
using LoremIpsumCMS.Infrastructure.Model;
using LoremIpsumCMS.Infrastructure.Raven.UniqueConstraints;
using Newtonsoft.Json;
using System;
using System.Collections.Generic;
using System.Collections.ObjectModel;
using System.Linq;

namespace LoremIpsumCMS.Model
{

    /// <summary>
    /// 
    /// </summary>
    public class Site : AbstractAggregate,
        IApplyEvent<SiteCreatedEvent>,
        IApplyEvent<SiteUpdatedEvent>
    {

        #region Properties

        /// <summary>
        /// 
        /// </summary>
        public string Name { get; protected set; }

        /// <summary>
        /// 
        /// </summary>
        [UniqueConstraint]
        public string NameSlug => Name.Slugify();

        /// <summary>
        /// 
        /// </summary>
        protected IList<SiteHost> HostsInner { get; set; }

        /// <summary>
        /// 
        /// </summary>
        [JsonIgnore]
        public IEnumerable<SiteHost> Hosts => HostsInner != null
            ? new ReadOnlyCollection<SiteHost>(HostsInner)
            : null;

        /// <summary>
        /// 
        /// </summary>
        public DateTime CreatedAt { get; protected set; }

        /// <summary>
        /// 
        /// </summary>
        public string CreatedByUserId { get; protected set; }

        /// <summary>
        /// 
        /// </summary>
        public DateTime? LastUpdatedAt { get; protected set; }

        #endregion

        #region Apply Created

        /// <summary>
        /// 
        /// </summary>
        /// <param name="event"></param>
        public void Apply(SiteCreatedEvent @event)
        {
            Name = @event.Name;
            HostsInner = @event.Hosts.ToList();
            CreatedAt = @event.CreatedAt;
            CreatedByUserId = @event.CreatedByUserId;
        }

        #endregion

        #region Apply Updated

        /// <summary>
        /// 
        /// </summary>
        /// <param name="event"></param>
        public void Apply(SiteUpdatedEvent @event)
        {
            Name = @event.Name;
            HostsInner = @event.Hosts.ToList();
            LastUpdatedAt = @event.UpdatedAt;
            //LastUpdatedByUserId = @event.LastUpdatedByUserId;
        }

        #endregion

    }

}
