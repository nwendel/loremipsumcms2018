﻿#region License
// Copyright (c) Niklas Wendel 2017-2018
// 
// Licensed under the Apache License, Version 2.0 (the "License"); 
// you may not use this file except in compliance with the License. 
// You may obtain a copy of the License at 
// 
// http://www.apache.org/licenses/LICENSE-2.0 
// 
// Unless required by applicable law or agreed to in writing, software 
// distributed under the License is distributed on an "AS IS" BASIS, 
// WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied. 
// See the License for the specific language governing permissions and 
// limitations under the License.
#endregion
using LoremIpsumCMS.Infrastructure.Raven;
using LoremIpsumCMS.Infrastructure.Raven.UniqueConstraints;
using LoremIpsumCMS.Model;
using System;
using System.Collections.Generic;
using System.Threading.Tasks;

namespace LoremIpsumCMS.Queries
{

    /// <summary>
    /// 
    /// </summary>
    public static class HostLoadByExtensions
    {

        #region Name

        /// <summary>
        /// 
        /// </summary>
        /// <param name="self"></param>
        /// <param name="name"></param>
        /// <returns></returns>
        public static Task<Host> Name(this AsyncDocumentSessionLoadBy<Host> self, string name)
        {
            if (string.IsNullOrEmpty(name))
            {
                throw new ArgumentNullException(nameof(name));
            }

            var host = self.AsyncSession.LoadByUniqueConstraintAsync<Host>(x => x.Name, name);
            return host;
        }

        #endregion

        #region Names

        /// <summary>
        /// 
        /// </summary>
        /// <param name="self"></param>
        /// <param name="names"></param>
        /// <returns></returns>
        public static Task<Dictionary<string, Host>> Names(this AsyncDocumentSessionLoadBy<Host> self, string[] names)
        {
            if (names == null)
            {
                throw new ArgumentNullException(nameof(names));
            }

            var hosts = self.AsyncSession.LoadByUniqueConstraintAsync<Host>(x => x.Name, names);
            return hosts;
        }

        #endregion

    }

}