﻿#region License
// Copyright (c) Niklas Wendel 2017-2018
// 
// Licensed under the Apache License, Version 2.0 (the "License"); 
// you may not use this file except in compliance with the License. 
// You may obtain a copy of the License at 
// 
// http://www.apache.org/licenses/LICENSE-2.0 
// 
// Unless required by applicable law or agreed to in writing, software 
// distributed under the License is distributed on an "AS IS" BASIS, 
// WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied. 
// See the License for the specific language governing permissions and 
// limitations under the License.
#endregion
using LoremIpsumCMS.Infrastructure.Raven;
using LoremIpsumCMS.Infrastructure.Raven.UniqueConstraints;
using LoremIpsumCMS.Model;
using System;
using System.Threading.Tasks;

namespace LoremIpsumCMS.Queries
{

    /// <summary>
    /// 
    /// </summary>
    public static class SiteLoadByExtensions
    {

        #region Name

        /// <summary>
        /// 
        /// </summary>
        /// <param name="self"></param>
        /// <param name="nameSlug"></param>
        /// <returns></returns>
        public static Task<Site> NameSlug(this AsyncDocumentSessionLoadBy<Site> self, string nameSlug)
        {
            if (string.IsNullOrEmpty(nameSlug))
            {
                throw new ArgumentNullException(nameof(nameSlug));
            }

            var site = self.AsyncSession.LoadByUniqueConstraintAsync<Site>(x => x.NameSlug, nameSlug);
            return site;
        }

        #endregion


    }

}
