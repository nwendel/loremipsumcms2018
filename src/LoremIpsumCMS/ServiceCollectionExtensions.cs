﻿#region License
// Copyright (c) Niklas Wendel 2017-2018
// 
// Licensed under the Apache License, Version 2.0 (the "License"); 
// you may not use this file except in compliance with the License. 
// You may obtain a copy of the License at 
// 
// http://www.apache.org/licenses/LICENSE-2.0 
// 
// Unless required by applicable law or agreed to in writing, software 
// distributed under the License is distributed on an "AS IS" BASIS, 
// WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied. 
// See the License for the specific language governing permissions and 
// limitations under the License.
#endregion
using AttachedProperties;
using FluentRegistration;
using LoremIpsumCMS.Infrastructure.Modules;
using Microsoft.Extensions.DependencyInjection;
using System;

namespace LoremIpsumCMS
{

    /// <summary>
    /// 
    /// </summary>
    public static class ServiceCollectionExtensions
    {

        #region Use Lorem Ipsum

        /// <summary>
        /// 
        /// </summary>
        /// <param name="self"></param>
        public static void AddLoremIpsum(this IServiceCollection self)
        {
            self.AddOptions();

            var moduleLoader = new ModuleLoader();
            var modules = moduleLoader.LoadAll();

            foreach (var module in modules)
            {
                self.Register(c => c
                    .Instance(module)
                    .WithServices.AllInterfaces());
                module.ConfigureServices(self);
            }
            self.SetAttachedValue(ModulesAttachedProperty.AreModulesLoaded, true);
        }

        /// <summary>
        /// 
        /// </summary>
        /// <param name="self"></param>
        /// <param name="optionsAction"></param>
        public static void AddLoremIpsum(this IServiceCollection self, Action<LoremIpsumOptions> optionsAction)
        {
            self.AddLoremIpsum();
            self.Configure(optionsAction);
        }

        #endregion

    }

}
