﻿#region License
// Copyright (c) Niklas Wendel 2017-2018
// 
// Licensed under the Apache License, Version 2.0 (the "License"); 
// you may not use this file except in compliance with the License. 
// You may obtain a copy of the License at 
// 
// http://www.apache.org/licenses/LICENSE-2.0 
// 
// Unless required by applicable law or agreed to in writing, software 
// distributed under the License is distributed on an "AS IS" BASIS, 
// WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied. 
// See the License for the specific language governing permissions and 
// limitations under the License.
#endregion
using LoremIpsumCMS;
using LoremIpsumCMS.Web.Areas.AdminShell.Controllers;
using Microsoft.AspNetCore.Builder;
using Microsoft.AspNetCore.Hosting;
using Microsoft.AspNetCore.TestHost;
using Microsoft.Extensions.DependencyInjection;
using MvcRouteTester.AspNetCore;

namespace Ryoukai.Web.Tests
{

    /// <summary>
    /// 
    /// </summary>
    public class RouteTestFixture
    {

        /// <summary>
        /// 
        /// </summary>
        public RouteTestFixture()
        {
            Server = new TestServer(new WebHostBuilder()
                .UseStartup<TestStartup>()
                .UseSetting(WebHostDefaults.ApplicationKey, typeof(Startup).Assembly.FullName));
        }

        /// <summary>
        /// 
        /// </summary>
        public TestServer Server { get; }

        /// <summary>
        /// 
        /// </summary>
        public class TestStartup
        {

            /// <summary>
            /// 
            /// </summary>
            /// <param name="services"></param>
            public void ConfigureServices(IServiceCollection services)
            {
                services.AddLoremIpsum();

                // TODO: Why is AddApplicationPart needed during testing, but not when running the application?
                services.AddMvc()
                    .AddApplicationPart(typeof(AdminDashboardController).Assembly);     
                services.AddMvcRouteTester();
            }

            /// <summary>
            /// 
            /// </summary>
            /// <param name="app"></param>
            /// <param name="env"></param>
            public void Configure(IApplicationBuilder app, IHostingEnvironment env)
            {
                app.UseMvc(routes =>
                {
                    routes.MapRoute(
                        name: "default",
                        template: "{controller=Home}/{action=Index}/{id?}");
                });
            }

        }

    }

}
