﻿#region License
// Copyright (c) Niklas Wendel 2017-2018
// 
// Licensed under the Apache License, Version 2.0 (the "License"); 
// you may not use this file except in compliance with the License. 
// You may obtain a copy of the License at 
// 
// http://www.apache.org/licenses/LICENSE-2.0 
// 
// Unless required by applicable law or agreed to in writing, software 
// distributed under the License is distributed on an "AS IS" BASIS, 
// WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied. 
// See the License for the specific language governing permissions and 
// limitations under the License.
#endregion
using LoremIpsumCMS.Web.Infrastructure.Mvc;
using Microsoft.AspNetCore.Mvc;

namespace ViewComponentActionApplication.Controllers
{

    /// <summary>
    /// 
    /// </summary>
    public class TestController : Controller
    {

        /// <summary>
        /// 
        /// </summary>
        /// <returns></returns>
        [HttpGet(nameof(SimpleViewComponentAction))]
        public IActionResult SimpleViewComponentAction()
        {
            return View();
        }

        /// <summary>
        /// 
        /// </summary>
        /// <returns></returns>
        [HttpGet(nameof(AreaViewComponentAction))]
        public IActionResult AreaViewComponentAction()
        {
            return View();
        }

        /// <summary>
        /// 
        /// </summary>
        /// <returns></returns>
        [HttpGet(nameof(RecursiveViewComponentAction))]
        public IActionResult RecursiveViewComponentAction()
        {
            return View();
        }

        /// <summary>
        /// 
        /// </summary>
        /// <returns></returns>
        [HttpGet(nameof(ArgumentsViewComponentAction))]
        public IActionResult ArgumentsViewComponentAction()
        {
            return View();
        }

        /// <summary>
        /// 
        /// </summary>
        /// <returns></returns>
        [ViewComponentAction]
        public IActionResult Component()
        {
            return Content("Component");
        }

        /// <summary>
        /// 
        /// </summary>
        /// <returns></returns>
        [ViewComponentAction]
        public IActionResult ParentComponent()
        {
            return PartialView();
        }

        /// <summary>
        /// 
        /// </summary>
        /// <param name="first"></param>
        /// <param name="second"></param>
        /// <returns></returns>
        [ViewComponentAction]
        public IActionResult ArgumentsComponent(string first, int second)
        {
            return Content($"{first} {second}");
        }

    }

}
