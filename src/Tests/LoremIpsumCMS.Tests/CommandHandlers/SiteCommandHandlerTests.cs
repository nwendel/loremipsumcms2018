﻿#region License
// Copyright (c) Niklas Wendel 2017-2018
// 
// Licensed under the Apache License, Version 2.0 (the "License"); 
// you may not use this file except in compliance with the License. 
// You may obtain a copy of the License at 
// 
// http://www.apache.org/licenses/LICENSE-2.0 
// 
// Unless required by applicable law or agreed to in writing, software 
// distributed under the License is distributed on an "AS IS" BASIS, 
// WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied. 
// See the License for the specific language governing permissions and 
// limitations under the License.
#endregion
using LoremIpsumCMS.CommandHandlers;
using LoremIpsumCMS.Commands;
using LoremIpsumCMS.Events;
using LoremIpsumCMS.Exceptions;
using LoremIpsumCMS.Infrastructure.Raven.UniqueConstraints;
using LoremIpsumCMS.Model;
using LoremIpsumCMS.TestHelper.Cqrs;
using System;
using System.Threading.Tasks;
using Xunit;

namespace LoremIpsumCMS.Tests.CommandHandlers
{

    /// <summary>
    /// 
    /// </summary>
    public class SiteCommandHandlerTests : AbstractCommandHandlerTests<Site>
    {

        /// <summary>
        /// 
        /// </summary>
        public SiteCommandHandlerTests()
        {
            CommandHandler = new SiteCommandHandler(AsyncDocumentSession);
        }

        /// <summary>
        /// 
        /// </summary>
        [Fact]
        public async Task CanHandleCreateSite()
        {
            var now = DateTime.Now;

            await Test(
                Given(),
                When(
                    new CreateSiteCommand
                    {
                        Name = "some site",
                        Hosts = new[] {new SiteHost("localhost", true)},
                        CreatedAt = now,
                        CreatedByUserId = "Users/1-A"
                    }),
                Then(
                    new SiteCreatedEvent
                    {
                        Name = "some site",
                        Hosts = new[] {new SiteHost("localhost", true)},
                        CreatedAt = now,
                        CreatedByUserId = "Users/1-A"
                    }));
        }

        /// <summary>
        /// 
        /// </summary>
        [Fact]
        public async Task ThrowsOnHandleCreateSiteExists()
        {
            var now = DateTime.Now;

            var site = new Site();
            site.ApplyEvent(new SiteCreatedEvent
            {
                Name = "some site",
                Hosts = new[] {new SiteHost("localhost", true)},
                CreatedAt = now,
                CreatedByUserId = "Users/1-A"
            });
            await AsyncDocumentSession.StoreAsync(site);
            await AsyncDocumentSession.SaveChangesExAsync();

            await Test(
                Given(),
                When(new CreateSiteCommand
                {
                    Name = "some site",
                    Hosts = new[] {new SiteHost("localhost", true)},
                    CreatedAt = now,
                    CreatedByUserId = "Users/1-A"
                }),
                ThenFailWithException<SiteExistsException>());
        }

        /// <summary>
        /// 
        /// </summary>
        [Fact]
        public async Task ThrowsOnHandleCreateHostExists()
        {
            var now = DateTime.Now;

            var host = new Host("localhost", "Sites/99-A", false);
            await AsyncDocumentSession.StoreAsync(host);
            await AsyncDocumentSession.SaveChangesExAsync();

            await Test(
                Given(),
                When(new CreateSiteCommand
                {
                    Name = "some site",
                    Hosts = new[] {new SiteHost("localhost", true)},
                    CreatedAt = now,
                    CreatedByUserId = "Users/1-A"
                }),
                ThenFailWithException<HostExistsException>());
        }

        /// <summary>
        /// 
        /// </summary>
        [Fact]
        public async Task CanHandleUpdateSite()
        {
            var now = DateTime.Now;

            await Test(
                Given(new SiteCreatedEvent
                {
                    Name = "some site",
                    Hosts = new[] {new SiteHost("localhost", true)},
                    CreatedAt = now,
                    CreatedByUserId = "Users/1-A"
                }),
                When(new UpdateSiteCommand
                {
                    Name = "new name",
                    Hosts = new[] {new SiteHost("another.host", true)},
                    UpdatedAt = now,
                    UpdatedByUserId = "Users/1-A"
                }),
                Then(new SiteUpdatedEvent
                {
                    Name = "new name",
                    Hosts = new[] { new SiteHost("another.host", true) },
                    UpdatedAt = now
                }));
        }

        /// <summary>
        /// 
        /// </summary>
        [Fact]
        public async Task CanHandleDeleteSite()
        {
            var now = DateTime.Now;

            await Test(
                Given(new SiteCreatedEvent
                {
                    Name = "some site",
                    Hosts = new[] {new SiteHost("localhost", true)},
                    CreatedAt = now,
                    CreatedByUserId = "Users/1-A"
                }),
                When(new DeleteSiteCommand()),
                Then(new SiteDeletedEvent()));
        }

        /// <summary>
        /// 
        /// </summary>
        [Fact]
        public async Task ThrowsOnHandleDeleteSiteNotFound()
        {
            var now = DateTime.Now;

            await Test(
                Given(),
                When(new DeleteSiteCommand()),
                ThenFailWithException<SiteNotFoundException>());
        }

    }

}
