﻿#region License
// Copyright (c) Niklas Wendel 2017-2018
// 
// Licensed under the Apache License, Version 2.0 (the "License"); 
// you may not use this file except in compliance with the License. 
// You may obtain a copy of the License at 
// 
// http://www.apache.org/licenses/LICENSE-2.0 
// 
// Unless required by applicable law or agreed to in writing, software 
// distributed under the License is distributed on an "AS IS" BASIS, 
// WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied. 
// See the License for the specific language governing permissions and 
// limitations under the License.
#endregion
using LoremIpsumCMS.EventHandlers;
using LoremIpsumCMS.Events;
using LoremIpsumCMS.Infrastructure.Raven;
using LoremIpsumCMS.Model;
using LoremIpsumCMS.Queries;
using LoremIpsumCMS.TestHelper.Cqrs;
using System;
using System.Threading.Tasks;
using Xunit;

namespace LoremIpsumCMS.Tests.EventHandlers
{

    /// <summary>
    /// 
    /// </summary>
    public class HostEventHandlerTests : AbstractEventHandlerTests
    {

        private readonly HostEventHandler _tested;

        /// <summary>
        /// 
        /// </summary>
        public HostEventHandlerTests()
        {
            _tested = new HostEventHandler(AsyncDocumentSession);
        }

        /// <summary>
        /// 
        /// </summary>
        [Fact]
        public async Task CanHandleSiteCreated()
        {
            await _tested.HandleAsync(new SiteCreatedEvent
            {
                Name = "the-site",
                Hosts = new[] {new SiteHost("localhost", true)},
                CreatedAt = DateTime.Now,
                CreatedByUserId = "Users/1-A"
            });

            var host = AsyncDocumentSession.LoadByAsync<Host>(x => x.Name("localhost"));

            Assert.NotNull(host);
        }

    }

}
