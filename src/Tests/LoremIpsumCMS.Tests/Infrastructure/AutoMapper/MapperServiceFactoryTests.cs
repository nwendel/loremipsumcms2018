﻿#region License
// Copyright (c) Niklas Wendel 2017-2018
// 
// Licensed under the Apache License, Version 2.0 (the "License"); 
// you may not use this file except in compliance with the License. 
// You may obtain a copy of the License at 
// 
// http://www.apache.org/licenses/LICENSE-2.0 
// 
// Unless required by applicable law or agreed to in writing, software 
// distributed under the License is distributed on an "AS IS" BASIS, 
// WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied. 
// See the License for the specific language governing permissions and 
// limitations under the License.
#endregion
using AutoMapper;
using FluentRegistration;
using LoremIpsumCMS.Infrastructure.AutoMapper;
using Microsoft.Extensions.DependencyInjection;
using System;
using Xunit;

namespace LoremIpsumCMS.Tests.Infrastructure.AutoMapper
{

    /// <summary>
    /// 
    /// </summary>
    public class MapperServiceFactoryTests
    {

        /// <summary>
        /// 
        /// </summary>
        [Fact]
        public void CanCreateMapper()
        {
            var services = new ServiceCollection();
            services.Install<MapperServiceInstaller>();
            services.AddSingleton<Profile, DummyMapperProfile>();
            services.AddSingleton<DummyValueResolver>();
            var serviceProvider = services.BuildServiceProvider();

            var factory = serviceProvider.GetRequiredService<MapperServiceFactory>();
            factory.Initialize();
            var mapper = serviceProvider.GetRequiredService<IMapper>();

            var source = new Customer {Age = 42};
            var destination = mapper.Map<Customer>(source);

            Assert.Equal(42, destination.Age);
        }

        /// <summary>
        /// 
        /// </summary>
        [Fact]
        public void ThrowsOnCreateMapperNotInitialized()
        {
            var services = new ServiceCollection();
            services.Install<MapperServiceInstaller>();
            var serviceProvider = services.BuildServiceProvider();

            Assert.Throws<InvalidOperationException>(() => serviceProvider.GetRequiredService<IMapper>());
        }

        #region Mapper Profile / Model

        /// <summary>
        /// 
        /// </summary>
        public class DummyMapperProfile : Profile
        {

            public DummyMapperProfile()
            {
                CreateMap<Customer, Customer>()
                    .ForMember(d => d.Age, o => o.MapFrom<DummyValueResolver>());
            }

        }

        /// <summary>
        /// 
        /// </summary>
        public class DummyValueResolver : IValueResolver<Customer, Customer, int>
        {

            /// <summary>
            /// 
            /// </summary>
            /// <param name="source"></param>
            /// <param name="destination"></param>
            /// <param name="destMember"></param>
            /// <param name="context"></param>
            /// <returns></returns>
            public int Resolve(Customer source, Customer destination, int destMember, ResolutionContext context)
            {
                return source.Age;
            }

        }

        /// <summary>
        /// 
        /// </summary>
        public class Customer
        {

            /// <summary>
            /// 
            /// </summary>
            public int Age { get; set; }

        }

        #endregion

    }

}
