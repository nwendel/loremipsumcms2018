﻿#region License
// Copyright (c) Niklas Wendel 2017-2019
// 
// Licensed under the Apache License, Version 2.0 (the "License"); 
// you may not use this file except in compliance with the License. 
// You may obtain a copy of the License at 
// 
// http://www.apache.org/licenses/LICENSE-2.0 
// 
// Unless required by applicable law or agreed to in writing, software 
// distributed under the License is distributed on an "AS IS" BASIS, 
// WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied. 
// See the License for the specific language governing permissions and 
// limitations under the License.
#endregion
using AutoMapper;
using LoremIpsumCMS.Infrastructure.AutoMapper;
using System.Collections.Generic;
using System.Linq;
using Xunit;

namespace LoremIpsumCMS.Tests.Infrastructure.AutoMapper
{

    /// <summary>
    /// 
    /// </summary>
    public class ObjectMapToExtensionsTests
    {

        private readonly IMapper _mapper;

        public ObjectMapToExtensionsTests()
        {
            var configuration = new MapperConfiguration(c =>
            {
                c.CreateMap<Source, Destination>();
            });
            _mapper = configuration.CreateMapper();
        }

        /// <summary>
        /// 
        /// </summary>
        [Fact]
        public void CanMapTo()
        {
            var source = new Source {Name = "asdf"};

            var destination = source.MapTo<Destination>(_mapper);

            Assert.NotNull(destination);
            Assert.Equal("asdf", destination.Name);
        }

        /// <summary>
        /// 
        /// </summary>
        [Fact]
        public void CanMapToArray()
        {
            var source = new[] {new Source {Name = "asdf"}};

            var destination = source.MapTo<Destination>(_mapper);

            Assert.NotNull(destination);
            Assert.Single(destination);
            Assert.Equal("asdf", destination[0].Name);
        }

        /// <summary>
        /// 
        /// </summary>
        [Fact]
        public void CanMapToEnumerable()
        {
            IEnumerable<Source> source = new[] { new Source { Name = "asdf" } };

            var destination = source.MapTo<Destination>(_mapper).ToList();

            Assert.NotNull(destination);
            Assert.Single(destination);
            Assert.Equal("asdf", destination[0].Name);
        }

        /// <summary>
        /// 
        /// </summary>
        [Fact]
        public void CanMapToNullSource()
        {
            var destination = ((Source) null).MapTo<Destination>(_mapper);

            Assert.Null(destination);
        }

        /// <summary>
        /// 
        /// </summary>
        [Fact]
        public void CanMapToNullSourceArray()
        {
            var destination = ((Source[])null).MapTo<Destination>(_mapper);

            Assert.Null(destination);
        }

        /// <summary>
        /// 
        /// </summary>
        [Fact]
        public void CanMapToNullSourceEnumerable()
        {
            var destination = ((IEnumerable<Source>)null).MapTo<Destination>(_mapper);

            Assert.Null(destination);
        }

        #region Model

        /// <summary>
        /// 
        /// </summary>
        public class Source
        {

            /// <summary>
            /// 
            /// </summary>
            public string Name { get; set; }

        }

        /// <summary>
        /// 
        /// </summary>
        public class Destination
        {

            /// <summary>
            /// 
            /// </summary>
            public string Name { get; set; }

        }

        #endregion

    }



}
