﻿#region License
// Copyright (c) Niklas Wendel 2017-2018
// 
// Licensed under the Apache License, Version 2.0 (the "License"); 
// you may not use this file except in compliance with the License. 
// You may obtain a copy of the License at 
// 
// http://www.apache.org/licenses/LICENSE-2.0 
// 
// Unless required by applicable law or agreed to in writing, software 
// distributed under the License is distributed on an "AS IS" BASIS, 
// WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied. 
// See the License for the specific language governing permissions and 
// limitations under the License.
#endregion
using LoremIpsumCMS.Infrastructure.Collections;
using LoremIpsumCMS.Infrastructure.Cqrs;
using LoremIpsumCMS.Infrastructure.EventStore;
using LoremIpsumCMS.Infrastructure.Model;
using LoremIpsumCMS.Infrastructure.Validation;
using Moq;
using Raven.Client.Documents.Session;
using Raven.TestDriver;
using System;
using System.Collections.Generic;
using System.Threading.Tasks;
using Xunit;

namespace LoremIpsumCMS.Tests.Infrastructure.Cqrs
{

    /// <summary>
    /// 
    /// </summary>
    public class RavenCommandSenderSendAsyncTests : RavenTestDriver
    {

        private readonly RavenCommandSender _tested;

        private readonly IAsyncDocumentSession _asyncDocumentSession;

        private readonly Mock<IServiceProvider> _serviceProviderMock = new Mock<IServiceProvider>();
        private readonly Mock<IValidationService> _validationServiceMock = new Mock<IValidationService>();
        private readonly Mock<IEventStore> _eventStoreMock = new Mock<IEventStore>();

        /// <summary>
        /// 
        /// </summary>
        public RavenCommandSenderSendAsyncTests()
        {
            _asyncDocumentSession = GetDocumentStore(null, Guid.NewGuid().ToString()).OpenAsyncSession();
            _tested = new RavenCommandSender(
                _serviceProviderMock.Object, 
                _validationServiceMock.Object,
                _eventStoreMock.Object, 
                _asyncDocumentSession);
        }

        /// <summary>
        /// 
        /// </summary>
        /// <returns></returns>
        [Fact]
        public async Task ThrowsOnSendAsyncNullCommand()
        {
            await Assert.ThrowsAsync<ArgumentNullException>(() => _tested.SendAsync((AbstractCommand<AbstractAggregate>)null));
        }

        /// <summary>
        /// 
        /// </summary>
        /// <returns></returns>
        [Fact]
        public async Task CanSendAsyncCreateCommand()
        {
            var someCommandHandler = new SomeCommandHandler();
            var someEventSubscriber = new SomeEventSubscriber();

            _serviceProviderMock
                .Setup(x => x.GetService(typeof(IHandleCommand<SomeCreateCommand>)))
                .Returns(someCommandHandler);
            _serviceProviderMock
                .Setup(x => x.GetService(typeof(IEnumerable<ISubscribeTo<SomeCreatedEvent>>)))
                .Returns(new[] {someEventSubscriber});
            _validationServiceMock
                .Setup(x => x.ValidateAsync(It.IsAny<SomeCreateCommand>()))
                .ReturnsAsync(new ValidationMessage[0]);

            var command = new SomeCreateCommand();
            var id = await _tested.SendAsync(command);
            var aggregate = await _asyncDocumentSession.LoadAsync<SomeAggregate>(id);

            Assert.True(aggregate.IsApplySomeCreatedEventCalled);
            Assert.True(someEventSubscriber.IsSomeEventHandled);
        }

        /// <summary>
        /// 
        /// </summary>
        /// <returns></returns>
        [Fact]
        public async Task CanSendAsyncUpdateCommand()
        {
            var someCommandHandler = new SomeCommandHandler();

            _serviceProviderMock
                .Setup(x => x.GetService(typeof(IHandleCommand<SomeUpdateCommand>)))
                .Returns(someCommandHandler);
            _serviceProviderMock
                .Setup(x => x.GetService(typeof(IEnumerable<ISubscribeTo<SomeUpdatedEvent>>)))
                .Returns(new object[0]);
            _validationServiceMock
                .Setup(x => x.ValidateAsync(It.IsAny<SomeUpdateCommand>()))
                .ReturnsAsync(new ValidationMessage[0]);


            var aggregate = new SomeAggregate();
            await _asyncDocumentSession.StoreAsync(aggregate);

            var command = new SomeUpdateCommand
            {
                Id = aggregate.Id
            };
            await _tested.SendAsync(command);

            Assert.True(aggregate.IsApplySomeUpdatedEventCalled);
        }

        /// <summary>
        /// 
        /// </summary>
        /// <returns></returns>
        [Fact]
        public async Task CanSendAsyncSameCommandTypeTwice()
        {
            var someCommandHandler = new SomeCommandHandler();
            var someEventSubscriber = new SomeEventSubscriber();

            _serviceProviderMock
                .Setup(x => x.GetService(typeof(IHandleCommand<SomeCreateCommand>)))
                .Returns(someCommandHandler);
            _serviceProviderMock
                .Setup(x => x.GetService(typeof(IEnumerable<ISubscribeTo<SomeCreatedEvent>>)))
                .Returns(new[] { someEventSubscriber });
            _validationServiceMock
                .Setup(x => x.ValidateAsync(It.IsAny<SomeCreateCommand>()))
                .ReturnsAsync(new ValidationMessage[0]);

            var command1 = new SomeCreateCommand();
            var id1 = await _tested.SendAsync(command1);
            var aggregate1 = await _asyncDocumentSession.LoadAsync<SomeAggregate>(id1);

            var command2 = new SomeCreateCommand();
            var id2 = await _tested.SendAsync(command2);
            var aggregate2 = await _asyncDocumentSession.LoadAsync<SomeAggregate>(id2);

            Assert.True(aggregate1.IsApplySomeCreatedEventCalled);
            Assert.True(aggregate2.IsApplySomeCreatedEventCalled);
        }

        /// <summary>
        /// 
        /// </summary>
        /// <returns></returns>
        [Fact]
        public async Task ThrowsOnSendAsyncUpdateCommandInvalidAggregateId()
        {
            var someCommandHandler = new SomeCommandHandler();

            _serviceProviderMock
                .Setup(x => x.GetService(typeof(IHandleCommand<SomeUpdateCommand>)))
                .Returns(someCommandHandler);
            _validationServiceMock
                .Setup(x => x.ValidateAsync(It.IsAny<SomeUpdateCommand>()))
                .ReturnsAsync(new ValidationMessage[0]);

            var command = new SomeUpdateCommand
            {
                Id = "dummy"
            };

            await Assert.ThrowsAsync<CqrsException>(() => _tested.SendAsync(command));
        }

        #region Helpers

        /// <summary>
        /// 
        /// </summary>
        public class SomeAggregate : AbstractAggregate,
            IApplyEvent<SomeCreatedEvent>,
            IApplyEvent<SomeUpdatedEvent>
        {

            /// <summary>
            /// 
            /// </summary>
            public bool IsApplySomeCreatedEventCalled { get; private set; }

            /// <summary>
            /// 
            /// </summary>
            public bool IsApplySomeUpdatedEventCalled { get; private set; }

            /// <summary>
            /// 
            /// </summary>
            /// <param name="event"></param>
            public void Apply(SomeCreatedEvent @event)
            {
                IsApplySomeCreatedEventCalled = true;
            }

            /// <summary>
            /// 
            /// </summary>
            /// <param name="event"></param>
            public void Apply(SomeUpdatedEvent @event)
            {
                IsApplySomeUpdatedEventCalled = true;
            }

        }

        /// <summary>
        /// 
        /// </summary>
        public class SomeCreateCommand : AbstractCommand<SomeAggregate>
        {

        }

        /// <summary>
        /// 
        /// </summary>
        public class SomeUpdateCommand : AbstractCommand<SomeAggregate>
        {

            /// <summary>
            /// 
            /// </summary>
            public string Id { get; set; }

        }

        /// <summary>
        /// 
        /// </summary>
        public class SomeCommandHandler : 
            IHandleCommand<SomeCreateCommand>,
            IHandleCommand<SomeUpdateCommand>
        {

            /// <summary>
            /// 
            /// </summary>
            /// <param name="command"></param>
            /// <returns></returns>
            public Task<IEnumerable<AbstractEvent>> HandleAsync(SomeCreateCommand command)
            {
                return AsyncEnumerable.Create<AbstractEvent>(yield =>
                {
                    yield.Return(new SomeCreatedEvent());
                    return Task.CompletedTask;
                });
            }

            /// <summary>
            /// 
            /// </summary>
            /// <param name="command"></param>
            /// <returns></returns>
            public Task<IEnumerable<AbstractEvent>> HandleAsync(SomeUpdateCommand command)
            {
                return AsyncEnumerable.Create<AbstractEvent>(yield =>
                {
                    yield.Return(new SomeUpdatedEvent
                    {
                        Id = command.Id
                    });
                    return Task.CompletedTask;
                });
            }

        }

        /// <summary>
        /// 
        /// </summary>
        public class SomeCreatedEvent : AbstractEvent
        {

        }

        /// <summary>
        /// 
        /// </summary>
        public class SomeUpdatedEvent : AbstractEvent
        {

        }

        /// <summary>
        /// 
        /// </summary>
        public class SomeEventSubscriber :
            ISubscribeTo<SomeCreatedEvent>
        {

            /// <summary>
            /// 
            /// </summary>
            public bool IsSomeEventHandled { get; private set; }

            /// <summary>
            /// 
            /// </summary>
            /// <param name="event"></param>
            /// <returns></returns>
            public Task HandleAsync(SomeCreatedEvent @event)
            {
                IsSomeEventHandled = true;
                return Task.CompletedTask;    
            }

        }

        #endregion

    }

}
