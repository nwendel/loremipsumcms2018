﻿#region License
// Copyright (c) Niklas Wendel 2017-2018
// 
// Licensed under the Apache License, Version 2.0 (the "License"); 
// you may not use this file except in compliance with the License. 
// You may obtain a copy of the License at 
// 
// http://www.apache.org/licenses/LICENSE-2.0 
// 
// Unless required by applicable law or agreed to in writing, software 
// distributed under the License is distributed on an "AS IS" BASIS, 
// WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied. 
// See the License for the specific language governing permissions and 
// limitations under the License.
#endregion
using LoremIpsumCMS.Infrastructure.Cqrs;
using LoremIpsumCMS.Infrastructure.EventStore;
using LoremIpsumCMS.Infrastructure.Model;
using LoremIpsumCMS.Infrastructure.Validation;
using Moq;
using Raven.TestDriver;
using System;
using System.Threading.Tasks;
using Xunit;

namespace LoremIpsumCMS.Tests.Infrastructure.Cqrs
{

    /// <summary>
    /// 
    /// </summary>
    public class RavenCommandSenderValidateAsyncTests : RavenTestDriver
    {

        private readonly RavenCommandSender _tested;

        private readonly Mock<IServiceProvider> _serviceProviderMock = new Mock<IServiceProvider>();
        private readonly Mock<IValidationService> _validationServiceMock = new Mock<IValidationService>();
        private readonly Mock<IEventStore> _eventStoreMock = new Mock<IEventStore>();

        /// <summary>
        /// 
        /// </summary>
        public RavenCommandSenderValidateAsyncTests()
        {
            var asyncDocumentSession = GetDocumentStore(null, Guid.NewGuid().ToString()).OpenAsyncSession();

            _tested = new RavenCommandSender(
                _serviceProviderMock.Object,
                _validationServiceMock.Object,
                _eventStoreMock.Object,
                asyncDocumentSession);
        }

        /// <summary>
        /// 
        /// </summary>
        /// <returns></returns>
        [Fact]
        public async Task ThrowsOnSendAsyncNullCommand()
        {
            await Assert.ThrowsAsync<ArgumentNullException>(() => _tested.SendAsync((AbstractCommand<AbstractAggregate>)null));
        }

        /// <summary>
        /// 
        /// </summary>
        /// <returns></returns>
        [Fact]
        public async Task CanValidateAsyncValidCommand()
        {
            var someCommandHandler = new SomeCommandHandler();

            _serviceProviderMock
                .Setup(x => x.GetService(typeof(IValidateCommand<SomeCommand>)))
                .Returns(someCommandHandler);
            _validationServiceMock
                .Setup(x => x.ValidateAsync(It.IsAny<SomeCommand>()))
                .ReturnsAsync(new ValidationMessage[0]);

            var command = new SomeCommand {IsValid = true};
            await _tested.ValidateAsync(command);

            Assert.True(someCommandHandler.IsValidated);
        }

        /// <summary>
        /// 
        /// </summary>
        /// <returns></returns>
        [Fact]
        public async Task ThrowsOnValidateAsyncInvalidCommand()
        {
            var someCommandHandler = new SomeCommandHandler();

            _serviceProviderMock
                .Setup(x => x.GetService(typeof(IValidateCommand<SomeCommand>)))
                .Returns(someCommandHandler);
            _validationServiceMock
                .Setup(x => x.ValidateAsync(It.IsAny<SomeCommand>()))
                .ReturnsAsync(new ValidationMessage[0]);

            var command = new SomeCommand { IsValid = false };
            await Assert.ThrowsAsync<SomeInvalidCommandException>(() => _tested.ValidateAsync(command));
        }

        /// <summary>
        /// 
        /// </summary>
        /// <returns></returns>
        [Fact]
        public async Task ThrowsOnValidateAsyncInvalidCommandUsingValidator()
        {
            var someCommandHandler = new SomeCommandHandler();

            _serviceProviderMock
                .Setup(x => x.GetService(typeof(IValidateCommand<SomeCommand>)))
                .Returns(someCommandHandler);
            _validationServiceMock
                .Setup(x => x.ValidateAsync(It.IsAny<SomeCommand>()))
                .ReturnsAsync(new[] {ValidationMessage.Create("SomeProperty", "Is invalid")});

            var command = new SomeCommand();
            await Assert.ThrowsAsync<ValidationException>(() => _tested.ValidateAsync(command));
        }

        #region Helpers

        /// <summary>
        /// 
        /// </summary>
        public class SomeAggregate : AbstractAggregate
        {

        }

        /// <summary>
        /// 
        /// </summary>
        public class SomeCommand : AbstractCommand<SomeAggregate>
        {

            /// <summary>
            /// 
            /// </summary>
            public bool IsValid { get; set; }

        }

        /// <summary>
        /// 
        /// </summary>
        public class SomeCommandHandler :
            IValidateCommand<SomeCommand>
        {

            /// <summary>
            /// 
            /// </summary>
            public bool IsValidated { get; set; }

            /// <summary>
            /// 
            /// </summary>
            /// <param name="command"></param>
            /// <returns></returns>
            public Task ValidateAsync(SomeCommand command)
            {
                if (!command.IsValid)
                {
                    throw new SomeInvalidCommandException();
                }

                IsValidated = true;

                return Task.CompletedTask;
            }

        }

        /// <summary>
        /// 
        /// </summary>
        public class SomeInvalidCommandException : Exception
        {

        }

        #endregion

    }

}
