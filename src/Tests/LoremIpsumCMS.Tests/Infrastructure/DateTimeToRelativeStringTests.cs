﻿#region License
// Copyright (c) Niklas Wendel 2017-2018
// 
// Licensed under the Apache License, Version 2.0 (the "License"); 
// you may not use this file except in compliance with the License. 
// You may obtain a copy of the License at 
// 
// http://www.apache.org/licenses/LICENSE-2.0 
// 
// Unless required by applicable law or agreed to in writing, software 
// distributed under the License is distributed on an "AS IS" BASIS, 
// WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied. 
// See the License for the specific language governing permissions and 
// limitations under the License.
#endregion
using LoremIpsumCMS.Infrastructure;
using System;
using Xunit;

namespace LoremIpsumCMS.Tests.Infrastructure
{

    /// <summary>
    /// 
    /// </summary>
    public class DateTimeToRelativeStringTests
    {

        /// <summary>
        /// 
        /// </summary>
        [Fact]
        public void CanToRelativeStringToday()
        {
            var tested = DateTime.Now;
            var result = tested.ToRelativeString();

            Assert.Contains("Today", result);
        }

        /// <summary>
        /// 
        /// </summary>
        [Fact]
        public void CanToRelativeStringYesterday()
        {
            var tested = DateTime.Now.AddDays(-1);
            var result = tested.ToRelativeString();

            Assert.Contains("Yesterday", result);
        }

        /// <summary>
        /// 
        /// </summary>
        [Fact]
        public void CanToRelativeStringLongAgo()
        {
            var tested = new DateTime(2000, 1, 1);
            var result = tested.ToRelativeString();

            Assert.Contains("2000-01-01", result);
        }

        /// <summary>
        /// 
        /// </summary>
        [Fact]
        public void CanToRelativeStringNull()
        {
            var tested = new DateTime?();
            var result = tested.ToRelativeString();

            Assert.Empty(result);
        }

    }

}
