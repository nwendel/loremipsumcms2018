﻿#region License
// Copyright (c) Niklas Wendel 2017-2018
// 
// Licensed under the Apache License, Version 2.0 (the "License"); 
// you may not use this file except in compliance with the License. 
// You may obtain a copy of the License at 
// 
// http://www.apache.org/licenses/LICENSE-2.0 
// 
// Unless required by applicable law or agreed to in writing, software 
// distributed under the License is distributed on an "AS IS" BASIS, 
// WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied. 
// See the License for the specific language governing permissions and 
// limitations under the License.
#endregion
using LoremIpsumCMS.Infrastructure.Cqrs;
using LoremIpsumCMS.Infrastructure.EventStore;
using Moq;
using Raven.Client.Documents.Session;
using System;
using System.Threading;
using System.Threading.Tasks;
using Xunit;

namespace LoremIpsumCMS.Tests.Infrastructure.EventStore
{

    /// <summary>
    /// 
    /// </summary>
    public class EventStoreArgumentTests
    {

        /// <summary>
        /// 
        /// </summary>
        /// <returns></returns>
        [Fact]
        public async Task ThrowsOnNullAggregateType()
        {
            var tested = new RavenEventStore(null);

            var ex = await Assert.ThrowsAsync<ArgumentNullException>(() => tested.SaveEventsAsync(null, new AbstractEvent[0]));
            Assert.Equal("aggregateType", ex.ParamName);
        }

        /// <summary>
        /// 
        /// </summary>
        [Fact]
        public async Task ThrowsOnNullEvents()
        {
            var tested = new RavenEventStore(null);

            var ex = await Assert.ThrowsAsync<ArgumentNullException>(() => tested.SaveEventsAsync(typeof(object), null));
            Assert.Equal("events", ex.ParamName);
        }

        /// <summary>
        /// 
        /// </summary>
        [Fact]
        public async Task ThrowsOnCreateNullEventId()
        {
            var tested = new RavenEventStore(null);
            var events = new[] { new SomeEvent() };

            var ex = await Assert.ThrowsAsync<InvalidOperationException>(() => tested.SaveEventsAsync(typeof(object), events));
            Assert.Equal("Cannot save event without AggregateId", ex.Message);
        }

        /// <summary>
        /// 
        /// </summary>
        [Fact]
        public async Task ThrowsOnCreateInconsistentEventIds()
        {
            var documentSessionMock = new Mock<IAsyncDocumentSession>();
            documentSessionMock
                .Setup(x => x.StoreAsync(It.IsAny<PersistedEvent>(), default(CancellationToken)))
                .Returns(Task.CompletedTask);

            var tested = new RavenEventStore(documentSessionMock.Object);
            var events = new[] { new SomeEvent { Id = "one" }, new SomeEvent { Id = "two" } };

            var ex = await Assert.ThrowsAsync<InvalidOperationException>(() => tested.SaveEventsAsync(typeof(object), events));
            Assert.Equal("Cannot save events with different AggregateIds", ex.Message);
        }

        #region Model

        /// <summary>
        /// 
        /// </summary>
        public class SomeEvent : AbstractEvent
        {
        }

        #endregion

    }

}
