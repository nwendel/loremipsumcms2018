﻿#region License
// Copyright (c) Niklas Wendel 2017-2018
// 
// Licensed under the Apache License, Version 2.0 (the "License"); 
// you may not use this file except in compliance with the License. 
// You may obtain a copy of the License at 
// 
// http://www.apache.org/licenses/LICENSE-2.0 
// 
// Unless required by applicable law or agreed to in writing, software 
// distributed under the License is distributed on an "AS IS" BASIS, 
// WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied. 
// See the License for the specific language governing permissions and 
// limitations under the License.
#endregion
using LoremIpsumCMS.Infrastructure.Cqrs;
using LoremIpsumCMS.Infrastructure.EventStore;
using Moq;
using Raven.Client.Documents.Session;
using System.Threading;
using System.Threading.Tasks;
using Xunit;


namespace LoremIpsumCMS.Tests.Infrastructure.EventStore
{

    /// <summary>
    /// 
    /// </summary>
    public class EventStoreSaveTests
    {

        /// <summary>
        /// 
        /// </summary>
        [Fact]
        public async Task CanSaveTwoEvents()
        {
            var asyncDocumentSessionMock = new Mock<IAsyncDocumentSession>();
            asyncDocumentSessionMock
                .Setup(x => x.StoreAsync(It.IsAny<PersistedEvent>(), default(CancellationToken)))
                .Returns(Task.CompletedTask);

            var tested = new RavenEventStore(asyncDocumentSessionMock.Object);
            var events = new[] { new SomeEvent { Id = "one" }, new SomeEvent { Id = "one" } };
            await tested.SaveEventsAsync(typeof(object), events);

            asyncDocumentSessionMock.Verify(x => x.StoreAsync(It.IsAny<PersistedEvent>(), default(CancellationToken)), Times.Exactly(2));
        }

        #region Model

        /// <summary>
        /// 
        /// </summary>
        public class SomeEvent : AbstractEvent
        {
        }

        #endregion

    }

}
