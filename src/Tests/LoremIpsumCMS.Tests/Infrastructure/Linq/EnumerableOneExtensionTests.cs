﻿#region License
// Copyright (c) Niklas Wendel 2017-2018
// 
// Licensed under the Apache License, Version 2.0 (the "License"); 
// you may not use this file except in compliance with the License. 
// You may obtain a copy of the License at 
// 
// http://www.apache.org/licenses/LICENSE-2.0 
// 
// Unless required by applicable law or agreed to in writing, software 
// distributed under the License is distributed on an "AS IS" BASIS, 
// WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied. 
// See the License for the specific language governing permissions and 
// limitations under the License.
#endregion
using LoremIpsumCMS.Infrastructure.Linq;
using Xunit;

namespace LoremIpsumCMS.Tests.Infrastructure.Linq
{

    /// <summary>
    /// 
    /// </summary>
    public class EnumerableOneExtensionTests
    {

        /// <summary>
        /// 
        /// </summary>
        [Fact]
        public void CanOneTrue()
        {
            var tested = new[] { "test" };

            Assert.True(tested.One());
        }

        /// <summary>
        /// 
        /// </summary>
        [Fact]
        public void CanOneFalseIfEmpty()
        {
            var tested = new string[0];

            Assert.False(tested.One());
        }

        /// <summary>
        /// 
        /// </summary>
        [Fact]
        public void CanOneFalseIfMultiple()
        {
            var tested = new[] { "test", "test" };

            Assert.False(tested.One());
        }

        /// <summary>
        /// 
        /// </summary>
        [Fact]
        public void CanOneWithPredicateTrue()
        {
            var tested = new[] { "test", "something" };

            Assert.True(tested.One(x => x == "test"));
        }

        /// <summary>
        /// 
        /// </summary>
        [Fact]
        public void CanOneWithPredicateFalseIfEmpty()
        {
            var tested = new[] { "something" };

            Assert.False(tested.One(x => x == "test"));
        }

        /// <summary>
        /// 
        /// </summary>
        [Fact]
        public void CanOneWithPredicateFalseIfMultiple()
        {
            var tested = new[] { "test", "test" };

            Assert.False(tested.One(x => x == "test"));
        }

    }

}
