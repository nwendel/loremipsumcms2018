﻿#region License
// Copyright (c) Niklas Wendel 2017-2018
// 
// Licensed under the Apache License, Version 2.0 (the "License"); 
// you may not use this file except in compliance with the License. 
// You may obtain a copy of the License at 
// 
// http://www.apache.org/licenses/LICENSE-2.0 
// 
// Unless required by applicable law or agreed to in writing, software 
// distributed under the License is distributed on an "AS IS" BASIS, 
// WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied. 
// See the License for the specific language governing permissions and 
// limitations under the License.
#endregion
using LoremIpsumCMS.Infrastructure.Cqrs;
using LoremIpsumCMS.Infrastructure.Model;
using System;
using Xunit;

namespace LoremIpsumCMS.Tests.Infrastructure.Model
{

    /// <summary>
    /// 
    /// </summary>
    public class AggregateTests
    {

        /// <summary>
        /// 
        /// </summary>
        [Fact]
        public void ThrowsOnApplyEventInvalidEvent()
        {
            var tested = new TestAggregate();

            Assert.Throws<InvalidOperationException>(() => tested.ApplyEvent(new InvalidEvent()));
        }

        /// <summary>
        /// 
        /// </summary>
        [Fact]
        public void CanApplyValidEvent()
        {
            var tested = new TestAggregate();
            tested.ApplyEvent(new ValidEvent());

            Assert.Equal("Called", tested.Id);
        }

        #region Model

        /// <summary>
        /// 
        /// </summary>
        public class TestAggregate :
            AbstractAggregate,
            IApplyEvent<ValidEvent>
        {

            /// <summary>
            /// 
            /// </summary>
            public string PublicId
            {
                get { return Id; }
                set { Id = value; }
            }

            /// <summary>
            /// 
            /// </summary>
            /// <param name="event"></param>
            public void Apply(ValidEvent @event)
            {
                Id = "Called";
            }

        }

        /// <summary>
        /// 
        /// </summary>
        public class ValidEvent : AbstractEvent
        {
        }

        /// <summary>
        /// 
        /// </summary>
        public class InvalidEvent : AbstractEvent
        {
        }

        #endregion

    }

}
