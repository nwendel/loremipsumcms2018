﻿#region License
// Copyright (c) Niklas Wendel 2017-2018
// 
// Licensed under the Apache License, Version 2.0 (the "License"); 
// you may not use this file except in compliance with the License. 
// You may obtain a copy of the License at 
// 
// http://www.apache.org/licenses/LICENSE-2.0 
// 
// Unless required by applicable law or agreed to in writing, software 
// distributed under the License is distributed on an "AS IS" BASIS, 
// WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied. 
// See the License for the specific language governing permissions and 
// limitations under the License.
#endregion
using LoremIpsumCMS.Infrastructure.Model;
using Xunit;

namespace LoremIpsumCMS.Tests.Infrastructure.Model
{

    /// <summary>
    /// 
    /// </summary>
    public class RootEntityTests
    {

        /// <summary>
        /// 
        /// </summary>
        [Fact]
        public void CanEqualsSameTransientObject()
        {
            var tested = new TestEntity();

            var isEqual = tested.Equals((object)tested);

            Assert.True(isEqual);
        }

        /// <summary>
        /// 
        /// </summary>
        [Fact]
        public void CanEqualsDifferentTransientObject()
        {
            var tested = new TestEntity();
            var other = new TestEntity();

            var isEqual = tested.Equals(other);

            Assert.False(isEqual);
        }

        /// <summary>
        /// 
        /// </summary>
        [Fact]
        public void CanEqualsDifferentObjectSameId()
        {
            var tested = new TestEntity { PublicId = "asdf" };
            var other = new TestEntity { PublicId = "asdf" };

            var isEqual = tested.Equals(other);

            Assert.True(isEqual);
        }

        /// <summary>
        /// 
        /// </summary>
        [Fact]
        public void CanEqualsNull()
        {
            var tested = new TestEntity();

            var isEqual = tested.Equals(null);

            Assert.False(isEqual);
        }

        /// <summary>
        /// 
        /// </summary>
        [Fact]
        public void CanEqualsWrongOtherType()
        {
            var tested = new TestEntity();
            var other = new AnotherTestEntity();

            var isEqual = tested.Equals(other);

            Assert.False(isEqual);
        }

        /// <summary>
        /// 
        /// </summary>
        [Fact]
        public void CanGetTransientHashCode()
        {
            var tested = new TestEntity();

            var hashCode = tested.GetHashCode();

            Assert.NotEqual(0, hashCode);
        }

        /// <summary>
        /// 
        /// </summary>
        [Fact]
        public void CanGetNonTransientHashCode()
        {
            var tested = new TestEntity { PublicId = "asdf" };

            var hashCode = tested.GetHashCode();

            Assert.NotEqual(0, hashCode);
        }

        #region Model

        /// <summary>
        /// 
        /// </summary>
        public class TestEntity : AbstractRootEntity
        {

            /// <summary>
            /// 
            /// </summary>
            public string PublicId
            {
                get => Id;
                set => Id = value;
            }

        }

        /// <summary>
        /// 
        /// </summary>
        public class AnotherTestEntity : AbstractRootEntity
        {
        }

        #endregion

    }

}
