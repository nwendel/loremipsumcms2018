﻿#region License
// Copyright (c) Niklas Wendel 2017-2018
// 
// Licensed under the Apache License, Version 2.0 (the "License"); 
// you may not use this file except in compliance with the License. 
// You may obtain a copy of the License at 
// 
// http://www.apache.org/licenses/LICENSE-2.0 
// 
// Unless required by applicable law or agreed to in writing, software 
// distributed under the License is distributed on an "AS IS" BASIS, 
// WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied. 
// See the License for the specific language governing permissions and 
// limitations under the License.
#endregion
using LoremIpsumCMS.Infrastructure.Proxy;
using System.Reflection;
using Xunit;

namespace LoremIpsumCMS.Tests.Infrastructure.Proxy
{

    /// <summary>
    /// 
    /// </summary>
    public class CreateTests
    {

        /// <summary>
        /// 
        /// </summary>
        [Fact]
        public void CanCreateEmptyInterface()
        {
            var target = new Empty();
            var tested = ProxyBuilder
                .For<IEmpty>()
                .WithTarget(() => target)
                .Build();
            var proxiedTarget = (tested as IProxyTarget<IEmpty>)?.Target;

            Assert.Same(target, proxiedTarget);
        }

        /// <summary>
        /// 
        /// </summary>
        [Fact]
        public void CanCreateProcedureNoArgumentsInterface()
        {
            var target = new ProcedureNoArguments();
            var tested = ProxyBuilder
                .For<IProcedureNoArguments>()
                .WithTarget(() => target)
                .Build();
            tested.Operation();
            var proxiedTarget = (tested as IProxyTarget<IProcedureNoArguments>)?.Target;

            Assert.Same(target, proxiedTarget);
            Assert.True(target.Invoked);
        }

        /// <summary>
        /// 
        /// </summary>
        [Fact]
        public void CanCreateFunctionNoArgumentsInterface()
        {
            var target = new FunctionNoArguments();
            var tested = ProxyBuilder
                .For<IFunctionNoArguments>()
                .WithTarget(() => target)
                .Build();
            var result = tested.Operation();
            var proxiedTarget = (tested as IProxyTarget<IFunctionNoArguments>)?.Target;

            Assert.Same(target, proxiedTarget);
            Assert.True(target.Invoked);
            Assert.Equal(42, result);
        }

        /// <summary>
        /// 
        /// </summary>
        [Fact]
        public void CanCreateProcedureWithArgumentsInterface()
        {
            var target = new ProcedureWithArguments();
            var tested = ProxyBuilder
                .For<IProcedureWithArguments>()
                .WithTarget(() => target)
                .Build();
            tested.Operation(new object(), 42, true, "adsf", null);
            var proxiedTarget = (tested as IProxyTarget<IProcedureWithArguments>)?.Target;

            Assert.Same(target, proxiedTarget);
            Assert.True(target.Invoked);
        }

        /// <summary>
        /// 
        /// </summary>
        [Fact]
        public void CanCreateGenericFunctionWithArgumentsInterface()
        {
            var target = new GenericFunctionWithArguments();
            var tested = ProxyBuilder
                .For<IGenericFunctionWithArguments>()
                .WithTarget(() => target)
                .Build();
            var result = tested.Operation(target);
            var proxiedTarget = (tested as IProxyTarget<IGenericFunctionWithArguments>)?.Target;

            Assert.Same(target, proxiedTarget);
            Assert.True(target.Invoked);
            Assert.Equal(target, result);
        }

        /// <summary>
        /// 
        /// </summary>
        [Fact]
        public void CanCreateInheritedInterfaces()
        {
            var tested = ProxyBuilder
                .For<IChildInterface>()
                .WithTarget(() => null)
                .Build();
            var testedType = tested.GetType();

            Assert.Equal(4, testedType.GetTypeInfo().GetInterfaces().Length);
            Assert.Contains(typeof(IProxyTarget<IChildInterface>), testedType.GetTypeInfo().GetInterfaces());
            Assert.Contains(typeof(IChildInterface), testedType.GetTypeInfo().GetInterfaces());
            Assert.Contains(typeof(IParentInterface), testedType.GetTypeInfo().GetInterfaces());
            Assert.Contains(typeof(IGrandParentInterface), testedType.GetTypeInfo().GetInterfaces());
        }

        #region Proxy Targets

        /// <summary>
        /// 
        /// </summary>
        public interface IEmpty
        {
        }

        /// <summary>
        /// 
        /// </summary>
        public class Empty : IEmpty
        {
        }

        /// <summary>
        /// 
        /// </summary>
        public interface IProcedureNoArguments
        {

            /// <summary>
            /// 
            /// </summary>
            void Operation();

        }

        /// <summary>
        /// 
        /// </summary>
        public interface IFunctionNoArguments
        {

            /// <summary>
            /// 
            /// </summary>
            int Operation();

        }

        /// <summary>
        /// 
        /// </summary>
        public class FunctionNoArguments : IFunctionNoArguments
        {

            /// <summary>
            /// 
            /// </summary>
            public bool Invoked { get; set; }

            /// <summary>
            /// 
            /// </summary>
            public int Operation()
            {
                Invoked = true;
                return 42;
            }

        }

        /// <summary>
        /// 
        /// </summary>
        public class ProcedureNoArguments : IProcedureNoArguments
        {

            /// <summary>
            /// 
            /// </summary>
            public bool Invoked { get; set; }

            /// <summary>
            /// 
            /// </summary>
            public void Operation()
            {
                Invoked = true;
            }

        }

        /// <summary>
        /// 
        /// </summary>
        public interface IProcedureWithArguments
        {

            /// <summary>
            /// 
            /// </summary>
            /// <param name="a"></param>
            /// <param name="b"></param>
            /// <param name="c"></param>
            /// <param name="d"></param>
            /// <param name="e"></param>
            void Operation(object a, int b, bool c, string d, IProcedureWithArguments e);

        }

        /// <summary>
        /// 
        /// </summary>
        public class ProcedureWithArguments : IProcedureWithArguments
        {

            /// <summary>
            /// 
            /// </summary>
            public bool Invoked { get; set; }

            /// <summary>
            /// 
            /// </summary>
            /// <param name="a"></param>
            /// <param name="b"></param>
            /// <param name="c"></param>
            /// <param name="d"></param>
            /// <param name="e"></param>
            public void Operation(object a, int b, bool c, string d, IProcedureWithArguments e)
            {
                Invoked = true;
            }

        }

        /// <summary>
        /// 
        /// </summary>
        public interface IGenericFunctionWithArguments
        {

            /// <summary>
            /// 
            /// </summary>
            /// <typeparam name="T"></typeparam>
            /// <param name="instance"></param>
            /// <returns></returns>
            T Operation<T>(T instance)
                where T : Empty, IGenericFunctionWithArguments, new();

        }

        /// <summary>
        /// 
        /// </summary>
        public class GenericFunctionWithArguments : Empty, IGenericFunctionWithArguments
        {

            /// <summary>
            /// 
            /// </summary>
            public bool Invoked { get; set; }

            /// <summary>
            /// 
            /// </summary>
            /// <typeparam name="T"></typeparam>
            /// <param name="instance"></param>
            /// <returns></returns>
            public T Operation<T>(T instance)
                where T : Empty, IGenericFunctionWithArguments, new()
            {
                Invoked = true;
                return instance;
            }

        }

        /// <summary>
        /// 
        /// </summary>
        public interface IChildInterface : IParentInterface
        {

            /// <summary>
            /// 
            /// </summary>
            void ChildOperation();

        }

        /// <summary>
        /// 
        /// </summary>
        public interface IParentInterface : IGrandParentInterface
        {

            /// <summary>
            /// 
            /// </summary>
            void ParentOperation();

        }

        /// <summary>
        /// 
        /// </summary>
        public interface IGrandParentInterface
        {

            /// <summary>
            /// 
            /// </summary>
            void GrandParentInterface();

        }

        #endregion

    }

}
