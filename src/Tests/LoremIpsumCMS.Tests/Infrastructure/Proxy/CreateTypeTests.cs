﻿#region License
// Copyright (c) Niklas Wendel 2017-2018
// 
// Licensed under the Apache License, Version 2.0 (the "License"); 
// you may not use this file except in compliance with the License. 
// You may obtain a copy of the License at 
// 
// http://www.apache.org/licenses/LICENSE-2.0 
// 
// Unless required by applicable law or agreed to in writing, software 
// distributed under the License is distributed on an "AS IS" BASIS, 
// WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied. 
// See the License for the specific language governing permissions and 
// limitations under the License.
#endregion
using LoremIpsumCMS.Infrastructure.Proxy;
using Xunit;

namespace LoremIpsumCMS.Tests.Infrastructure.Proxy
{

    /// <summary>
    /// 
    /// </summary>
    public class CreateTypeTests
    {

        /// <summary>
        /// 
        /// </summary>
        [Fact]
        public void CanCreateTypeTwice()
        {
            var first = ProxyBuilder
                .For<IEmpty>()
                .WithTarget(() => null)
                .Build();
            var second = ProxyBuilder
                .For<IEmpty>()
                .WithTarget(() => null)
                .Build();

            Assert.Same(first.GetType(), second.GetType());
        }

        #region Proxy Targets

        /// <summary>
        /// 
        /// </summary>
        public interface IEmpty
        {
        }

        #endregion

    }

}
