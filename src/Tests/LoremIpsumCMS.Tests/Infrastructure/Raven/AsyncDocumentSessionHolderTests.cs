﻿#region License
// Copyright (c) Niklas Wendel 2017-2018
// 
// Licensed under the Apache License, Version 2.0 (the "License"); 
// you may not use this file except in compliance with the License. 
// You may obtain a copy of the License at 
// 
// http://www.apache.org/licenses/LICENSE-2.0 
// 
// Unless required by applicable law or agreed to in writing, software 
// distributed under the License is distributed on an "AS IS" BASIS, 
// WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied. 
// See the License for the specific language governing permissions and 
// limitations under the License.
#endregion
using LoremIpsumCMS.Infrastructure.Raven;
using LoremIpsumCMS.TestHelper;
using Microsoft.Extensions.Options;
using System;
using System.Linq;
using Xunit;

namespace LoremIpsumCMS.Tests.Infrastructure.Raven
{

    /// <summary>
    /// 
    /// </summary>
    public class AsyncDocumentSessionHolderTests : AbstractRavenTests
    {

        private readonly IOptions<LoremIpsumOptions> _options;

        /// <summary>
        /// 
        /// </summary>
        public AsyncDocumentSessionHolderTests()
        {
            _options = Options.Create(new LoremIpsumOptions
            {
                RavenUrl = DocumentStore.Urls.First(),
                RavenDatabase = Guid.NewGuid().ToString()
            });
        }

        /// <summary>
        /// 
        /// </summary>
        [Fact]
        public void CanGetNullSession()
        {
            var tested = new AsyncDocumentSessionHolder(DocumentStore, _options);

            Assert.Null(tested.AsyncDocumentSession);
        }

        /// <summary>
        /// 
        /// </summary>
        [Fact]
        public void CanOpenSession()
        {
            var tested = new AsyncDocumentSessionHolder(DocumentStore, _options);

            tested.OpenAsyncDocumentSession();

            Assert.NotNull(tested.AsyncDocumentSession);
        }

        /// <summary>
        /// 
        /// </summary>
        [Fact]
        public void CanCloseSession()
        {
            var tested = new AsyncDocumentSessionHolder(DocumentStore, _options);

            tested.OpenAsyncDocumentSession();
            tested.CloseAsyncDocumentSession();

            Assert.Null(tested.AsyncDocumentSession);
        }

        /// <summary>
        /// 
        /// </summary>
        [Fact]
        public void ThrowsOnOpenDuplicateSession()
        {
            var tested = new AsyncDocumentSessionHolder(DocumentStore, _options);

            tested.OpenAsyncDocumentSession();

            Assert.Throws<InvalidOperationException>(() => tested.OpenAsyncDocumentSession());
        }

    }

}
