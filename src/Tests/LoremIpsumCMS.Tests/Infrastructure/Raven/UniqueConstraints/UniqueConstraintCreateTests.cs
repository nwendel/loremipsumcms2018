﻿#region License
// Copyright (c) Niklas Wendel 2017-2018
// 
// Licensed under the Apache License, Version 2.0 (the "License"); 
// you may not use this file except in compliance with the License. 
// You may obtain a copy of the License at 
// 
// http://www.apache.org/licenses/LICENSE-2.0 
// 
// Unless required by applicable law or agreed to in writing, software 
// distributed under the License is distributed on an "AS IS" BASIS, 
// WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied. 
// See the License for the specific language governing permissions and 
// limitations under the License.
#endregion
using LoremIpsumCMS.Infrastructure.Model;
using LoremIpsumCMS.Infrastructure.Raven.UniqueConstraints;
using LoremIpsumCMS.Infrastructure.Raven.UniqueConstraints.Internal;
using LoremIpsumCMS.TestHelper;
using Raven.Client.Documents;
using Raven.Client.Documents.Operations.CompareExchange;
using System;
using System.Threading.Tasks;
using Xunit;

namespace LoremIpsumCMS.Tests.Infrastructure.Raven.UniqueConstraints
{

    /// <summary>
    /// 
    /// </summary>
    public class UniqueConstraintCreateTests : AbstractRavenTests
    {

        private readonly IDocumentStore _documentStore;

        /// <summary>
        /// 
        /// </summary>
        public UniqueConstraintCreateTests()
        {
            _documentStore = GetDocumentStore(null, Guid.NewGuid().ToString());
        }

        /// <summary>
        /// 
        /// </summary>
        [Fact]
        public async Task CanCreateNone()
        {
            var create = new None { Name = "some name" };
            using (var asyncDocumentSession = _documentStore.OpenAsyncSession())
            {
                await asyncDocumentSession.StoreAsync(create);
                await asyncDocumentSession.SaveChangesExAsync();
            }

            Assert.NotNull(create.Id);
        }

        /// <summary>
        /// 
        /// </summary>
        [Fact]
        public async Task CanCreateOne()
        {
            var create = new One {Name = "some name"};
            using (var asyncDocumentSession = _documentStore.OpenAsyncSession())
            {
                await asyncDocumentSession.StoreAsync(create);
                await asyncDocumentSession.SaveChangesExAsync();
            }

            var key = UniqueConstraintHelper.CreateKey<One>(_documentStore, x => x.Name, create.Name);
            var result = _documentStore.Operations.Send(new GetCompareExchangeValueOperation<string>(key));

            Assert.Equal(create.Id, result.Value);
        }

        /// <summary>
        /// 
        /// </summary>
        [Fact]
        public async Task CanCreateTwo()
        {
            var create = new Two {FirstName = "some", LastName = "name"};
            using (var asyncDocumentSession = _documentStore.OpenAsyncSession())
            {
                await asyncDocumentSession.StoreAsync(create);
                await asyncDocumentSession.SaveChangesExAsync();
            }

            var key1 = UniqueConstraintHelper.CreateKey<Two>(_documentStore, x => x.FirstName, create.FirstName);
            var result1 = _documentStore.Operations.Send(new GetCompareExchangeValueOperation<string>(key1));
            var key2 = UniqueConstraintHelper.CreateKey<Two>(_documentStore, x => x.LastName, create.LastName);
            var result2 = _documentStore.Operations.Send(new GetCompareExchangeValueOperation<string>(key2));

            Assert.Equal(create.Id, result1.Value);
            Assert.Equal(create.Id, result2.Value);
        }

        /// <summary>
        /// 
        /// </summary>
        [Fact]
        public async Task ThrowsOnCreateOneDuplicate()
        {
            var create = new One { Name = "some name" };
            using (var asyncDocumentSession = _documentStore.OpenAsyncSession())
            {
                await asyncDocumentSession.StoreAsync(create);
                await asyncDocumentSession.SaveChangesExAsync();
            }

            var duplicate = new One { Name = "some name" };
            using (var asyncDocumentSession = _documentStore.OpenAsyncSession())
            {
                await asyncDocumentSession.StoreAsync(duplicate);
                await Assert.ThrowsAsync<UniqueConstraintException>(() => asyncDocumentSession.SaveChangesExAsync());
            }

            var key = UniqueConstraintHelper.CreateKey<One>(_documentStore, x => x.Name, create.Name);
            var result = _documentStore.Operations.Send(new GetCompareExchangeValueOperation<string>(key));

            Assert.Equal(create.Id, result.Value);
        }

        /// <summary>
        /// 
        /// </summary>
        [Fact]
        public async Task ThrowsOnCreateOneDuplicateSameSession()
        {
            var create = new One { Name = "some name" };
            var duplicate = new One { Name = "some name" };
            using (var asyncDocumentSession = _documentStore.OpenAsyncSession())
            {
                await asyncDocumentSession.StoreAsync(create);
                await asyncDocumentSession.StoreAsync(duplicate);
                await Assert.ThrowsAsync<UniqueConstraintException>(() => asyncDocumentSession.SaveChangesExAsync());
            }

            using (var asyncDocumentSession = _documentStore.OpenAsyncSession())
            {
                var loaded = await asyncDocumentSession.LoadAsync<One>(create.Id);
                Assert.Null(loaded);
            }

            var key = UniqueConstraintHelper.CreateKey<One>(_documentStore, x => x.Name, create.Name);
            var result = _documentStore.Operations.Send(new GetCompareExchangeValueOperation<string>(key));
            Assert.Null(result);
        }

        /// <summary>
        /// 
        /// </summary>
        [Fact]
        public async Task CanCreateOneNullPropertyValue()
        {
            var create = new One();
            using (var asyncDocumentSession = _documentStore.OpenAsyncSession())
            {
                await asyncDocumentSession.StoreAsync(create);
                await asyncDocumentSession.SaveChangesExAsync();
            }

            var key = UniqueConstraintHelper.CreateKey<One>(_documentStore, x => x.Name, create.Name);
            var result = _documentStore.Operations.Send(new GetCompareExchangeValueOperation<string>(key));

            Assert.Null(result);
        }

        /// <summary>
        /// 
        /// </summary>
        [Fact]
        public async Task CanCreateOneDuplicateDeleted()
        {
            var create1 = new One { Name = "some name" };
            using (var asyncDocumentSession = _documentStore.OpenAsyncSession())
            {
                await asyncDocumentSession.StoreAsync(create1);
                await asyncDocumentSession.SaveChangesExAsync();
            }

            var create2 = new One { Name = "some name" };
            using (var asyncDocumentSession = _documentStore.OpenAsyncSession())
            {
                var loaded = await asyncDocumentSession.LoadAsync<One>(create1.Id);
                asyncDocumentSession.Delete(loaded);

                await asyncDocumentSession.StoreAsync(create2);
                await asyncDocumentSession.SaveChangesExAsync();
            }

            var key = UniqueConstraintHelper.CreateKey<One>(_documentStore, x => x.Name, create2.Name);
            var result = _documentStore.Operations.Send(new GetCompareExchangeValueOperation<string>(key));

            Assert.Equal(create2.Id, result.Value);
        }


        #region Model

        /// <summary>
        /// 
        /// </summary>
        public class None : AbstractAggregate
        {

            /// <summary>
            /// 
            /// </summary>
            public string Name { get; set; }

        }

        /// <summary>
        /// 
        /// </summary>
        public class One : AbstractAggregate
        {

            /// <summary>
            /// 
            /// </summary>
            [UniqueConstraint]
            public string Name { get; set; }

            /// <summary>
            /// 
            /// </summary>
            public string MoreInformation { get; private set; }

        }

        /// <summary>
        /// 
        /// </summary>
        public class Two : AbstractAggregate
        {

            /// <summary>
            /// 
            /// </summary>
            [UniqueConstraint]
            public string FirstName { get; set; }

            /// <summary>
            /// 
            /// </summary>
            [UniqueConstraint]
            public string LastName { get; set; }

        }

        #endregion

    }

}
