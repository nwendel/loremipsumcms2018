﻿#region License
// Copyright (c) Niklas Wendel 2017-2018
// 
// Licensed under the Apache License, Version 2.0 (the "License"); 
// you may not use this file except in compliance with the License. 
// You may obtain a copy of the License at 
// 
// http://www.apache.org/licenses/LICENSE-2.0 
// 
// Unless required by applicable law or agreed to in writing, software 
// distributed under the License is distributed on an "AS IS" BASIS, 
// WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied. 
// See the License for the specific language governing permissions and 
// limitations under the License.
#endregion
using LoremIpsumCMS.Infrastructure.Model;
using LoremIpsumCMS.Infrastructure.Raven.UniqueConstraints;
using LoremIpsumCMS.Infrastructure.Raven.UniqueConstraints.Internal;
using LoremIpsumCMS.TestHelper;
using Raven.Client.Documents;
using Raven.Client.Documents.Operations.CompareExchange;
using System;
using System.Threading.Tasks;
using Xunit;

namespace LoremIpsumCMS.Tests.Infrastructure.Raven.UniqueConstraints
{

    /// <summary>
    /// 
    /// </summary>
    public class UniqueConstraintDeleteTests : AbstractRavenTests
    {

        private readonly IDocumentStore _documentStore;

        /// <summary>
        /// 
        /// </summary>
        public UniqueConstraintDeleteTests()
        {
            _documentStore = GetDocumentStore(null, Guid.NewGuid().ToString());
        }

        /// <summary>
        /// 
        /// </summary>
        [Fact]
        public async Task CanDeleteNone()
        {
            var create = new None { Name = "some name" };
            using (var asyncDocumentSession = _documentStore.OpenAsyncSession())
            {
                await asyncDocumentSession.StoreAsync(create);
                await asyncDocumentSession.SaveChangesExAsync();
            }

            using (var asyncDocumentSession = _documentStore.OpenAsyncSession())
            {
                var delete = await asyncDocumentSession.LoadAsync<None>(create.Id);
                asyncDocumentSession.Delete(delete);
                await asyncDocumentSession.SaveChangesExAsync();
            }

            using (var asyncDocumentSession = _documentStore.OpenAsyncSession())
            {
                var loaded = await asyncDocumentSession.LoadAsync<None>(create.Id);
                Assert.Null(loaded);
            }
        }

        /// <summary>
        /// 
        /// </summary>
        [Fact]
        public async Task CanDeleteOne()
        {
            var create = new One { Name = "some name" };
            using (var asyncDocumentSession = _documentStore.OpenAsyncSession())
            {
                await asyncDocumentSession.StoreAsync(create);
                await asyncDocumentSession.SaveChangesExAsync();
            }

            using (var asyncDocumentSession = _documentStore.OpenAsyncSession())
            {
                var delete = await asyncDocumentSession.LoadAsync<One>(create.Id);
                asyncDocumentSession.Delete(delete);
                await asyncDocumentSession.SaveChangesExAsync();
            }

            using (var asyncDocumentSession = _documentStore.OpenAsyncSession())
            {
                var loaded = await asyncDocumentSession.LoadAsync<One>(create.Id);
                Assert.Null(loaded);
            }

            var key = UniqueConstraintHelper.CreateKey<One>(_documentStore, x => x.Name, create.Name);
            var result = _documentStore.Operations.Send(new GetCompareExchangeValueOperation<string>(key));

            Assert.Null(result);
        }

        #region Model

        /// <summary>
        /// 
        /// </summary>
        public class None : AbstractAggregate
        {

            public string Name { get; set; }

        }

        /// <summary>
        /// 
        /// </summary>
        public class One : AbstractAggregate
        {

            /// <summary>
            /// 
            /// </summary>
            [UniqueConstraint]
            public string Name { get; set; }

        }

        /// <summary>
        /// 
        /// </summary>
        public class Two : AbstractAggregate
        {

            /// <summary>
            /// 
            /// </summary>
            [UniqueConstraint]
            public string FirstName { get; set; }

            /// <summary>
            /// 
            /// </summary>
            [UniqueConstraint]
            public string LastName { get; set; }

        }

        #endregion

    }

}
