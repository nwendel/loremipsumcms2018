﻿#region License
// Copyright (c) Niklas Wendel 2017-2018
// 
// Licensed under the Apache License, Version 2.0 (the "License"); 
// you may not use this file except in compliance with the License. 
// You may obtain a copy of the License at 
// 
// http://www.apache.org/licenses/LICENSE-2.0 
// 
// Unless required by applicable law or agreed to in writing, software 
// distributed under the License is distributed on an "AS IS" BASIS, 
// WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied. 
// See the License for the specific language governing permissions and 
// limitations under the License.
#endregion
using LoremIpsumCMS.Infrastructure.Model;
using LoremIpsumCMS.Infrastructure.Raven.UniqueConstraints;
using LoremIpsumCMS.TestHelper;
using Raven.Client.Documents;
using System;
using System.Threading.Tasks;
using Xunit;

namespace LoremIpsumCMS.Tests.Infrastructure.Raven.UniqueConstraints
{

    /// <summary>
    /// 
    /// </summary>
    public class UniqueConstraintLoadTests : AbstractRavenTests
    {

        private readonly IDocumentStore _documentStore;

        /// <summary>
        /// 
        /// </summary>
        public UniqueConstraintLoadTests()
        {
            _documentStore = GetDocumentStore(null, Guid.NewGuid().ToString());
        }

        /// <summary>
        /// 
        /// </summary>
        [Fact]
        public async Task CanLoadByUniqueConstraint()
        {
            var createEntity = new One {Name = "some-name"};
            using (var asyncDocumentSession = _documentStore.OpenAsyncSession())
            {
                await asyncDocumentSession.StoreAsync(createEntity);
                await asyncDocumentSession.SaveChangesExAsync();
            }

            using (var asyncDocumentSession = _documentStore.OpenAsyncSession())
            {
                var loadEntity = await asyncDocumentSession.LoadByUniqueConstraintAsync<One>(x => x.Name, "some-name");

                Assert.NotNull(loadEntity);
                Assert.Equal(createEntity.Id, loadEntity.Id);
            }
        }

        /// <summary>
        /// 
        /// </summary>
        [Fact]
        public async Task CanLoadByUniqueConstraintSingleNotFound()
        {
            using (var asyncDocumentSession = _documentStore.OpenAsyncSession())
            {
                var loadEntity = await asyncDocumentSession.LoadByUniqueConstraintAsync<One>(x => x.Name, "some-name");

                Assert.Null(loadEntity);
            }
        }

        /// <summary>
        /// 
        /// </summary>
        /// <returns></returns>
        [Fact]
        public async Task ThrowsOnLoadByUniqueConstraintNoAttribute()
        {
            using (var asyncDocumentSession = _documentStore.OpenAsyncSession())
            {
                await Assert.ThrowsAsync<InvalidOperationException>(() =>
                    asyncDocumentSession.LoadByUniqueConstraintAsync<None>(x => x.Name, "asdf"));
            }
        }

        /// <summary>
        /// 
        /// </summary>
        [Fact]
        public async Task CanLoadByUniqueConstraintMultiple()
        {
            var createEntity1 = new One { Name = "some-name" };
            var createEntity2 = new One { Name = "some-other-name" };
            using (var asyncDocumentSession = _documentStore.OpenAsyncSession())
            {
                await asyncDocumentSession.StoreAsync(createEntity1);
                await asyncDocumentSession.StoreAsync(createEntity2);
                await asyncDocumentSession.SaveChangesExAsync();
            }

            using (var asyncDocumentSession = _documentStore.OpenAsyncSession())
            {
                var loadEntities = await asyncDocumentSession.LoadByUniqueConstraintAsync<One>(x => x.Name, new[] {"some-name", "some-other-name"});

                Assert.NotNull(loadEntities);
                Assert.Equal(2, loadEntities.Count);
            }
        }

        /// <summary>
        /// 
        /// </summary>
        [Fact]
        public async Task CanLoadByUniqueConstraintMultipleOneElementNotFound()
        {
            var createEntity1 = new One { Name = "some-name" };
            var createEntity2 = new One { Name = "some-other-name" };
            using (var asyncDocumentSession = _documentStore.OpenAsyncSession())
            {
                await asyncDocumentSession.StoreAsync(createEntity1);
                await asyncDocumentSession.StoreAsync(createEntity2);
                await asyncDocumentSession.SaveChangesExAsync();
            }

            using (var asyncDocumentSession = _documentStore.OpenAsyncSession())
            {
                var loadEntities = await asyncDocumentSession.LoadByUniqueConstraintAsync<One>(x => x.Name, new[] { "not-found" });

                Assert.NotNull(loadEntities);
                Assert.Single(loadEntities);
                Assert.Contains(loadEntities, k => k.Key == "not-found");
            }
        }

        #region Model

        /// <summary>
        /// 
        /// </summary>
        public class None : AbstractAggregate
        {

            /// <summary>
            /// 
            /// </summary>
            public string Name { get; set; }

        }

        /// <summary>
        /// 
        /// </summary>
        public class One : AbstractAggregate
        {

            /// <summary>
            /// 
            /// </summary>
            [UniqueConstraint]
            public string Name { get; set; }

        }

        #endregion

    }

}


