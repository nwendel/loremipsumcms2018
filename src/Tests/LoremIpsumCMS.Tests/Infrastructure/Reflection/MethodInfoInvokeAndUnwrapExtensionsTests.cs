﻿#region License
// Copyright (c) Niklas Wendel 2017-2018
// 
// Licensed under the Apache License, Version 2.0 (the "License"); 
// you may not use this file except in compliance with the License. 
// You may obtain a copy of the License at 
// 
// http://www.apache.org/licenses/LICENSE-2.0 
// 
// Unless required by applicable law or agreed to in writing, software 
// distributed under the License is distributed on an "AS IS" BASIS, 
// WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied. 
// See the License for the specific language governing permissions and 
// limitations under the License.
#endregion
using LoremIpsumCMS.Infrastructure.Reflection;
using System;
using System.Reflection;
using System.Threading.Tasks;
using Xunit;

namespace LoremIpsumCMS.Tests.Infrastructure.Reflection
{

    /// <summary>
    /// 
    /// </summary>
    public class MethodInfoInvokeAndUnwrapExtensionTests
    {

        /// <summary>
        /// 
        /// </summary>
        [Fact]
        public void CanUnwrapException()
        {
            var tested = new TestObject();
            var methodInfo = typeof(TestObject).GetMethod(nameof(TestObject.ThrowException));

            Assert.Throws<ArgumentException>(() => methodInfo.InvokeAndUnwrapException(tested, null));
        }

        /// <summary>
        /// 
        /// </summary>
        [Fact]
        public void CanGetWrappedException()
        {
            var tested = new TestObject();
            var methodInfo = typeof(TestObject).GetMethod(nameof(TestObject.ThrowException));

            Assert.Throws<TargetInvocationException>(() => methodInfo.Invoke(tested, null));
        }

        /// <summary>
        /// 
        /// </summary>
        [Fact]
        public async Task CanInvokeAsync()
        {
            var tested = new TestObject();
            var methodInfo = typeof(TestObject).GetMethod(nameof(TestObject.ThrowExceptionAsync));

            await Assert.ThrowsAsync<ArgumentException>(async () => await methodInfo.InvokeAsync(tested, null));
        }

        /// <summary>
        /// 
        /// </summary>
        [Fact]
        public async Task CanInvokeAsyncReturnValue()
        {
            var tested = new TestObject();
            var methodInfo = typeof(TestObject).GetMethod(nameof(TestObject.ThrowExceptionAsync));

            await Assert.ThrowsAsync<ArgumentException>(async () => await methodInfo.InvokeAsync<string>(tested, null));
        }

        #region Model

        /// <summary>
        /// 
        /// </summary>
        public class TestObject
        {

            /// <summary>
            /// 
            /// </summary>
            public void ThrowException()
            {
                throw new ArgumentException();
            }

            /// <summary>
            /// 
            /// </summary>
            /// <returns></returns>
            public async Task<string> ThrowExceptionAsync()
            {
                await Task.Run(() => throw new ArgumentException());
                return null;
            }

        }

        #endregion

    }

}
