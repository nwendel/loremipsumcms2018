﻿#region License
// Copyright (c) Niklas Wendel 2017-2018
// 
// Licensed under the Apache License, Version 2.0 (the "License"); 
// you may not use this file except in compliance with the License. 
// You may obtain a copy of the License at 
// 
// http://www.apache.org/licenses/LICENSE-2.0 
// 
// Unless required by applicable law or agreed to in writing, software 
// distributed under the License is distributed on an "AS IS" BASIS, 
// WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied. 
// See the License for the specific language governing permissions and 
// limitations under the License.
#endregion
using LoremIpsumCMS.Infrastructure.Linq;
using LoremIpsumCMS.Infrastructure.Validation;
using Microsoft.Extensions.DependencyInjection;
using System.Threading.Tasks;
using Xunit;

namespace LoremIpsumCMS.Tests.Infrastructure.Validation
{

    /// <summary>
    /// 
    /// </summary>
    public class InstanceIncludeValidationTests
    {

        /// <summary>
        /// 
        /// </summary>
        [Fact]
        public async Task CanValidateNotDefault()
        {
            var customer = new Customer
            {
                Age = 47
            };

            var tested = new CustomerValidator();
            var serviceProvider = new ServiceCollection()
                .AddSingleton<AgeAwareValidator, AgeAwareValidator>()
                .BuildServiceProvider();
            var context = new ValidationContext<Customer>(customer, serviceProvider);
            var messages = await tested.ValidateAsync(context).ToListAsync();

            Assert.Empty(messages);
        }

        /// <summary>
        /// 
        /// </summary>
        [Fact]
        public async Task CanValidateNotDefaultInvalid()
        {
            var customer = new Customer();

            var tested = new CustomerValidator();
            var serviceProvider = new ServiceCollection()
                .AddSingleton<AgeAwareValidator, AgeAwareValidator>()
                .BuildServiceProvider();
            var context = new ValidationContext<Customer>(customer, serviceProvider);
            var messages = await tested.ValidateAsync(context).ToListAsync();

            Assert.Single(messages);
            Assert.All(messages, x =>
            {
                Assert.Equal("Age", x.PropertyName);
                Assert.Equal("Age must be set", x.Text);
            });
        }

        #region Model / Validators

        /// <summary>
        /// 
        /// </summary>
        public interface IAgeAware
        {

            /// <summary>
            /// 
            /// </summary>
            int Age { get; }

        }

        /// <summary>
        /// 
        /// </summary>
        public class Customer : IAgeAware
        {

            /// <summary>
            /// 
            /// </summary>
            public int Age { get; set; }

        }

        /// <summary>
        /// 
        /// </summary>
        public class CustomerValidator : AbstractValidator<Customer>
        {

            /// <summary>
            /// 
            /// </summary>
            public CustomerValidator()
            {
                Include<AgeAwareValidator>();
            }

        }

        /// <summary>
        /// 
        /// </summary>
        public class AgeAwareValidator : AbstractValidator<IAgeAware>
        {

            /// <summary>
            /// 
            /// </summary>
            public AgeAwareValidator()
            {
                RuleFor(x => x.Age).NotDefault();
            }

        }

        #endregion

    }

}
