﻿#region License
// Copyright (c) Niklas Wendel 2017-2018
// 
// Licensed under the Apache License, Version 2.0 (the "License"); 
// you may not use this file except in compliance with the License. 
// You may obtain a copy of the License at 
// 
// http://www.apache.org/licenses/LICENSE-2.0 
// 
// Unless required by applicable law or agreed to in writing, software 
// distributed under the License is distributed on an "AS IS" BASIS, 
// WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied. 
// See the License for the specific language governing permissions and 
// limitations under the License.
#endregion
using LoremIpsumCMS.Infrastructure.Validation.Internal;
using System;
using Xunit;

namespace LoremIpsumCMS.Tests.Infrastructure.Validation.Internal
{

    /// <summary>
    /// 
    /// </summary>
    public class PropertyChainTests
    {

        /// <summary>
        /// 
        /// </summary>
        [Fact]
        public void CanCreate()
        {
            var tested = new PropertyChain();

            Assert.Equal(string.Empty, tested.Name);
            Assert.Equal(string.Empty, tested.FullName);
            Assert.Equal(string.Empty, tested.ToString());
        }

        /// <summary>
        /// 
        /// </summary>
        [Fact]
        public void ThrowsOnCreateChildPropertyNullParent()
        {
            Assert.Throws<ArgumentNullException>("parent", () => new PropertyChain(null, "Asdf"));
        }

        /// <summary>
        /// 
        /// </summary>
        [Fact]
        public void ThrowsOnCreateChildPropertyNullPropertyName()
        {
            Assert.Throws<ArgumentNullException>("propertyName", () => new PropertyChain(new PropertyChain(), null));
        }

        /// <summary>
        /// 
        /// </summary>
        [Fact]
        public void CanCreateChildProperty()
        {
            var tested = new PropertyChain(new PropertyChain(), "Asdf");

            Assert.Equal("Asdf", tested.Name);
            Assert.Equal("Asdf", tested.FullName);
            Assert.Equal("Asdf", tested.ToString());
        }

        /// <summary>
        /// 
        /// </summary>
        [Fact]
        public void CanCreateChildProperties()
        {
            var tested = new PropertyChain(new PropertyChain(new PropertyChain(), "Asdf"), "Qwerty");

            Assert.Equal("Qwerty", tested.Name);
            Assert.Equal("Asdf.Qwerty", tested.FullName);
            Assert.Equal("Asdf.Qwerty", tested.ToString());
        }

        /// <summary>
        /// 
        /// </summary>
        [Fact]
        public void ThrowsOnCreateIndexNullParent()
        {
            Assert.Throws<ArgumentNullException>("parent", () => new PropertyChain(null, 0));
        }

        /// <summary>
        /// 
        /// </summary>
        [Fact]
        public void ThrowsOnCreateIndexWithoutProperty()
        {
            var parent = new PropertyChain();

            Assert.Throws<ArgumentException>("parent", () => new PropertyChain(parent, 0));
        }

        /// <summary>
        /// 
        /// </summary>
        [Fact]
        public void CanCreateIndexedProperty()
        {
            var tested = new PropertyChain(new PropertyChain(new PropertyChain(), "Asdf"), 1);

            Assert.Equal("Asdf", tested.Name);
            Assert.Equal("Asdf[1]", tested.FullName);
            Assert.Equal("Asdf[1]", tested.ToString());
        }

    }

}
