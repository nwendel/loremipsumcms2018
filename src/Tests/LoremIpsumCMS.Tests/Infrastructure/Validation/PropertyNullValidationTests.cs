﻿#region License
// Copyright (c) Niklas Wendel 2017-2018
// 
// Licensed under the Apache License, Version 2.0 (the "License"); 
// you may not use this file except in compliance with the License. 
// You may obtain a copy of the License at 
// 
// http://www.apache.org/licenses/LICENSE-2.0 
// 
// Unless required by applicable law or agreed to in writing, software 
// distributed under the License is distributed on an "AS IS" BASIS, 
// WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied. 
// See the License for the specific language governing permissions and 
// limitations under the License.
#endregion
using LoremIpsumCMS.Infrastructure.Linq;
using LoremIpsumCMS.Infrastructure.Validation;
using Microsoft.Extensions.DependencyInjection;
using System.Threading.Tasks;
using Xunit;

namespace LoremIpsumCMS.Tests.Infrastructure.Validation
{

    /// <summary>
    /// 
    /// </summary>
    public class PropertyNullValidationTests
    {

        /// <summary>
        /// 
        /// </summary>
        [Fact]
        public async Task CanValidateNull()
        {
            var customer = new Customer();

            var tested = new CustomerValidator(true);
            var serviceProvider = new ServiceCollection()
                .BuildServiceProvider();
            var context = new ValidationContext<Customer>(customer, serviceProvider);
            var messages = await tested.ValidateAsync(context).ToListAsync();

            Assert.Empty(messages);
        }

        /// <summary>
        /// 
        /// </summary>
        [Fact]
        public async Task CanValidateNullInvalid()
        {
            var customer = new Customer
            {
                Name = "Lorem Ipsum"
            };

            var tested = new CustomerValidator(true);
            var serviceProvider = new ServiceCollection()
                .BuildServiceProvider();
            var context = new ValidationContext<Customer>(customer, serviceProvider);
            var messages = await tested.ValidateAsync(context).ToListAsync();

            Assert.Single(messages);
            Assert.All(messages, x =>
            {
                Assert.Equal("Name", x.PropertyName);
                Assert.Equal("Name must not be set", x.Text);
            });
        }

        /// <summary>
        /// 
        /// </summary>
        [Fact]
        public async Task CanValidateNotNull()
        {
            var customer = new Customer
            {
                Name = "Lorem Ipsum"
            };

            var tested = new CustomerValidator(false);
            var serviceProvider = new ServiceCollection()
                .BuildServiceProvider();
            var context = new ValidationContext<Customer>(customer, serviceProvider);
            var messages = await tested.ValidateAsync(context).ToListAsync();

            Assert.Empty(messages);
        }

        /// <summary>
        /// 
        /// </summary>
        [Fact]
        public async Task CanValidateNotNullInvalid()
        {
            var customer = new Customer();

            var tested = new CustomerValidator(false);
            var serviceProvider = new ServiceCollection()
                .BuildServiceProvider();
            var context = new ValidationContext<Customer>(customer, serviceProvider);
            var messages = await tested.ValidateAsync(context).ToListAsync();

            Assert.Single(messages);
            Assert.All(messages, x =>
            {
                Assert.Equal("Name", x.PropertyName);
                Assert.Equal("Name must be set", x.Text);
            });
        }

        #region Model / Validators

        /// <summary>
        /// 
        /// </summary>
        public class Customer
        {

            /// <summary>
            /// 
            /// </summary>
            public string Name { get; set; }

        }

        /// <summary>
        /// 
        /// </summary>
        public class CustomerValidator : AbstractValidator<Customer>
        {

            /// <summary>
            /// 
            /// </summary>
            /// <param name="null"></param>
            public CustomerValidator(bool @null)
            {
                if (@null)
                {
                    RuleFor(x => x.Name).Null();
                }
                else
                {
                    RuleFor(x => x.Name).NotNull();
                }
            }

        }

        #endregion

    }

}
