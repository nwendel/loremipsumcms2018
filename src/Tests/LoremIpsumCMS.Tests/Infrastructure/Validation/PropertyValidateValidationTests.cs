﻿#region License
// Copyright (c) Niklas Wendel 2017-2018
// 
// Licensed under the Apache License, Version 2.0 (the "License"); 
// you may not use this file except in compliance with the License. 
// You may obtain a copy of the License at 
// 
// http://www.apache.org/licenses/LICENSE-2.0 
// 
// Unless required by applicable law or agreed to in writing, software 
// distributed under the License is distributed on an "AS IS" BASIS, 
// WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied. 
// See the License for the specific language governing permissions and 
// limitations under the License.
#endregion
using LoremIpsumCMS.Infrastructure.Linq;
using LoremIpsumCMS.Infrastructure.Validation;
using Microsoft.Extensions.DependencyInjection;
using System.Threading.Tasks;
using Xunit;

namespace LoremIpsumCMS.Tests.Infrastructure.Validation
{

    /// <summary>
    /// 
    /// </summary>
    public class PropertyValidateValidationTests
    {

        /// <summary>
        /// 
        /// </summary>
        [Fact]
        public async Task CanValidateAgeDefault()
        {
            var order = new Order
            {
                Customer = new Customer
                {
                    Age = 47
                }
            };

            var tested = new OrderValidator();
            var serviceProvider = new ServiceCollection()
                .AddSingleton<IValidator<Customer>, CustomerValidator>()
                .BuildServiceProvider();
            var context = new ValidationContext<Order>(order, serviceProvider);
            var messages = await tested.ValidateAsync(context).ToListAsync();

            Assert.Empty(messages);
        }

        /// <summary>
        /// 
        /// </summary>
        [Fact]
        public async Task CanValidateAgeDefaultInvalid()
        {
            var order = new Order
            {
                Customer = new Customer()
            };

            var tested = new OrderValidator();
            var serviceProvider = new ServiceCollection()
                .AddSingleton<IValidator<Customer>, CustomerValidator>()
                .BuildServiceProvider();
            var context = new ValidationContext<Order>(order, serviceProvider);
            var messages = await tested.ValidateAsync(context).ToListAsync();

            Assert.Single(messages);
            Assert.All(messages, x =>
            {
                Assert.Equal("Customer.Age", x.PropertyName);
                Assert.Equal("Age must be set", x.Text);
            });
        }

        /// <summary>
        /// 
        /// </summary>
        [Fact]
        public async Task CanValidateAgeNullCustomer()
        {
            var order = new Order();

            var tested = new OrderValidator();
            var serviceProvider = new ServiceCollection()
                .AddSingleton<IValidator<Customer>, CustomerValidator>()
                .BuildServiceProvider();
            var context = new ValidationContext<Order>(order, serviceProvider);
            var messages = await tested.ValidateAsync(context).ToListAsync();

            Assert.Empty(messages);
        }

        /// <summary>
        /// 
        /// </summary>
        [Fact]
        public async Task CanValidateNullCustomer()
        {
            var order = new Order();

            var tested = new OrderValidator();
            var serviceProvider = new ServiceCollection()
                .AddSingleton<IValidator<Customer>, CustomerValidator>()
                .BuildServiceProvider();
            var context = new ValidationContext<Order>(order, serviceProvider);
            var messages = await tested.ValidateAsync(context).ToListAsync();

            Assert.Empty(messages);
        }

        #region Model / Validators

        /// <summary>
        /// 
        /// </summary>
        public class Order
        {

            /// <summary>
            /// 
            /// </summary>
            public Customer Customer { get; set; }

        }


        /// <summary>
        /// 
        /// </summary>
        public class Customer
        {

            /// <summary>
            /// 
            /// </summary>
            public int Age { get; set; }

        }

        /// <summary>
        /// 
        /// </summary>
        public class OrderValidator : AbstractValidator<Order>
        {

            /// <summary>
            /// 
            /// </summary>
            public OrderValidator()
            {
                RuleFor(p => p.Customer).Validate();
            }

        }

        /// <summary>
        /// 
        /// </summary>
        public class CustomerValidator : AbstractValidator<Customer>
        {

            /// <summary>
            /// 
            /// </summary>
            public CustomerValidator()
            {
                RuleFor(x => x.Age).NotDefault();
            }

        }

        #endregion

    }

}
