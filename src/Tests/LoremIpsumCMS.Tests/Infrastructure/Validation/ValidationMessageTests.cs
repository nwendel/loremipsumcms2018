﻿#region License
// Copyright (c) Niklas Wendel 2017-2018
// 
// Licensed under the Apache License, Version 2.0 (the "License"); 
// you may not use this file except in compliance with the License. 
// You may obtain a copy of the License at 
// 
// http://www.apache.org/licenses/LICENSE-2.0 
// 
// Unless required by applicable law or agreed to in writing, software 
// distributed under the License is distributed on an "AS IS" BASIS, 
// WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied. 
// See the License for the specific language governing permissions and 
// limitations under the License.
#endregion
using LoremIpsumCMS.Infrastructure.Validation;
using LoremIpsumCMS.Infrastructure.Validation.Internal;
using System;
using Xunit;

namespace LoremIpsumCMS.Tests.Infrastructure.Validation
{

    /// <summary>
    /// 
    /// </summary>
    public class ValidationMessageTests
    {

        /// <summary>
        /// 
        /// </summary>
        [Fact]
        public void ThrowsOnCreateNullPropertyName()
        {
            Assert.Throws<ArgumentNullException>("propertyName", () => ValidationMessage.Create((string)null, "text"));
        }

        /// <summary>
        /// 
        /// </summary>
        [Fact]
        public void ThrowsOnCreateNullText()
        {
            Assert.Throws<ArgumentNullException>("text", () => ValidationMessage.Create("property", null));
        }

        /// <summary>
        /// 
        /// </summary>
        [Fact]
        public void CanCreate()
        {
            var tested = ValidationMessage.Create("property", "text");

            Assert.Equal("property", tested.PropertyName);
            Assert.Equal("text", tested.Text);
        }

        /// <summary>
        /// 
        /// </summary>
        [Fact]
        public void CanCreateEmptyPropertyName()
        {
            var tested = ValidationMessage.Create(string.Empty, "text");

            Assert.Equal(string.Empty, tested.PropertyName);
            Assert.Equal("text", tested.Text);
        }

        /// <summary>
        /// 
        /// </summary>
        [Fact]
        public void ThrowsOnCreateExpressionNullProperty()
        {
            Assert.Throws<ArgumentNullException>("propertyExpression", () => ValidationMessage.Create<Customer>(null, "text"));
        }

        /// <summary>
        /// 
        /// </summary>
        [Fact]
        public void ThrowsOnCreateExpressionNullText()
        {
            Assert.Throws<ArgumentNullException>("text", () => ValidationMessage.Create<Customer>(x => x.Age, null));
        }

        /// <summary>
        /// 
        /// </summary>
        [Fact]
        public void CanCreateExpression()
        {
            var tested = ValidationMessage.Create<Customer>(x => x.Age, "text");

            Assert.Equal(nameof(Customer.Age), tested.PropertyName);
            Assert.Equal("text", tested.Text);
        }

        /// <summary>
        /// 
        /// </summary>
        [Fact]
        public void ThrowsOnCreateChainNullProperty()
        {
            Assert.Throws<ArgumentNullException>("propertyChain", () => ValidationMessage.Create((PropertyChain)null, "text"));
        }

        /// <summary>
        /// 
        /// </summary>
        [Fact]
        public void ThrowsOnCreateChainNullText()
        {
            var propertyChain = new PropertyChain();
            Assert.Throws<ArgumentNullException>("text", () => ValidationMessage.Create(propertyChain, null));
        }

        /// <summary>
        /// 
        /// </summary>
        [Fact]
        public void CanCreateChain()
        {
            var propertyChain = new PropertyChain(new PropertyChain(), nameof(Customer.Age));
            var tested = ValidationMessage.Create(propertyChain, "text");

            Assert.Equal(nameof(Customer.Age), tested.PropertyName);
            Assert.Equal("text", tested.Text);
        }

        #region Model

        /// <summary>
        /// 
        /// </summary>
        public class Customer
        {

            /// <summary>
            /// 
            /// </summary>
            public int Age { get; set; }

        }

        #endregion

    }

}
