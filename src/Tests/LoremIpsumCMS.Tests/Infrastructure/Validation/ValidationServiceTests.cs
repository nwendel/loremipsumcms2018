﻿#region License
// Copyright (c) Niklas Wendel 2017-2018
// 
// Licensed under the Apache License, Version 2.0 (the "License"); 
// you may not use this file except in compliance with the License. 
// You may obtain a copy of the License at 
// 
// http://www.apache.org/licenses/LICENSE-2.0 
// 
// Unless required by applicable law or agreed to in writing, software 
// distributed under the License is distributed on an "AS IS" BASIS, 
// WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied. 
// See the License for the specific language governing permissions and 
// limitations under the License.
#endregion
using LoremIpsumCMS.Infrastructure.Validation;
using Moq;
using System;
using System.Collections.Generic;
using System.Threading.Tasks;
using Xunit;

namespace LoremIpsumCMS.Tests.Infrastructure.Validation
{

    /// <summary>
    /// 
    /// </summary>
    public class ValidationServiceTests
    {

        private readonly ValidationService _tested;
        private readonly Mock<IServiceProvider> _serviceProviderMock = new Mock<IServiceProvider>();
        private readonly Mock<IValidator<Model>> _validatorMock = new Mock<IValidator<Model>>();


        /// <summary>
        /// 
        /// </summary>
        public ValidationServiceTests()
        {
            _tested = new ValidationService(_serviceProviderMock.Object);
        }

        /// <summary>
        /// 
        /// </summary>
        /// <returns></returns>
        [Fact]
        public async Task ThrowsOnValidateAsyncNullInstance()
        {
            await Assert.ThrowsAsync<ArgumentNullException>("instance", () => _tested.ValidateAsync(null));
        }

        /// <summary>
        /// 
        /// </summary>
        [Fact]
        public async Task CanValidateAsyncValidModel()
        {
            _serviceProviderMock
                .Setup(x => x.GetService(typeof(IEnumerable<IValidator<Model>>)))
                .Returns(new[] { _validatorMock.Object });
            _validatorMock
                .Setup(x => x.ValidateAsync(It.IsAny<IValidationContext<Model>>()))
                .ReturnsAsync(new ValidationMessage[0]);

            var model = new Model();
            var messages = await _tested.ValidateAsync(model);

            Assert.Empty(messages);
        }

        /// <summary>
        /// 
        /// </summary>
        [Fact]
        public async Task CanValidateAsyncInvalidModel()
        {
            _serviceProviderMock
                .Setup(x => x.GetService(typeof(IEnumerable<IValidator<Model>>)))
                .Returns(new[] { _validatorMock.Object });
            _validatorMock
                .Setup(x => x.ValidateAsync(It.IsAny<IValidationContext<Model>>()))
                .ReturnsAsync(new[] {ValidationMessage.Create("Property", "Text")});

            var model = new Model();
            var messages = await _tested.ValidateAsync(model);

            Assert.Single(messages);
        }

        #region Model

        /// <summary>
        /// 
        /// </summary>
        public class Model
        {

        }

        #endregion

    }

}
