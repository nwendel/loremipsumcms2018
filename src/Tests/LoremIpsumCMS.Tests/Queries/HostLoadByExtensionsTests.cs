﻿#region License
// Copyright (c) Niklas Wendel 2017-2018
// 
// Licensed under the Apache License, Version 2.0 (the "License"); 
// you may not use this file except in compliance with the License. 
// You may obtain a copy of the License at 
// 
// http://www.apache.org/licenses/LICENSE-2.0 
// 
// Unless required by applicable law or agreed to in writing, software 
// distributed under the License is distributed on an "AS IS" BASIS, 
// WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied. 
// See the License for the specific language governing permissions and 
// limitations under the License.
#endregion
using LoremIpsumCMS.Infrastructure.Raven;
using LoremIpsumCMS.Infrastructure.Raven.UniqueConstraints;
using LoremIpsumCMS.Model;
using LoremIpsumCMS.Queries;
using LoremIpsumCMS.TestHelper;
using System;
using System.Threading.Tasks;
using Xunit;

namespace LoremIpsumCMS.Tests.Queries
{

    /// <summary>
    /// 
    /// </summary>
    public class HostLoadByExtensionsTests : AbstractRavenTests
    {

        /// <summary>
        /// 
        /// </summary>
        [Fact]
        public async Task ThrowsOnLoadByNameNullName()
        {
            var asyncDocumentSession = GetDocumentStore().OpenAsyncSession();

            await Assert.ThrowsAsync<ArgumentNullException>("name", () => asyncDocumentSession.LoadByAsync<Host>(x => x.Name(null)));
        }

        /// <summary>
        /// 
        /// </summary>
        [Fact]
        public async Task CanLoadByName()
        {
            var host = new Host("asdf", "sites/1-A", true);

            var store = GetDocumentStore();
            using (var asyncDocumentSession = store.OpenAsyncSession())
            {
                await asyncDocumentSession.StoreAsync(host);
                await asyncDocumentSession.SaveChangesExAsync();
            }

            using (var asyncDocumentSession = store.OpenAsyncSession())
            {
                var loaded = await asyncDocumentSession.LoadByAsync<Host>(x => x.Name(host.Name));

                Assert.Equal(host.Id, loaded.Id);
            }
        }

        /// <summary>
        /// 
        /// </summary>
        [Fact]
        public async Task ThrowsOnLoadByNameNullNames()
        {
            var asyncDocumentSession = GetDocumentStore().OpenAsyncSession();

            await Assert.ThrowsAsync<ArgumentNullException>("names", () => asyncDocumentSession.LoadByAsync<Host>(x => x.Names(null)));
        }

    }

}
