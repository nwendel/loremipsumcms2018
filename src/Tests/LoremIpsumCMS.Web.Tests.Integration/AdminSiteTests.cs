﻿#region License
// Copyright (c) Niklas Wendel 2017-2018
// 
// Licensed under the Apache License, Version 2.0 (the "License"); 
// you may not use this file except in compliance with the License. 
// You may obtain a copy of the License at 
// 
// http://www.apache.org/licenses/LICENSE-2.0 
// 
// Unless required by applicable law or agreed to in writing, software 
// distributed under the License is distributed on an "AS IS" BASIS, 
// WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied. 
// See the License for the specific language governing permissions and 
// limitations under the License.
#endregion
using LoremIpsumCMS.PuppeteerSharp;
using LoremIpsumCMS.TestHelper;
using LoremIpsumCMS.Web.Tests.Integration.PageModels;
using Microsoft.AspNetCore;
using Microsoft.AspNetCore.Hosting;
using PuppeteerSharp;
using System;
using System.Linq;
using System.Threading.Tasks;
using Xunit;

namespace LoremIpsumCMS.Web.Tests.Integration
{

    /// <summary>
    /// 
    /// </summary>
    public class AdminSiteTests : AbstractRavenTests
    {

        private readonly PuppeteerHost _puppeteerHost;
                                                          
        /// <summary>
        /// 
        /// </summary>
        public AdminSiteTests()
        {
            var store = GetDocumentStore(null, Guid.NewGuid().ToString());
            TestStartup.Url = store.Urls.First();

            var webHostBuilder = WebHost
                .CreateDefaultBuilder()
                .UseStartup<TestStartup>();

            new BrowserFetcher().DownloadAsync(BrowserFetcher.DefaultRevision).Wait();
            var browser = Puppeteer.LaunchAsync(new LaunchOptions()).Result;

            _puppeteerHost = new PuppeteerHost(browser, webHostBuilder);
        }

        /// <summary>
        /// 
        /// </summary>
        [Fact]
        public async Task CanViewCreateSite()
        {
            var page = await _puppeteerHost
                .NavigateTo<AdminCreateSitePageModel>("/admin/site/create")
                .ExecuteAsync();

            // TODO: Not correct assert, all page tiles have this title?
            Assert.Equal("Lorem Ipsum CMS - Administration", await page.TitleAsync);
        }

        /// <summary>
        /// 
        /// </summary>
        [Fact]
        public async Task CanCreateSite()
        {
            var page = await _puppeteerHost
                .NavigateTo<AdminCreateSitePageModel>("/admin/site/create")
                .CreateSite("Some Name")
                .ExecuteAsync();

            // TODO: Not correct assert, all page tiles have this title?
            Assert.Equal("Lorem Ipsum CMS - Administration", await page.TitleAsync);
        }

        /// <summary>
        /// 
        /// </summary>
        public override void Dispose()
        {
            base.Dispose();
            _puppeteerHost?.Dispose();
        }

    }

}
