﻿#region License
// Copyright (c) Niklas Wendel 2017-2018
// 
// Licensed under the Apache License, Version 2.0 (the "License"); 
// you may not use this file except in compliance with the License. 
// You may obtain a copy of the License at 
// 
// http://www.apache.org/licenses/LICENSE-2.0 
// 
// Unless required by applicable law or agreed to in writing, software 
// distributed under the License is distributed on an "AS IS" BASIS, 
// WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied. 
// See the License for the specific language governing permissions and 
// limitations under the License.
#endregion
using LoremIpsumCMS.Web.Areas.AdminShell.Controllers;
using Microsoft.AspNetCore.Builder;
using Microsoft.AspNetCore.Hosting;
using Microsoft.AspNetCore.Mvc.ModelBinding;
using Microsoft.AspNetCore.TestHost;
using Microsoft.Extensions.DependencyInjection;
using MvcRouteTester.AspNetCore;
using System.Threading.Tasks;

//using LoremIpsumCMS.Web.Infrastructure.Mvc.ModelBinding.Binders;

namespace LoremIpsumCMS.Web.Tests.Routes
{

    /// <summary>
    /// 
    /// </summary>
    public class RouteTestFixture
    {

        /// <summary>
        /// 
        /// </summary>
        public RouteTestFixture()
        {
            Server = new TestServer(new WebHostBuilder()
                .UseStartup<TestStartup>()
                .UseSetting(WebHostDefaults.ApplicationKey, typeof(AdminDashboardController).Assembly.FullName));
        }

        /// <summary>
        /// 
        /// </summary>
        public TestServer Server { get; }

        /// <summary>
        /// 
        /// </summary>
        public class TestStartup
        {

            /// <summary>
            /// 
            /// </summary>
            /// <param name="services"></param>
            public void ConfigureServices(IServiceCollection services)
            {
                services.AddMvc();
                services.AddMvcRouteTester();

                //services.Configure<MvcOptions>(o =>
                //{
                //    o.ModelBinderProviders.Insert(0, new SimpleModelBinderProvider<Site, NullModelBinder>());
                //    o.ModelBinderProviders.Insert(0, new SimpleModelBinderProvider<SiteReference, NullModelBinder>());
                //});
            }

            /// <summary>
            /// 
            /// </summary>
            /// <param name="app"></param>
            /// <param name="env"></param>
            public void Configure(IApplicationBuilder app, IHostingEnvironment env)
            {
                app.UseMvc();
            }

        }

        /// <summary>
        /// 
        /// </summary>
        public class NullModelBinder : IModelBinder
        {

            /// <summary>
            /// 
            /// </summary>
            /// <param name="bindingContext"></param>
            /// <returns></returns>
            public Task BindModelAsync(ModelBindingContext bindingContext)
            {
                //bindingContext.ModelState.SetModelValue(bindingContext.ModelName, null, null);
                //bindingContext.Result = ModelBindingResult.Success(null);

                return Task.CompletedTask;
            }

        }

    }

}
