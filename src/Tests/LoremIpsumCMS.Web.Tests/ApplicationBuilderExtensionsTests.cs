﻿#region License
// Copyright (c) Niklas Wendel 2017-2018
// 
// Licensed under the Apache License, Version 2.0 (the "License"); 
// you may not use this file except in compliance with the License. 
// You may obtain a copy of the License at 
// 
// http://www.apache.org/licenses/LICENSE-2.0 
// 
// Unless required by applicable law or agreed to in writing, software 
// distributed under the License is distributed on an "AS IS" BASIS, 
// WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied. 
// See the License for the specific language governing permissions and 
// limitations under the License.
#endregion
using Microsoft.AspNetCore.Builder.Internal;
using Microsoft.AspNetCore.Hosting;
using Microsoft.Extensions.DependencyInjection;
using Moq;
using Xunit;

namespace LoremIpsumCMS.Web.Tests
{

    /// <summary>
    /// 
    /// </summary>
    public class ApplicationBuilderExtensionsTests
    {

        private Mock<IHostingEnvironment> _hostingEnvironmentMock = new Mock<IHostingEnvironment>();

        /// <summary>
        /// 
        /// </summary>
        [Fact]
        public void CanUseLoremIpsum()
        {
            var services = new ServiceCollection();
            services.AddLogging();
            services.AddSingleton(_hostingEnvironmentMock.Object);
            services.AddLoremIpsum();
            var serviceProvider = services.BuildServiceProvider();
            var tested = new ApplicationBuilder(serviceProvider);

            tested.UseLoremIpsum();
            var result = tested.Build();

            Assert.NotNull(result);
        }

    }

}
