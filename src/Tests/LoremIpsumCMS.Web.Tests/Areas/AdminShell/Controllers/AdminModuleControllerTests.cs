﻿#region License
// Copyright (c) Niklas Wendel 2017-2018
// 
// Licensed under the Apache License, Version 2.0 (the "License"); 
// you may not use this file except in compliance with the License. 
// You may obtain a copy of the License at 
// 
// http://www.apache.org/licenses/LICENSE-2.0 
// 
// Unless required by applicable law or agreed to in writing, software 
// distributed under the License is distributed on an "AS IS" BASIS, 
// WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied. 
// See the License for the specific language governing permissions and 
// limitations under the License.
#endregion
using AutoMapper;
using LoremIpsumCMS.Infrastructure.Modules;
using LoremIpsumCMS.Web.Areas.AdminShell.Controllers;
using LoremIpsumCMS.Web.Areas.AdminShell.Mappers.AdminModule;
using LoremIpsumCMS.Web.Areas.AdminShell.ViewModels.AdminModule;
using Microsoft.AspNetCore.Mvc;
using System.Collections.Generic;
using System.Linq;
using Xunit;

namespace LoremIpsumCMS.Web.Tests.Areas.AdminShell.Controllers
{

    /// <summary>
    /// 
    /// </summary>
    public class AdminModuleControllerTests
    {

        private readonly IMapper _mapper;


        public AdminModuleControllerTests()
        {
            var mapperConfiguration = new MapperConfiguration(c =>
            {
                c.AddProfile<AdminModuleMapperProfile>();
            });
            _mapper = mapperConfiguration.CreateMapper();
        }

        /// <summary>
        /// 
        /// </summary>
        [Fact]
        public void CanDetails()
        {
            var modules = new IModule[] {new CoreModule(), new CoreWebModule()};
            var tested = new AdminModuleController(modules, _mapper);

            var result = tested.Index();

            var viewResult = Assert.IsType<ViewResult>(result);
            var indexViewModel = Assert.IsAssignableFrom<IEnumerable<IndexModuleViewModel>>(viewResult.Model)
                .ToArray();

            Assert.Contains(indexViewModel, x => x.Name == modules[0].Name);
            Assert.Contains(indexViewModel, x => x.Version == typeof(CoreModule).Assembly.GetName().Version.ToString());
            Assert.Contains(indexViewModel, x => x.AssemblyName == typeof(CoreModule).Assembly.GetName().Name);
            Assert.Contains(indexViewModel, x => x.Name == modules[1].Name);
            Assert.Contains(indexViewModel, x => x.Version == typeof(CoreWebModule).Assembly.GetName().Version.ToString());
            Assert.Contains(indexViewModel, x => x.AssemblyName == typeof(CoreWebModule).Assembly.GetName().Name);
        }

    }

}
