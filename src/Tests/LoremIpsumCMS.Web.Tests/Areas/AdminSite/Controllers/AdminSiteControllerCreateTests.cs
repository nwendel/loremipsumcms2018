﻿#region License
// Copyright (c) Niklas Wendel 2017-2018
// 
// Licensed under the Apache License, Version 2.0 (the "License"); 
// you may not use this file except in compliance with the License. 
// You may obtain a copy of the License at 
// 
// http://www.apache.org/licenses/LICENSE-2.0 
// 
// Unless required by applicable law or agreed to in writing, software 
// distributed under the License is distributed on an "AS IS" BASIS, 
// WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied. 
// See the License for the specific language governing permissions and 
// limitations under the License.
#endregion
using AutoMapper;
using LoremIpsumCMS.Commands;
using LoremIpsumCMS.Infrastructure.Cqrs;
using LoremIpsumCMS.Web.Areas.AdminSite.Controllers;
using LoremIpsumCMS.Web.Areas.AdminSite.Mappers.AdminSite;
using LoremIpsumCMS.Web.Areas.AdminSite.ViewModels.AdminSite;
using LoremIpsumCMS.Web.Infrastructure.Http;
using Microsoft.AspNetCore.Http;
using Microsoft.AspNetCore.Mvc;
using Moq;
using System.Threading.Tasks;
using Xunit;

namespace LoremIpsumCMS.Web.Tests.Areas.AdminSite.Controllers
{

    /// <summary>
    /// 
    /// </summary>
    public class AdminSiteControllerCreateTests
    {

        private readonly AdminSiteController _tested;

        private readonly Mock<IHostAccessor> _hostAccessorMock = new Mock<IHostAccessor>();
        private readonly Mock<ICommandSender> _commandSenderMock = new Mock<ICommandSender>();

        /// <summary>
        /// 
        /// </summary>
        public AdminSiteControllerCreateTests()
        {
            var mapper = new MapperConfiguration(config => { config.AddProfile<AdminSiteMapperProfile>(); })
                .CreateMapper();

            _tested = new AdminSiteController(_hostAccessorMock.Object, _commandSenderMock.Object, mapper, null, null, null);
        }

        /// <summary>
        /// 
        /// </summary>
        [Fact]
        public void CanGetCreate()
        {
            _hostAccessorMock
                .Setup(x => x.CurrentHost)
                .Returns(new HostString("localhost"));

            var result = _tested.Create();

            var viewResult = Assert.IsType<ViewResult>(result);
            var viewModel = Assert.IsType<CreateViewModel>(viewResult.Model);
            Assert.Null(viewModel.Name);
            Assert.Single(viewModel.Hosts);
            Assert.Contains(viewModel.Hosts, x => x.Name == "localhost");
        }

        /// <summary>
        /// 
        /// </summary>
        /// <returns></returns>
        [Fact(Skip = "Not now")]
        public async Task CanPostCreate()
        {
            var viewModel = new CreateViewModel
            {
                Name = "Some Name",
                Hosts = new[]
                {
                    new SiteHostViewModel {Name = "localhost", IsAdmin = true}
                }
            };

            _commandSenderMock
                .Setup(x => x.SendAsync(It.Is<CreateSiteCommand>(c => c.Name == viewModel.Name)))
                .ReturnsAsync("site/1-A");

            var result = await _tested.Create(viewModel);

            var redirectToActionResult = Assert.IsType<RedirectToActionResult>(result);
            Assert.Equal(nameof(AdminSiteController.Index), redirectToActionResult.ActionName);
        }

        /// <summary>
        /// 
        /// </summary>
        [Fact]
        public void CanInvalidCreate()
        {
            var viewModel = new CreateViewModel();

            var result = _tested.InvalidCreate(viewModel);

            var viewResult = Assert.IsType<ViewResult>(result);
            Assert.IsType<CreateViewModel>(viewResult.Model);
        }

    }

}
