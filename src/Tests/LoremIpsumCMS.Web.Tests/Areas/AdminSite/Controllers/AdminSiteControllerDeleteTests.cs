﻿#region License
// Copyright (c) Niklas Wendel 2017-2018
// 
// Licensed under the Apache License, Version 2.0 (the "License"); 
// you may not use this file except in compliance with the License. 
// You may obtain a copy of the License at 
// 
// http://www.apache.org/licenses/LICENSE-2.0 
// 
// Unless required by applicable law or agreed to in writing, software 
// distributed under the License is distributed on an "AS IS" BASIS, 
// WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied. 
// See the License for the specific language governing permissions and 
// limitations under the License.
#endregion
using AutoMapper;
using LoremIpsumCMS.Commands;
using LoremIpsumCMS.Events;
using LoremIpsumCMS.Infrastructure.Cqrs;
using LoremIpsumCMS.Infrastructure.Raven.UniqueConstraints;
using LoremIpsumCMS.Model;
using LoremIpsumCMS.TestHelper;
using LoremIpsumCMS.Web.Areas.AdminSite.Controllers;
using LoremIpsumCMS.Web.Areas.AdminSite.Mappers.AdminSite;
using LoremIpsumCMS.Web.Areas.AdminSite.ViewModels.AdminSite;
using LoremIpsumCMS.Web.Infrastructure.Http;
using Microsoft.AspNetCore.Mvc;
using Moq;
using Raven.Client.Documents.Session;
using System;
using System.Threading.Tasks;
using Xunit;

namespace LoremIpsumCMS.Web.Tests.Areas.AdminSite.Controllers
{

    /// <summary>
    /// 
    /// </summary>
    public class AdminSiteControllerDeleteTests : AbstractRavenTests
    {

        private readonly AdminSiteController _tested;

        private readonly Mock<IHostAccessor> _hostAccessorMock = new Mock<IHostAccessor>();
        private readonly Mock<IServiceProvider> _serviceProviderMock = new Mock<IServiceProvider>();
        private readonly Mock<ICommandSender> _commandSenderMock = new Mock<ICommandSender>();

        private readonly IAsyncDocumentSession _asyncDocumentSession;

        /// <summary>
        /// 
        /// </summary>
        public AdminSiteControllerDeleteTests()
        {
            var mapper = new MapperConfiguration(config =>
                {
                    config.AddProfile<AdminSiteMapperProfile>();
                    config.ConstructServicesUsing(t => _serviceProviderMock.Object.GetService(t));
                })
                .CreateMapper();
            _asyncDocumentSession = DocumentStore.OpenAsyncSession();

            _tested = new AdminSiteController(_hostAccessorMock.Object, _commandSenderMock.Object, mapper, _asyncDocumentSession, null, null);
        }

        /// <summary>
        /// 
        /// </summary>
        /// <returns></returns>
        [Fact]
        public async Task CanDelete()
        {
            var site = new Site();
            site.Apply(new SiteCreatedEvent
            {
                Name = "Some Name",
                Hosts = new [] { new SiteHost("localhost", true) }
            });

            await _asyncDocumentSession.StoreAsync(site);
            await _asyncDocumentSession.SaveChangesExAsync();

            _commandSenderMock
                .Setup(x => x.SendAsync(It.Is<DeleteSiteCommand>(c => c.Id == site.Id)))
                .ReturnsAsync(site.Id);

            var viewModel = new DeleteViewModel
            {
                NameSlug = "some-name"
            };

            var result = await _tested.Delete(viewModel);

            var redirectResult = Assert.IsType<RedirectToActionResult>(result);
            Assert.Same(nameof(AdminSiteController.Index), redirectResult.ActionName);
        }

    }

}
