﻿#region License
// Copyright (c) Niklas Wendel 2017-2018
// 
// Licensed under the Apache License, Version 2.0 (the "License"); 
// you may not use this file except in compliance with the License. 
// You may obtain a copy of the License at 
// 
// http://www.apache.org/licenses/LICENSE-2.0 
// 
// Unless required by applicable law or agreed to in writing, software 
// distributed under the License is distributed on an "AS IS" BASIS, 
// WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied. 
// See the License for the specific language governing permissions and 
// limitations under the License.
#endregion
using AutoMapper;
using LoremIpsumCMS.Events;
using LoremIpsumCMS.Infrastructure.Cqrs;
using LoremIpsumCMS.Infrastructure.Raven.UniqueConstraints;
using LoremIpsumCMS.Model;
using LoremIpsumCMS.TestHelper;
using LoremIpsumCMS.Web.Areas.AdminSite.Controllers;
using LoremIpsumCMS.Web.Areas.AdminSite.Mappers.AdminSite;
using LoremIpsumCMS.Web.Areas.AdminSite.ViewModels.AdminSite;
using LoremIpsumCMS.Web.Infrastructure.Http;
using LoremIpsumCMS.Web.Infrastructure.Validation;
using Microsoft.AspNetCore.Mvc;
using Moq;
using Raven.Client.Documents.Session;
using System.Threading.Tasks;
using Xunit;

namespace LoremIpsumCMS.Web.Tests.Areas.AdminSite.Controllers
{

    /// <summary>
    /// 
    /// </summary>
    public class AdminSiteControllerEditTests : AbstractRavenTests
    {

        private readonly AdminSiteController _tested;

        private readonly Mock<IHostAccessor> _hostAccessorMock = new Mock<IHostAccessor>();
        private readonly Mock<ICommandSender> _commandSenderMock = new Mock<ICommandSender>();

        private readonly IAsyncDocumentSession _asyncDocumentSession;

        /// <summary>
        /// 
        /// </summary>
        public AdminSiteControllerEditTests()
        {
            var mapper = new MapperConfiguration(config => { config.AddProfile<AdminSiteMapperProfile>(); })
                .CreateMapper();
            _asyncDocumentSession = DocumentStore.OpenAsyncSession();

            _tested = new AdminSiteController(_hostAccessorMock.Object, _commandSenderMock.Object, mapper, _asyncDocumentSession, null, null);
        }

        /// <summary>
        /// 
        /// </summary>
        [Fact]
        public void CanGetEdit()
        {
            var site = new Site();
            site.ApplyEvent(new SiteCreatedEvent
            {
                Name = "some name",
                Hosts = new [] {new SiteHost("localhost", true), }
            });

            var result = _tested.Edit(site);

            var viewResult = Assert.IsType<ViewResult>(result);
            var viewModel = Assert.IsType<EditViewModel>(viewResult.Model);
            Assert.Equal(site.Name, viewModel.Name);
            Assert.Contains(viewModel.Hosts, x => x.Name == "localhost");
        }

        /// <summary>
        /// 
        /// </summary>
        [Fact(Skip = "Not now")]
        public async Task CanPostEdit()
        {
            var site = new Site();
            site.Apply(new SiteCreatedEvent
            {
                Name = "Some Site",
                Hosts = new[] { new SiteHost("localhost", true) }
            });

            await _asyncDocumentSession.StoreAsync(site);
            await _asyncDocumentSession.SaveChangesExAsync();

            var viewModel = new EditViewModel
            {
                OldNameSlug = "some-site",
                Name = "new name",
                Hosts = new[]
                {
                    new SiteHostViewModel {Name = "localhost"}
                }
            };

            var result = await _tested.Edit(viewModel);

            var redirectResult = Assert.IsType<RedirectToActionResult>(result);
            Assert.Equal(nameof(AdminSiteController.Index), redirectResult.ActionName);
        }

        /// <summary>
        /// 
        /// </summary>
        [Fact]
        public void CanValidateEdit()
        {
            var site = new Site();
            var result = _tested.ValidateEdit(site);

            Assert.Equal(ValidationResult.Success, result);
        }

        /// <summary>
        /// 
        /// </summary>
        [Fact]
        public void CanValidateEditNotFound()
        {
            var result = _tested.ValidateEdit(null);

            Assert.Equal(ValidationResult.NotFound, result);
        }

        /// <summary>
        /// 
        /// </summary>
        [Fact]
        public void CanInvalidEdit()
        {
            var viewModel = new EditViewModel();

            var result = _tested.InvalidEdit(viewModel);

            var viewResult = Assert.IsType<ViewResult>(result);
            Assert.Equal(nameof(AdminSiteController.Edit), viewResult.ViewName);
            Assert.Same(viewModel, viewResult.Model);
        }

    }

}
