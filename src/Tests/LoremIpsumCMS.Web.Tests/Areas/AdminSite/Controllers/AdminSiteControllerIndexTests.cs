﻿#region License
// Copyright (c) Niklas Wendel 2017-2018
// 
// Licensed under the Apache License, Version 2.0 (the "License"); 
// you may not use this file except in compliance with the License. 
// You may obtain a copy of the License at 
// 
// http://www.apache.org/licenses/LICENSE-2.0 
// 
// Unless required by applicable law or agreed to in writing, software 
// distributed under the License is distributed on an "AS IS" BASIS, 
// WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied. 
// See the License for the specific language governing permissions and 
// limitations under the License.
#endregion
using AutoMapper;
using LoremIpsumCMS.Events;
using LoremIpsumCMS.Infrastructure.Raven.UniqueConstraints;
using LoremIpsumCMS.Model;
using LoremIpsumCMS.TestHelper;
using LoremIpsumCMS.Web.Areas.AdminSite.Controllers;
using LoremIpsumCMS.Web.Areas.AdminSite.Mappers.AdminSite;
using LoremIpsumCMS.Web.Areas.AdminSite.ViewModels.AdminSite;
using LoremIpsumCMS.Web.Infrastructure.Http;
using Microsoft.AspNetCore.Http;
using Microsoft.AspNetCore.Mvc;
using Moq;
using Raven.Client.Documents.Session;
using System;
using System.Threading.Tasks;
using Xunit;

namespace LoremIpsumCMS.Web.Tests.Areas.AdminSite.Controllers
{

    /// <summary>
    /// 
    /// </summary>
    public class AdminSiteControllerIndexTests : AbstractRavenTests
    {

        private readonly AdminSiteController _tested;

        private readonly Mock<IHostAccessor> _hostAccessorMock = new Mock<IHostAccessor>();
        private readonly Mock<IServiceProvider> _serviceProviderMock = new Mock<IServiceProvider>();

        private readonly IAsyncDocumentSession _asyncDocumentSession;

        /// <summary>
        /// 
        /// </summary>
        public AdminSiteControllerIndexTests()
        {
            var mapper = new MapperConfiguration(config =>
                {
                    config.AddProfile<AdminSiteMapperProfile>();
                    config.ConstructServicesUsing(t => _serviceProviderMock.Object.GetService(t));
                })
                .CreateMapper();
            _asyncDocumentSession = DocumentStore.OpenAsyncSession();

            _tested = new AdminSiteController(_hostAccessorMock.Object, null, mapper, _asyncDocumentSession, null, null);
        }

        /// <summary>
        /// 
        /// </summary>
        /// <returns></returns>
        [Fact]
        public async Task CanIndexNoSites()
        {
            var result = await _tested.Index();

            var viewResult = Assert.IsType<ViewResult>(result);
            var model = Assert.IsType<IndexViewModel>(viewResult.Model);
            Assert.Empty(model.Sites);
        }

        /// <summary>
        /// 
        /// </summary>
        /// <returns></returns>
        [Fact]
        public async Task CanIndexCurrentSite()
        {
            _serviceProviderMock
                .Setup(x => x.GetService(typeof(IndexSiteViewModelIsCurrentValueResolver)))
                .Returns(new IndexSiteViewModelIsCurrentValueResolver(_hostAccessorMock.Object));
            _hostAccessorMock
                .Setup(x => x.CurrentHost)
                .Returns(new HostString("localhost"));

            var site = new Site();
            site.ApplyEvent(new SiteCreatedEvent
            {
                Name = "Some Name",
                Hosts = new[] { new SiteHost("localhost", true) },
                CreatedAt = DateTime.Now
            });
            await _asyncDocumentSession.StoreAsync(site);
            await _asyncDocumentSession.SaveChangesExAsync();

            var result = await _tested.Index();

            var viewResult = Assert.IsType<ViewResult>(result);
            var model = Assert.IsType<IndexViewModel>(viewResult.Model);
            var siteViewModel = Assert.Single(model.Sites);
            Assert.NotNull(siteViewModel);
            Assert.Equal("Some Name", siteViewModel.Name);
            Assert.Equal("some-name", siteViewModel.NameSlug);
            Assert.NotNull(siteViewModel.CreatedAt);
            Assert.Empty(siteViewModel.LastUpdatedAt);
            Assert.True(siteViewModel.IsCurrent);
        }

        /// <summary>
        /// 
        /// </summary>
        /// <returns></returns>
        [Fact]
        public async Task CanIndexAnotherSite()
        {
            _serviceProviderMock
                .Setup(x => x.GetService(typeof(IndexSiteViewModelIsCurrentValueResolver)))
                .Returns(new IndexSiteViewModelIsCurrentValueResolver(_hostAccessorMock.Object));
            _hostAccessorMock
                .Setup(x => x.CurrentHost)
                .Returns(new HostString("localhost"));

            var site = new Site();
            site.ApplyEvent(new SiteCreatedEvent
            {
                Name = "Some Name",
                Hosts = new[] { new SiteHost("another", true) },
                CreatedAt = DateTime.Now
            });
            await _asyncDocumentSession.StoreAsync(site);
            await _asyncDocumentSession.SaveChangesExAsync();

            var result = await _tested.Index();

            var viewResult = Assert.IsType<ViewResult>(result);
            var model = Assert.IsType<IndexViewModel>(viewResult.Model);
            var siteViewModel = Assert.Single(model.Sites);
            Assert.NotNull(siteViewModel);
            Assert.False(siteViewModel.IsCurrent);
        }

    }

}
