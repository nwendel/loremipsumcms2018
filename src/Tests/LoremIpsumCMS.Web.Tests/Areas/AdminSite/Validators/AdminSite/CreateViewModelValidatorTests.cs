﻿#region License
// Copyright (c) Niklas Wendel 2017-2018
// 
// Licensed under the Apache License, Version 2.0 (the "License"); 
// you may not use this file except in compliance with the License. 
// You may obtain a copy of the License at 
// 
// http://www.apache.org/licenses/LICENSE-2.0 
// 
// Unless required by applicable law or agreed to in writing, software 
// distributed under the License is distributed on an "AS IS" BASIS, 
// WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied. 
// See the License for the specific language governing permissions and 
// limitations under the License.
#endregion
using LoremIpsumCMS.Events;
using LoremIpsumCMS.Infrastructure.Linq;
using LoremIpsumCMS.Infrastructure.Raven.UniqueConstraints;
using LoremIpsumCMS.Infrastructure.Validation;
using LoremIpsumCMS.Model;
using LoremIpsumCMS.TestHelper;
using LoremIpsumCMS.Web.Areas.AdminSite.Validators.AdminSite;
using LoremIpsumCMS.Web.Areas.AdminSite.ViewModels.AdminSite;
using Microsoft.Extensions.DependencyInjection;
using Raven.Client.Documents.Session;
using System;
using System.Threading.Tasks;
using Xunit;

namespace LoremIpsumCMS.Web.Tests.Areas.AdminSite.Validators.AdminSite
{

    /// <summary>
    /// 
    /// </summary>
    public class CreateViewModelValidatorTests : AbstractRavenTests
    {

        private readonly IServiceProvider _serviceProvider;
        private readonly IAsyncDocumentSession _asyncDocumentSession;
        private readonly CreateViewModelValidator _tested;

        /// <summary>
        /// 
        /// </summary>
        public CreateViewModelValidatorTests()
        {
            _serviceProvider = new ServiceCollection()
                .AddSingleton<CreateEditViewModelValidator, CreateEditViewModelValidator>()
                .BuildServiceProvider();
            _asyncDocumentSession = GetDocumentStore(null, Guid.NewGuid().ToString()).OpenAsyncSession();
            _tested = new CreateViewModelValidator(_asyncDocumentSession);
        }

        /// <summary>
        /// 
        /// </summary>
        /// <returns></returns>
        [Fact]
        public async Task CanValidateEmpty()
        {
            var viewModel = new CreateViewModel();

            var context = new ValidationContext<CreateViewModel>(viewModel, _serviceProvider);
            var messages = await _tested.ValidateAsync(context).ToListAsync();

            Assert.Contains(messages, m => m.PropertyName == nameof(CreateViewModel.Name));
            Assert.Contains(messages, m => m.PropertyName == nameof(CreateViewModel.Hosts));
        }

        /// <summary>
        /// 
        /// </summary>
        /// <returns></returns>
        [Fact]
        public async Task CanValidateHostnameNull()
        {
            var viewModel = new CreateViewModel
            {
                Hosts = new[] {new SiteHostViewModel()}
            };

            var context = new ValidationContext<CreateViewModel>(viewModel, _serviceProvider);
            var messages = await _tested.ValidateAsync(context).ToListAsync();

            Assert.Contains(messages, m => m.PropertyName == nameof(CreateViewModel.Hosts));
        }

        /// <summary>
        /// 
        /// </summary>
        /// <returns></returns>
        [Fact]
        public async Task CanValidateNoHosts()
        {
            var viewModel = new CreateViewModel
            {
                Hosts = new SiteHostViewModel[0]
            };

            var context = new ValidationContext<CreateViewModel>(viewModel, _serviceProvider);
            var messages = await _tested.ValidateAsync(context).ToListAsync();

            Assert.Contains(messages, m => m.PropertyName == nameof(CreateViewModel.Hosts));
        }

        /// <summary>
        /// 
        /// </summary>
        /// <returns></returns>
        [Fact]
        public async Task CanValidateDuplicateSiteName()
        {
            var site = new Site();
            site.ApplyEvent(new SiteCreatedEvent
            {
                Name = "existing site",
                Hosts = new SiteHost[0]
            });
            await _asyncDocumentSession.StoreAsync(site);
            await _asyncDocumentSession.SaveChangesExAsync();

            var viewModel = new CreateViewModel
            {
                Name = site.Name,
                Hosts = new []
                {
                    new SiteHostViewModel
                    {
                        Name = "localhost",
                        IsAdmin = true
                    }
                }
            };

            var context = new ValidationContext<CreateViewModel>(viewModel, _serviceProvider);
            var messages = await _tested.ValidateAsync(context).ToListAsync();

            Assert.Contains(messages, m => m.PropertyName == nameof(CreateViewModel.Name));
        }

        /// <summary>
        /// 
        /// </summary>
        /// <returns></returns>
        [Fact]
        public async Task CanValidateDuplicateHostname()
        {
            var host = new Host("localhost", "dummy", true);
            await _asyncDocumentSession.StoreAsync(host);
            await _asyncDocumentSession.SaveChangesExAsync();

            var viewModel = new CreateViewModel
            {
                Name = "another site",
                Hosts = new[]
                {
                    new SiteHostViewModel
                    {
                        Name = "localhost",
                        IsAdmin = true
                    }
                }
            };

            var context = new ValidationContext<CreateViewModel>(viewModel, _serviceProvider);
            var messages = await _tested.ValidateAsync(context).ToListAsync();

            Assert.Contains(messages, m => m.PropertyName == nameof(CreateViewModel.Hosts));
        }

    }

}
