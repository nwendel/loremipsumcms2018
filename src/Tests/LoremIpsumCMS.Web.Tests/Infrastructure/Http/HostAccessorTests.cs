﻿#region License
// Copyright (c) Niklas Wendel 2017-2018
// 
// Licensed under the Apache License, Version 2.0 (the "License"); 
// you may not use this file except in compliance with the License. 
// You may obtain a copy of the License at 
// 
// http://www.apache.org/licenses/LICENSE-2.0 
// 
// Unless required by applicable law or agreed to in writing, software 
// distributed under the License is distributed on an "AS IS" BASIS, 
// WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied. 
// See the License for the specific language governing permissions and 
// limitations under the License.
#endregion
using LoremIpsumCMS.Web.Infrastructure.Http;
using LoremIpsumCMS.Web.TestHelper.Http;
using Microsoft.AspNetCore.Http;
using Microsoft.Extensions.Options;
using Xunit;

namespace LoremIpsumCMS.Web.Tests.Infrastructure.Http
{

    /// <summary>
    /// 
    /// </summary>
    public class HostAccessorTests : AbstractHttpContextTests
    {

        private readonly HostAccessor _tested;

        private readonly IOptions<LoremIpsumOptions> _options = Options.Create(new LoremIpsumOptions());

        /// <summary>
        /// 
        /// </summary>
        public HostAccessorTests()
        {
            _tested = new HostAccessor(HttpContextAccessorMock.Object, _options);
        }

        /// <summary>
        /// 
        /// </summary>
        [Fact]
        public void CanIsLocalhostTrue()
        {
            HttpContext.Request.Host = new HostString("::1");

            var result = _tested.IsLocalhost;

            Assert.True(result);
        }

        /// <summary>
        /// 
        /// </summary>
        [Fact]
        public void CanIsLocalhostFalse()
        {
            HttpContext.Request.Host = new HostString("some.other.host");

            var result = _tested.IsLocalhost;

            Assert.False(result);
        }

        /// <summary>
        /// 
        /// </summary>
        [Fact]
        public void CanHasOverrideFalse()
        {
            var result = _tested.HasOverride;

            Assert.False(result);
        }

        /// <summary>
        /// 
        /// </summary>
        [Fact]
        public void CanHasOverrideTrue()
        {
            HttpContext.Session.SetHostnameOverride("asdf");

            var result = _tested.HasOverride;

            Assert.True(result);
        }

        /// <summary>
        /// 
        /// </summary>
        [Fact]
        public void CanGetOverrideHost()
        {
            HttpContext.Session.SetHostnameOverride("asdf");

            var result = _tested.OverrideHost;

            Assert.Equal("asdf", result);
        }

        /// <summary>
        /// 
        /// </summary>
        [Fact]
        public void CanGetOverrideHostNotSet()
        {
            var result = _tested.OverrideHost;

            Assert.Null(result);
        }

        /// <summary>
        /// 
        /// </summary>
        [Fact]
        public void CanGetCurrentHost()
        {
            HttpContext.Request.Host = new HostString("::1");

            var result = _tested.CurrentHost;

            Assert.Equal("::1", result.Value);
        }

        /// <summary>
        /// 
        /// </summary>
        [Fact]
        public void CanGetCurrentHostOverrideNotLocalhost()
        {
            HttpContext.Request.Host = new HostString("::1");
            HttpContext.Session.SetHostnameOverride("asdf");

            var result = _tested.CurrentHost;

            Assert.Equal("::1", result.Value);
        }

        /// <summary>
        /// 
        /// </summary>
        [Fact]
        public void CanGetCurrentHostOverrideLocalhostWithPort()
        {
            HttpContext.Request.Host = new HostString("::1", 9090);
            _options.Value.IsHostnameOverrideEnabled = true;
            HttpContext.Session.SetHostnameOverride("asdf");

            var result = _tested.CurrentHost;

            Assert.Equal("asdf:9090", result.Value);
        }

    }

}
