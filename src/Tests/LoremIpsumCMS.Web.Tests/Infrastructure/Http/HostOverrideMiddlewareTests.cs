﻿#region License
// Copyright (c) Niklas Wendel 2017-2018
// 
// Licensed under the Apache License, Version 2.0 (the "License"); 
// you may not use this file except in compliance with the License. 
// You may obtain a copy of the License at 
// 
// http://www.apache.org/licenses/LICENSE-2.0 
// 
// Unless required by applicable law or agreed to in writing, software 
// distributed under the License is distributed on an "AS IS" BASIS, 
// WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied. 
// See the License for the specific language governing permissions and 
// limitations under the License.
#endregion
using LoremIpsumCMS.Web.Infrastructure.Http;
using LoremIpsumCMS.Web.TestHelper.Http;
using Microsoft.AspNetCore.Http;
using Microsoft.Extensions.Options;
using Moq;
using System.Threading.Tasks;
using Xunit;

namespace LoremIpsumCMS.Web.Tests.Infrastructure.Http
{

    /// <summary>
    /// 
    /// </summary>
    public class HostOverrideMiddlewareTests : AbstractHttpContextTests
    {

        private readonly Mock<IHostAccessor> _hostAccessorMock = new Mock<IHostAccessor>();
        private readonly IOptions<LoremIpsumOptions> _options = Options.Create(new LoremIpsumOptions());

        /// <summary>
        /// 
        /// </summary>
        [Fact]
        public async Task CanOverrideHost()
        {
            var tested = new HostnameOverrideMiddleware(_hostAccessorMock.Object, _options);

            _hostAccessorMock
                .Setup(x => x.IsLocalhost)
                .Returns(true);
            _options.Value.IsHostnameOverrideEnabled = true;
            HttpContext.Request.QueryString = new QueryString($"?{_options.Value.HostnameOverrideQueryParameterName}=asdf");

            await tested.InvokeAsync(HttpContext, c => Task.CompletedTask);

            Assert.Equal("asdf", HttpContext.Session.GetHostnameOverride());
        }

    }

}
