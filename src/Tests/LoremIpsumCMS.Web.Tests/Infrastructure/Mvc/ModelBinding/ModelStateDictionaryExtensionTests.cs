﻿#region License
// Copyright (c) Niklas Wendel 2017-2018
// 
// Licensed under the Apache License, Version 2.0 (the "License"); 
// you may not use this file except in compliance with the License. 
// You may obtain a copy of the License at 
// 
// http://www.apache.org/licenses/LICENSE-2.0 
// 
// Unless required by applicable law or agreed to in writing, software 
// distributed under the License is distributed on an "AS IS" BASIS, 
// WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied. 
// See the License for the specific language governing permissions and 
// limitations under the License.
#endregion
using LoremIpsumCMS.Infrastructure.Validation;
using LoremIpsumCMS.Web.Infrastructure.Mvc.ModelBinding;
using Microsoft.AspNetCore.Mvc.ModelBinding;
using System;
using Xunit;

namespace LoremIpsumCMS.Web.Tests.Infrastructure.Mvc.ModelBinding
{

    /// <summary>
    /// 
    /// </summary>
    public class ModelStateDictionaryExtensionTests
    {

        /// <summary>
        /// 
        /// </summary>
        [Fact]
        public void ThrowsOnAddModelErrorsNullMessages()
        {
            var tested = new ModelStateDictionary();

            Assert.Throws<ArgumentNullException>("messages", () => tested.AddModelErrors(null));
        }

        /// <summary>
        /// 
        /// </summary>
        [Fact]
        public void CanAddMessages()
        {
            var tested = new ModelStateDictionary();
            var messages = new[]
            {
                ValidationMessage.Create("asdf", "asdf message"),
                ValidationMessage.Create("qwerty", "qwerty message")
            };

            tested.AddModelErrors(messages);

            Assert.Equal(2, tested.ErrorCount);

            Assert.NotNull(tested["asdf"]);
            Assert.Single(tested["asdf"].Errors);
            Assert.Contains(tested["asdf"].Errors, x => x.ErrorMessage == "asdf message");

            Assert.NotNull(tested["qwerty"]);
            Assert.Single(tested["qwerty"].Errors);
            Assert.Contains(tested["qwerty"].Errors, x => x.ErrorMessage == "qwerty message");
        }

    }

}
