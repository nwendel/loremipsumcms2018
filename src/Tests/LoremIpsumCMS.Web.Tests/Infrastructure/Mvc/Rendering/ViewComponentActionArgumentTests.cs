﻿#region License
// Copyright (c) Niklas Wendel 2017-2018
// 
// Licensed under the Apache License, Version 2.0 (the "License"); 
// you may not use this file except in compliance with the License. 
// You may obtain a copy of the License at 
// 
// http://www.apache.org/licenses/LICENSE-2.0 
// 
// Unless required by applicable law or agreed to in writing, software 
// distributed under the License is distributed on an "AS IS" BASIS, 
// WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied. 
// See the License for the specific language governing permissions and 
// limitations under the License.
#endregion
using LoremIpsumCMS.Web.Infrastructure.Mvc.Rendering;
using Microsoft.AspNetCore.Mvc;
using Moq;
using System;
using System.Threading.Tasks;
using Xunit;

namespace LoremIpsumCMS.Web.Tests.Infrastructure.Mvc.Rendering
{

    /// <summary>
    /// 
    /// </summary>
    public class ViewComponentActionArgumentTests
    {

        private readonly Mock<IViewComponentHelper> _viewComponentHelper = new Mock<IViewComponentHelper>();

        /// <summary>
        /// 
        /// </summary>
        [Fact]
        public async Task ThrowsOnActionNullActionName()
        {
            var tested = _viewComponentHelper.Object;

            await Assert.ThrowsAsync<ArgumentNullException>("actionName", () => tested.ActionAsync(null, "controllerName"));
        }

        /// <summary>
        /// 
        /// </summary>
        [Fact]
        public async Task ThrowsOnActionNullControllerName()
        {
            var tested = _viewComponentHelper.Object;

            await Assert.ThrowsAsync<ArgumentNullException>("controllerName", () => tested.ActionAsync("actionName", null));
        }

        /// <summary>
        /// 
        /// </summary>
        [Fact]
        public async Task ThrowsOnActionNullRouteValues()
        {
            var tested = _viewComponentHelper.Object;

            await Assert.ThrowsAsync<ArgumentNullException>("routeValues", () => tested.ActionAsync("actionName", "controllerName", null));
        }

    }

}
