﻿#region License
// Copyright (c) Niklas Wendel 2017-2018
// 
// Licensed under the Apache License, Version 2.0 (the "License"); 
// you may not use this file except in compliance with the License. 
// You may obtain a copy of the License at 
// 
// http://www.apache.org/licenses/LICENSE-2.0 
// 
// Unless required by applicable law or agreed to in writing, software 
// distributed under the License is distributed on an "AS IS" BASIS, 
// WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied. 
// See the License for the specific language governing permissions and 
// limitations under the License.
#endregion
using Microsoft.AspNetCore.Hosting;
using Microsoft.AspNetCore.TestHost;
using System;
using System.Net.Http;
using System.Threading.Tasks;
using ViewComponentActionApplication;
using ViewComponentActionApplication.Controllers;
using Xunit;

namespace LoremIpsumCMS.Web.Tests.Infrastructure.Mvc.Rendering
{

    // TODO: I need to add a test where I am not using attributes for routes on a controller

    /// <summary>
    /// 
    /// </summary>
    public class ViewComponentActionTests : IDisposable
    {

        private readonly TestServer _server;
        private readonly HttpClient _client;

        /// <summary>
        /// 
        /// </summary>
        public ViewComponentActionTests()
        {
            _server = new TestServer(new WebHostBuilder().UseStartup<Startup>());
            _client = _server.CreateClient();
        }

        /// <summary>
        /// 
        /// </summary>
        [Fact]
        public async Task CanRenderSimpleViewComponentAction()
        {
            var content = await _client.GetStringAsync($"/{nameof(TestController.SimpleViewComponentAction)}");

            Assert.Contains("Before", content);
            Assert.Contains("Component", content);
            Assert.Contains("After", content);
        }

        /// <summary>
        /// 
        /// </summary>
        [Fact]
        public async Task CanRenderAreaViewComponentAction()
        {
            var content = await _client.GetStringAsync($"/{nameof(TestController.AreaViewComponentAction)}");

            Assert.Contains("Before", content);
            Assert.Contains("Area Component", content);
            Assert.Contains("After", content);
        }

        /// <summary>
        /// 
        /// </summary>
        [Fact]
        public async Task CanRenderRecursiveViewComponentAction()
        {
            var content = await _client.GetStringAsync($"/{nameof(TestController.RecursiveViewComponentAction)}");

            Assert.Contains("Before", content);
            Assert.Contains("ParentBefore", content);
            Assert.Contains("Component", content);
            Assert.Contains("ParentAfter", content);
            Assert.Contains("After", content);
        }

        /// <summary>
        /// 
        /// </summary>
        [Fact]
        public async Task CanRenderArgumentsViewComponentAction()
        {
            var content = await _client.GetStringAsync($"/{nameof(TestController.ArgumentsViewComponentAction)}");

            Assert.Contains("Before", content);
            Assert.Contains("first-argument", content);
            Assert.Contains("123", content);
            Assert.Contains("After", content);
        }

        /// <summary>
        /// 
        /// </summary>
        public void Dispose()
        {
            _client?.Dispose();
            _server?.Dispose();
        }

    }

}
