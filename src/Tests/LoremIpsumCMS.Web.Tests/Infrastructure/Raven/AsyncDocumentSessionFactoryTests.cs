﻿#region License
// Copyright (c) Niklas Wendel 2017-2018
// 
// Licensed under the Apache License, Version 2.0 (the "License"); 
// you may not use this file except in compliance with the License. 
// You may obtain a copy of the License at 
// 
// http://www.apache.org/licenses/LICENSE-2.0 
// 
// Unless required by applicable law or agreed to in writing, software 
// distributed under the License is distributed on an "AS IS" BASIS, 
// WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied. 
// See the License for the specific language governing permissions and 
// limitations under the License.
#endregion
using FluentRegistration;
using LoremIpsumCMS.Infrastructure.Raven;
using LoremIpsumCMS.Infrastructure.Raven.UniqueConstraints;
using LoremIpsumCMS.TestHelper;
using LoremIpsumCMS.Web.Infrastructure.Raven;
using Microsoft.AspNetCore.Hosting;
using Microsoft.AspNetCore.Http;
using Microsoft.Extensions.DependencyInjection;
using Microsoft.Extensions.Options;
using Moq;
using Raven.Client.Documents.Session;
using System;
using System.Linq;
using System.Threading;
using System.Threading.Tasks;
using Xunit;

namespace LoremIpsumCMS.Web.Tests.Infrastructure.Raven
{

    /// <summary>
    /// 
    /// </summary>
    public class AsyncDocumentSessionFactoryTests : AbstractRavenTests
    {

        private readonly Mock<IHttpContextAccessor> _httpContextAccessorMock = new Mock<IHttpContextAccessor>();
        private readonly Mock<IApplicationLifetime> _applicationLifetimeMock = new Mock<IApplicationLifetime>();

        private readonly IServiceProvider _serviceProvider;

        /// <summary>
        /// 
        /// </summary>
        public AsyncDocumentSessionFactoryTests()
        {
            var options = Options.Create(new LoremIpsumOptions
            {
                RavenUrl = DocumentStore.Urls.First(),
                RavenDatabase = Guid.NewGuid().ToString()
            });

            var services = new ServiceCollection();
            services.AddSingleton(_httpContextAccessorMock.Object);
            services.AddSingleton(_applicationLifetimeMock.Object);
            services.AddSingleton(options);
            services.Install<RavenServiceInstaller>();
            services.Install<RavenWebServiceInstaller>();
            _serviceProvider = services.BuildServiceProvider();
        }

        /// <summary>
        /// 
        /// </summary>
        [Fact]
        public void CanCreateAsyncDocumentSession()
        {
            var httpContext = new DefaultHttpContext();
            _httpContextAccessorMock
                .Setup(x => x.HttpContext)
                .Returns(httpContext);
            _applicationLifetimeMock
                .Setup(x => x.ApplicationStopped)
                .Returns(new CancellationToken());
            
            var asyncDocumentSession = _serviceProvider.GetRequiredService<IAsyncDocumentSession>();

            Assert.NotNull(asyncDocumentSession);
        }

        /// <summary>
        /// 
        /// </summary>
        [Fact]
        public async Task CanSaveChanges()
        {
            var httpContext = new DefaultHttpContext();
            _httpContextAccessorMock
                .Setup(x => x.HttpContext)
                .Returns(httpContext);
            _applicationLifetimeMock
                .Setup(x => x.ApplicationStopped)
                .Returns(new CancellationToken());

            var asyncDocumentSession = _serviceProvider.GetRequiredService<IAsyncDocumentSession>();

            await asyncDocumentSession.SaveChangesExAsync();

            // TODO: What to Assert?
        }

        /// <summary>
        /// 
        /// </summary>
        [Fact]
        public async Task CanSaveChangesNoHttpContext()
        {
            _httpContextAccessorMock
                .Setup(x => x.HttpContext)
                .Returns(default(HttpContext));
            _applicationLifetimeMock
                .Setup(x => x.ApplicationStopped)
                .Returns(new CancellationToken());

            var asyncDocumentSessionHolder = _serviceProvider.GetRequiredService<IAsyncDocumentSessionHolder>();
            asyncDocumentSessionHolder.OpenAsyncDocumentSession();

            var asyncDocumentSession = _serviceProvider.GetRequiredService<IAsyncDocumentSession>();

            await asyncDocumentSession.SaveChangesExAsync();

            // TODO: What to Assert?
        }

    }

}
