﻿#region License
// Copyright (c) Niklas Wendel 2017-2018
// 
// Licensed under the Apache License, Version 2.0 (the "License"); 
// you may not use this file except in compliance with the License. 
// You may obtain a copy of the License at 
// 
// http://www.apache.org/licenses/LICENSE-2.0 
// 
// Unless required by applicable law or agreed to in writing, software 
// distributed under the License is distributed on an "AS IS" BASIS, 
// WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied. 
// See the License for the specific language governing permissions and 
// limitations under the License.
#endregion
using LoremIpsumCMS.TestHelper;
using LoremIpsumCMS.Web.Infrastructure.Raven;
using Microsoft.AspNetCore.Hosting;
using Microsoft.Extensions.Options;
using Moq;
using System;
using System.Linq;
using System.Threading;
using Xunit;

namespace LoremIpsumCMS.Web.Tests.Infrastructure.Raven
{

    /// <summary>
    /// 
    /// </summary>
    public class DocumentStoreFactoryTests : AbstractRavenTests
    {

        private readonly IOptions<LoremIpsumOptions> _options;
        private readonly Mock<IApplicationLifetime> _applicationLifetimeMock = new Mock<IApplicationLifetime>();

        /// <summary>
        /// 
        /// </summary>
        public DocumentStoreFactoryTests()
        {
            _options = Options.Create(new LoremIpsumOptions
            {
                RavenUrl = DocumentStore.Urls.First(),
                RavenDatabase = Guid.NewGuid().ToString()
            });
        }

        /// <summary>
        /// 
        /// </summary>
        [Fact]
        public void CanCreate()
        {
            _applicationLifetimeMock.Setup(x => x.ApplicationStopped).Returns(new CancellationToken());

            var tested = new DocumentStoreFactory(_options, _applicationLifetimeMock.Object);

            var documentStore = tested.CreateDocumentStore();

            Assert.NotNull(documentStore);
        }

        /// <summary>
        /// 
        /// </summary>
        [Fact]
        public void CanCreateTwice()
        {
            _applicationLifetimeMock.Setup(x => x.ApplicationStopped).Returns(new CancellationToken());

            var tested = new DocumentStoreFactory(_options, _applicationLifetimeMock.Object);

            var documentStore = tested.CreateDocumentStore();
            var anotherDocumentStore = tested.CreateDocumentStore();

            Assert.Same(documentStore, anotherDocumentStore);
        }

        /// <summary>
        /// 
        /// </summary>
        [Fact]
        public void CanCreateTwiceDifferentFactories()
        {
            _applicationLifetimeMock.Setup(x => x.ApplicationStopped).Returns(new CancellationToken());

            var factory1 = new DocumentStoreFactory(_options, _applicationLifetimeMock.Object);
            var factory2 = new DocumentStoreFactory(_options, _applicationLifetimeMock.Object);

            var documentStore = factory1.CreateDocumentStore();
            var anotherDocumentStore = factory2.CreateDocumentStore();

            Assert.NotSame(documentStore, anotherDocumentStore);
        }

        /// <summary>
        /// 
        /// </summary>
        [Fact]
        public void CanDisposeStore()
        {
            var cancellationTokenSource = new CancellationTokenSource();
            _applicationLifetimeMock.Setup(x => x.ApplicationStopped).Returns(cancellationTokenSource.Token);

            var tested = new DocumentStoreFactory(_options, _applicationLifetimeMock.Object);

            var documentStore = tested.CreateDocumentStore();
            cancellationTokenSource.Cancel();

            Assert.True(documentStore.WasDisposed);
        }

    }

}
