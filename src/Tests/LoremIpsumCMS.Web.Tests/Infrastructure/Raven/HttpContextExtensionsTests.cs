﻿#region License
// Copyright (c) Niklas Wendel 2017-2018
// 
// Licensed under the Apache License, Version 2.0 (the "License"); 
// you may not use this file except in compliance with the License. 
// You may obtain a copy of the License at 
// 
// http://www.apache.org/licenses/LICENSE-2.0 
// 
// Unless required by applicable law or agreed to in writing, software 
// distributed under the License is distributed on an "AS IS" BASIS, 
// WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied. 
// See the License for the specific language governing permissions and 
// limitations under the License.
#endregion
using LoremIpsumCMS.TestHelper;
using LoremIpsumCMS.Web.Infrastructure.Raven;
using Microsoft.AspNetCore.Http;
using Xunit;

namespace LoremIpsumCMS.Web.Tests.Infrastructure.Raven
{

    /// <summary>
    /// 
    /// </summary>
    public class HttpContextExtensionsTests : AbstractRavenTests
    {

        /// <summary>
        /// 
        /// </summary>
        [Fact]
        public void CanSetAndGetSession()
        {
            var session = DocumentStore.OpenAsyncSession();
            var tested = new DefaultHttpContext();

            tested.SetAsyncDocumentSession(session);
            var gotSession = tested.GetAsyncDocumentSession();

            Assert.Same(session, gotSession);
        }

        /// <summary>
        /// 
        /// </summary>
        [Fact]
        public void CanGetNullSession()
        {
            var tested = new DefaultHttpContext();

            var session = tested.GetAsyncDocumentSession();

            Assert.Null(session);
        }

    }

}
