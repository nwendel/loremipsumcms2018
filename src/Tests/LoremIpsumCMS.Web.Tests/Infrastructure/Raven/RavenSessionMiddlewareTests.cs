﻿#region License
// Copyright (c) Niklas Wendel 2017-2018
// 
// Licensed under the Apache License, Version 2.0 (the "License"); 
// you may not use this file except in compliance with the License. 
// You may obtain a copy of the License at 
// 
// http://www.apache.org/licenses/LICENSE-2.0 
// 
// Unless required by applicable law or agreed to in writing, software 
// distributed under the License is distributed on an "AS IS" BASIS, 
// WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied. 
// See the License for the specific language governing permissions and 
// limitations under the License.
#endregion
using LoremIpsumCMS.TestHelper;
using LoremIpsumCMS.Web.Infrastructure.Raven;
using Microsoft.AspNetCore.Http;
using Raven.Client.Documents.Session;
using System;
using System.Threading.Tasks;
using Xunit;

namespace LoremIpsumCMS.Web.Tests.Infrastructure.Raven
{

    /// <summary>
    /// 
    /// </summary>
    public class RavenSessionMiddlewareTests : AbstractRavenTests
    {

        /// <summary>
        /// 
        /// </summary>
        [Fact]
        public async Task CanInvoke()
        {
            var context = new DefaultHttpContext();
            var tested = new RavenSessionMiddleware();

            var asyncDocumentSession = (AsyncDocumentSession)DocumentStore.OpenAsyncSession();
            context.SetAsyncDocumentSession(asyncDocumentSession);

            await tested.InvokeAsync(context, c => Task.CompletedTask);

            Assert.Throws<ObjectDisposedException>(() => asyncDocumentSession.AssertNotDisposed());
        }

        /// <summary>
        /// 
        /// </summary>
        [Fact]
        public async Task CanInvokeNoSession()
        {
            var context = new DefaultHttpContext();
            var tested = new RavenSessionMiddleware();

            await tested.InvokeAsync(context, c => Task.CompletedTask);

            Assert.Null(context.GetAsyncDocumentSession());
        }

    }

}
