﻿#region License
// Copyright (c) Niklas Wendel 2017-2018
// 
// Licensed under the Apache License, Version 2.0 (the "License"); 
// you may not use this file except in compliance with the License. 
// You may obtain a copy of the License at 
// 
// http://www.apache.org/licenses/LICENSE-2.0 
// 
// Unless required by applicable law or agreed to in writing, software 
// distributed under the License is distributed on an "AS IS" BASIS, 
// WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied. 
// See the License for the specific language governing permissions and 
// limitations under the License.
#endregion
using FluentRegistration;
using LoremIpsumCMS.Infrastructure.Raven;
using LoremIpsumCMS.Web.Infrastructure.Raven;
using Microsoft.AspNetCore.Builder.Internal;
using Microsoft.Extensions.DependencyInjection;
using Xunit;

namespace LoremIpsumCMS.Web.Tests.Infrastructure.Raven
{

    /// <summary>
    /// 
    /// </summary>
    public class RavenWebInitializerTests
    {

        /// <summary>
        /// 
        /// </summary>
        [Fact]
        public void CanAddRavenMiddleware()
        {
            var services = new ServiceCollection();
            services.Install<RavenServiceInstaller>();
            services.Install<RavenWebServiceInstaller>();
            var serviceProvider = services.BuildServiceProvider();
            var applicationBuilder = new ApplicationBuilder(serviceProvider);

            var tested = new RavenWebInitializer();
            tested.Initialize(applicationBuilder);

            // TODO: Can I Assert something here?
        }

    }

}
