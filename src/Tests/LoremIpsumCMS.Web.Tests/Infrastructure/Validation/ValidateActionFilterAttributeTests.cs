﻿#region License
// Copyright (c) Niklas Wendel 2017-2018
// 
// Licensed under the Apache License, Version 2.0 (the "License"); 
// you may not use this file except in compliance with the License. 
// You may obtain a copy of the License at 
// 
// http://www.apache.org/licenses/LICENSE-2.0 
// 
// Unless required by applicable law or agreed to in writing, software 
// distributed under the License is distributed on an "AS IS" BASIS, 
// WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied. 
// See the License for the specific language governing permissions and 
// limitations under the License.
#endregion
using LoremIpsumCMS.Infrastructure.Validation;
using LoremIpsumCMS.Web.Infrastructure.Validation;
using LoremIpsumCMS.Web.TestHelper.Http;
using Microsoft.AspNetCore.Mvc;
using Microsoft.AspNetCore.Mvc.Filters;
using Moq;
using System.Threading.Tasks;
using Xunit;

namespace LoremIpsumCMS.Web.Tests.Infrastructure.Validation
{

    /// <summary>
    /// 
    /// </summary>
    public class ValidateActionFilterAttributeTests : AbstractHttpContextTests
    {

        private readonly Mock<IValidationService> _validationServiceMock = new Mock<IValidationService>();

        /// <summary>
        /// 
        /// </summary>
        [Fact]
        public async Task CanInvokeValidateNoArguments()
        {
            var controller = new TestController();
            var actionExecutingContext = CreateActionExecutingContextFor(controller, c => c.SimpleAction());

            var tested = new ValidateActionFilterAttribute(_validationServiceMock.Object);

            await tested.OnActionExecutionAsync(actionExecutingContext, () => Task.FromResult<ActionExecutedContext>(null));

            Assert.True(controller.ModelState.IsValid);
            Assert.True(controller.IsValidateCalled);
        }

        /// <summary>
        /// 
        /// </summary>
        [Fact]
        public async Task CanInvokeValidateNoArgumentsRedirect()
        {
            var controller = new TestController();
            var actionExecutingContext = CreateActionExecutingContextFor(controller, c => c.SimpleActionRedirect());

            var tested = new ValidateActionFilterAttribute(_validationServiceMock.Object);

            await tested.OnActionExecutionAsync(actionExecutingContext, () => Task.FromResult<ActionExecutedContext>(null));

            Assert.True(controller.ModelState.IsValid);
            Assert.True(controller.IsValidateCalled);
            Assert.IsType<NotFoundResult>(actionExecutingContext.Result);
        }

        /// <summary>
        /// 
        /// </summary>
        [Fact]
        public async Task CanInvokeNoArgumentsNoValidate()
        {
            var controller = new TestController();
            var actionExecutingContext = CreateActionExecutingContextFor(controller, c => c.SimpleActionNoValidate());

            var tested = new ValidateActionFilterAttribute(_validationServiceMock.Object);

            await tested.OnActionExecutionAsync(actionExecutingContext, () => Task.FromResult<ActionExecutedContext>(null));

            Assert.True(controller.ModelState.IsValid);
            Assert.False(controller.IsValidateCalled);
        }

        /// <summary>
        /// 
        /// </summary>
        [Fact]
        public async Task CanInvokeNoArgumentsInvalid()
        {
            var controller = new TestController();
            var actionExecutingContext = CreateActionExecutingContextFor(controller, c => c.SimpleActionInvalid());

            var tested = new ValidateActionFilterAttribute(_validationServiceMock.Object);

            await tested.OnActionExecutionAsync(actionExecutingContext, () => Task.FromResult<ActionExecutedContext>(null));

            Assert.False(controller.ModelState.IsValid);
            Assert.True(controller.IsValidateCalled);
            Assert.True(controller.IsInvalidCalled);
        }

        /// <summary>
        /// 
        /// </summary>
        [Fact]
        public async Task ThrowsOnInvokeNoArgumentsInvalidNoMethod()
        {
            var controller = new TestController();
            var actionExecutingContext = CreateActionExecutingContextFor(controller, c => c.SimpleActionInvalidNoMethod());

            var tested = new ValidateActionFilterAttribute(_validationServiceMock.Object);

            await Assert.ThrowsAsync<ValidationMethodException>(() => tested.OnActionExecutionAsync(actionExecutingContext, () => Task.FromResult<ActionExecutedContext>(null)));
        }

        /// <summary>
        /// 
        /// </summary>
        [Fact]
        public async Task ThrowsOnInvokeNoArgumentsInvalidNullResult()
        {
            var controller = new TestController();
            var actionExecutingContext = CreateActionExecutingContextFor(controller, c => c.SimpleActionInvalidNullResult());

            var tested = new ValidateActionFilterAttribute(_validationServiceMock.Object);

            await Assert.ThrowsAsync<ValidationMethodException>(() => tested.OnActionExecutionAsync(actionExecutingContext, () => Task.FromResult<ActionExecutedContext>(null)));
        }

        /// <summary>
        /// 
        /// </summary>
        [Fact]
        public async Task ThrowsOnInvokeNoArgumentsAmbiguousValidate()
        {
            var controller = new TestController();
            var actionExecutingContext = CreateActionExecutingContextFor(controller, c => c.SimpleActionAmbiguousValidate());

            var tested = new ValidateActionFilterAttribute(_validationServiceMock.Object);

            await Assert.ThrowsAsync<ValidationMethodException>(() => tested.OnActionExecutionAsync(actionExecutingContext, () => Task.FromResult<ActionExecutedContext>(null)));
        }

        /// <summary>
        /// 
        /// </summary>
        [Fact]
        public async Task ThrowsOnInvokeNoArgumentsValidateWrongReturnType()
        {
            var controller = new TestController();
            var actionExecutingContext = CreateActionExecutingContextFor(controller, c => c.SimpleActionValidateWrongReturnType());

            var tested = new ValidateActionFilterAttribute(_validationServiceMock.Object);

            await Assert.ThrowsAsync<ValidationMethodException>(() => tested.OnActionExecutionAsync(actionExecutingContext, () => Task.FromResult<ActionExecutedContext>(null)));
        }

        /// <summary>
        /// 
        /// </summary>
        [Fact]
        public async Task ThrowsOnInvokeNoArgumentsValidateAsyncWrongReturnType()
        {
            var controller = new TestController();
            var actionExecutingContext = CreateActionExecutingContextFor(controller, c => c.SimpleActionValidateAsyncWrongReturnType());

            var tested = new ValidateActionFilterAttribute(_validationServiceMock.Object);

            await Assert.ThrowsAsync<ValidationMethodException>(() => tested.OnActionExecutionAsync(actionExecutingContext, () => Task.FromResult<ActionExecutedContext>(null)));
        }

        /// <summary>
        /// 
        /// </summary>
        [Fact]
        public async Task CanValidateViewModels()
        {
            var firstViewModel = new FirstViewModel();
            var secondViewModel = new SecondViewModel();
            var controller = new TestController();
            var actionExecutingContext = CreateActionExecutingContextFor(controller, c => c.ActionWithArguments(firstViewModel, secondViewModel));

            _validationServiceMock
                .Setup(x => x.ValidateAsync(firstViewModel))
                .ReturnsAsync(new ValidationMessage[0]);
            _validationServiceMock
                .Setup(x => x.ValidateAsync(secondViewModel))
                .ReturnsAsync(new ValidationMessage[0]);

            var tested = new ValidateActionFilterAttribute(_validationServiceMock.Object);
            await tested.OnActionExecutionAsync(actionExecutingContext, () => Task.FromResult<ActionExecutedContext>(null));

            Assert.True(controller.ModelState.IsValid);
        }

        /// <summary>
        /// 
        /// </summary>
        [Fact]
        public async Task CanValidateViewModelsInvalid()
        {
            var firstViewModel = new FirstViewModel();
            var secondViewModel = new SecondViewModel();
            var controller = new TestController();
            var actionExecutingContext = CreateActionExecutingContextFor(controller, c => c.ActionWithArguments(firstViewModel, secondViewModel));

            _validationServiceMock
                .Setup(x => x.ValidateAsync(firstViewModel))
                .ReturnsAsync(new [] {ValidationMessage.Create("First", "Message")});
            _validationServiceMock
                .Setup(x => x.ValidateAsync(secondViewModel))
                .ReturnsAsync(new[] { ValidationMessage.Create("Second", "Message") });

            var tested = new ValidateActionFilterAttribute(_validationServiceMock.Object);
            await tested.OnActionExecutionAsync(actionExecutingContext, () => Task.FromResult<ActionExecutedContext>(null));

            Assert.False(controller.ModelState.IsValid);
            Assert.Equal(2, controller.ModelState.ErrorCount);
        }

        #region Controller

        /// <summary>
        /// 
        /// </summary>
        private class TestController : Controller
        {

            /// <summary>
            /// 
            /// </summary>
            public bool IsValidateCalled { get; private set; }

            /// <summary>
            /// 
            /// </summary>
            public bool IsInvalidCalled { get; private set; }

            /// <summary>
            /// 
            /// </summary>
            /// <returns></returns>
            public IActionResult SimpleActionNoValidate()
            {
                return null;
            }

            /// <summary>
            /// 
            /// </summary>
            /// <returns></returns>
            public ValidationResult ValidateSimpleAction()
            {
                IsValidateCalled = true;
                return ValidationResult.Success;
            }

            /// <summary>
            /// 
            /// </summary>
            /// <returns></returns>
            public IActionResult SimpleAction()
            {
                return null;
            }

            /// <summary>
            /// 
            /// </summary>
            /// <returns></returns>
            public ValidationResult ValidateSimpleActionRedirect()
            {
                IsValidateCalled = true;
                return ValidationResult.NotFound;
            }

            /// <summary>
            /// 
            /// </summary>
            /// <returns></returns>
            public IActionResult SimpleActionRedirect()
            {
                return null;
            }

            /// <summary>
            /// 
            /// </summary>
            /// <returns></returns>
            public ValidationResult ValidateSimpleActionInvalid()
            {
                IsValidateCalled = true;
                return ValidationResult.Message("Invalid");
            }

            /// <summary>
            /// 
            /// </summary>
            /// <returns></returns>
            public IActionResult InvalidSimpleActionInvalid()
            {
                IsInvalidCalled = true;
                return Content(""); 
            }

            /// <summary>
            /// 
            /// </summary>
            /// <returns></returns>
            public IActionResult SimpleActionInvalid()
            {
                return null;
            }

            /// <summary>
            /// 
            /// </summary>
            /// <returns></returns>
            public ValidationResult ValidateSimpleActionInvalidNoMethod()
            {
                IsValidateCalled = true;
                return ValidationResult.Message("Invalid");
            }

            /// <summary>
            /// 
            /// </summary>
            /// <returns></returns>
            public IActionResult SimpleActionInvalidNoMethod()
            {
                return null;
            }

            /// <summary>
            /// 
            /// </summary>
            /// <returns></returns>
            public ValidationResult ValidateSimpleActionInvalidNullResult()
            {
                IsValidateCalled = true;
                return ValidationResult.Message("Invalid");
            }

            /// <summary>
            /// 
            /// </summary>
            /// <returns></returns>
            public IActionResult InvalidSimpleActionInvalidNullResult()
            {
                IsInvalidCalled = true;
                return null;
            }

            /// <summary>
            /// 
            /// </summary>
            /// <returns></returns>
            public IActionResult SimpleActionInvalidNullResult()
            {
                return null;
            }

            /// <summary>
            /// 
            /// </summary>
            /// <returns></returns>
            public ValidationResult ValidateSimpleActionAmbiguousValidate()
            {
                return null;
            }

            /// <summary>
            /// 
            /// </summary>
            /// <returns></returns>
            public Task<ValidationResult> ValidateSimpleActionAmbiguousValidateAsync()
            {
                return null;
            }

            /// <summary>
            /// 
            /// </summary>
            /// <returns></returns>
            public IActionResult SimpleActionAmbiguousValidate()
            {
                return null;
            }

            /// <summary>
            /// 
            /// </summary>
            /// <returns></returns>
            public Task<object> ValidateSimpleActionValidateWrongReturnType()
            {
                return null;
            }

            /// <summary>
            /// 
            /// </summary>
            /// <returns></returns>
            public IActionResult SimpleActionValidateWrongReturnType()
            {
                return null;
            }

            /// <summary>
            /// 
            /// </summary>
            /// <returns></returns>
            public object ValidateSimpleActionValidateAsyncWrongReturnTypeAsync()
            {
                return null;
            }

            /// <summary>
            /// 
            /// </summary>
            /// <returns></returns>
            public IActionResult SimpleActionValidateAsyncWrongReturnType()
            {
                return null;
            }

            /// <summary>
            /// 
            /// </summary>
            /// <returns></returns>
            public IActionResult InvalidActionWithArguments(FirstViewModel firstViewModel, SecondViewModel secondViewModel)
            {
                IsInvalidCalled = true;
                return Content("");
            }

            /// <summary>
            /// 
            /// </summary>
            /// <returns></returns>
            public IActionResult ActionWithArguments(FirstViewModel firstViewModel, SecondViewModel secondViewModel)
            {
                return Content(string.Empty);
            }

        }

        /// <summary>
        /// 
        /// </summary>
        private class FirstViewModel
        {
        }

        /// <summary>
        /// 
        /// </summary>
        private class SecondViewModel
        {
        }

        #endregion
    }

}
