﻿#region License
// Copyright (c) Niklas Wendel 2017-2018
// 
// Licensed under the Apache License, Version 2.0 (the "License"); 
// you may not use this file except in compliance with the License. 
// You may obtain a copy of the License at 
// 
// http://www.apache.org/licenses/LICENSE-2.0 
// 
// Unless required by applicable law or agreed to in writing, software 
// distributed under the License is distributed on an "AS IS" BASIS, 
// WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied. 
// See the License for the specific language governing permissions and 
// limitations under the License.
#endregion
using LoremIpsumCMS.Web.Infrastructure.Validation;
using Microsoft.AspNetCore.Mvc;
using Xunit;

namespace LoremIpsumCMS.Web.Tests.Infrastructure.Validation
{

    /// <summary>
    /// 
    /// </summary>
    public class ValidationResultTests
    {

        /// <summary>
        /// 
        /// </summary>
        [Fact]
        public void CanSuccess()
        {
            var result = ValidationResult.Success;

            Assert.True(result.IsSuccess);
            Assert.False(result.HasMessages);
            Assert.False(result.IsRedirect);
        }

        /// <summary>
        /// 
        /// </summary>
        [Fact]
        public void CanNotFound()
        {
            var result = ValidationResult.NotFound;

            Assert.False(result.IsSuccess);
            Assert.False(result.HasMessages);
            Assert.True(result.IsRedirect);
            Assert.IsType<NotFoundResult>(result.RedirectActionResult);
        }

        /// <summary>
        /// 
        /// </summary>
        [Fact]
        public void CanForbidden()
        {
            var result = ValidationResult.Forbid;

            Assert.False(result.IsSuccess);
            Assert.False(result.HasMessages);
            Assert.True(result.IsRedirect);
            Assert.IsType<ForbidResult>(result.RedirectActionResult);
        }
        /// <summary>
        /// 
        /// </summary>
        [Fact]
        public void CanBadRequest()
        {
            var result = ValidationResult.BadRequest;

            Assert.False(result.IsSuccess);
            Assert.False(result.HasMessages);
            Assert.True(result.IsRedirect);
            Assert.IsType<BadRequestResult>(result.RedirectActionResult);
        }

    }

}
